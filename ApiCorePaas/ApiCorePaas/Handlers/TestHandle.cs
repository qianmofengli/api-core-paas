﻿using ApiCorePaas.DataModels;
using ApiCorePaas.Entities.Core;
using ApiCorePaas.Entities.Iot;
using ApiCorePaas.Entities.Mongo;
using ApiCorePaas.Helpers;
using ApiCorePaas.Models;
using ApiCorePaas.Models.DependencyInjection;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.ErrorAndResult;
using ApiCorePaas.Models.SqlSugar;
using MongoDB.Driver;
using QMFL.MongoMultiDbRepository;

namespace ApiCorePaas.Handlers;

/// <summary>
/// 测试处理类
/// </summary>
public class TestHandle : Application, ITransient
{
    private readonly ILogger<TestHandle> _logger;
    private readonly SqlSugarRepository<User> _userRepository;
    private readonly SqlSugarRepository<Device> _deviceRepository;
    private readonly IMongoRepository<IotMqttLog> _iotMqttLogMongoRepository;
    private readonly IMongoRepository<OperationLog> _operationLogMongoRepository;
    private readonly IMongoRepository<DeviceMessageLog> _deviceMessageLogMongoRepository;
    private readonly MongoDbFactory _dbFactory;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="userRepository"></param>
    /// <param name="deviceRepository"></param>
    /// <param name="iotMqttLogMongoRepository"></param>
    /// <param name="operationLogMongoRepository"></param>
    /// <param name="dbFactory"></param>
    /// <param name="deviceMessageLogMongoRepository"></param>
    public TestHandle(ILogger<TestHandle> logger, SqlSugarRepository<User> userRepository,
        SqlSugarRepository<Device> deviceRepository, IMongoRepository<IotMqttLog> iotMqttLogMongoRepository,
        IMongoRepository<OperationLog> operationLogMongoRepository, MongoDbFactory dbFactory,
        IMongoRepository<DeviceMessageLog> deviceMessageLogMongoRepository)
    {
        _logger = logger;
        _userRepository = userRepository;
        _deviceRepository = deviceRepository;
        _iotMqttLogMongoRepository = iotMqttLogMongoRepository;
        _operationLogMongoRepository = operationLogMongoRepository;
        _dbFactory = dbFactory;
        _deviceMessageLogMongoRepository = deviceMessageLogMongoRepository;
    }

    /// <summary>
    /// 测试方法
    /// </summary>
    /// <returns></returns>
    public async Task<(bool, ErrorInfo)> Test()
    {
        try
        {
            // if (true)
            // {
            //     throw new Exception("异常消息");
            // }

            // _logger.LogInformation("测试消息");
            // var user = await _userRepository.GetListAsync();
            // var device = await _deviceRepository.GetListAsync();
            // var iotMqttLog = await _iotMqttLogMongoRepository.GetAllAsync();
            // var operationLog = await _operationLogMongoRepository.GetAllAsync();
            // var collectionNames = new List<string>
            // {
            //     "20231201",
            //     "20231202"
            // };
            // var startTime = new DateTime(2023, 12, 02);
            // var endTime = new DateTime(2023, 12, 04);
            //
            // var filter = Builders<DeviceMessageLog>.Filter.And(
            //     Builders<DeviceMessageLog>.Filter.Eq("EnterpriseId", 9));
            // var (result, total) =
            //     await _deviceMessageLogMongoRepository.GetCrossCollectionPagedAsync(_dbFactory, startTime, endTime,
            //         filter, null, 11, 10);
            // var testPostModel = new TestPostModel
            // {
            //     DatabaseEnum = DatabaseEnum.Core,
            //     IntValue = 1
            // };
            // return (testPostModel, null);
            var message = new DeviceMessageLog
            {
                Message = "111",
                Topic = "11",
                DeviceNo = 12323232323,
                EnterpriseId = 9,
                CreateTime = DateTime.Now,
                EncryptedMessage = "111",
                MsgId = "111",
                IsHandle = false,
                ProductId = 1,
                MessageSource = MessageSourceEnum.Device,
            };
            await _deviceMessageLogMongoRepository.InsertAsync(message, "20231207");
            var a = await _deviceMessageLogMongoRepository.FindAsync(null, "20231207");
            return (true, null);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "测试异常");
            return (false, new ErrorInfo(ErrorEnum.DataError, ex.Message));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public async Task<(string, ErrorInfo)> TestToken()
    {
        try
        {
            var token = JwtHelper.GenerateJwtToken(1, false);
            return (token, null);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "测试异常");
            return (null, new ErrorInfo(ErrorEnum.DataError, ex.Message));
        }
    }
}