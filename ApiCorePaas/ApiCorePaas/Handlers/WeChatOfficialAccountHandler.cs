﻿using System.Xml;
using ApiCorePaas.DataModels.ErnieBot;
using ApiCorePaas.DataModels.WeChat;
using ApiCorePaas.Helpers;
using ApiCorePaas.Models;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.DependencyInjection;
using ApiCorePaas.Models.Entities;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.ErrorAndResult;
using ApiCorePaas.Models.SqlSugar;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;

namespace ApiCorePaas.Handlers;

/// <summary>
/// 微信公众号处理类
/// </summary>
public class WeChatOfficialAccountHandler : Application, ITransient
{
    private readonly ILogger<WeChatOfficialAccountHandler> _logger;
    private readonly ErnieBotHandle _ernieBotHandle;
    private readonly SqlSugarRepository<UserAccount> _userAccountRepository;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="userAccountRepository"></param>
    /// <param name="ernieBotHandle"></param>
    public WeChatOfficialAccountHandler(ILogger<WeChatOfficialAccountHandler> logger,
        SqlSugarRepository<UserAccount> userAccountRepository, ErnieBotHandle ernieBotHandle)
    {
        _logger = logger;
        _userAccountRepository = userAccountRepository;
        _ernieBotHandle = ernieBotHandle;
    }

    /// <summary>
    /// 验证消息来自微信
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public Task<string> VerifyMessage(AuthEventModel model)
    {
        _logger.LogInformation("微信GET消息验证，{Serialize}", Util.Serialize(model));
        var list = new List<string>
        {
            AppSettings.WeChatOption.OfficialAccountToken,
            model.Timestamp,
            model.Nonce
        };
        list.Sort();
        var res = string.Join("", list.ToArray());
        var encryptedString = Util.Sha1Encrypt(res);
        if (encryptedString.Equals(model.Signature))
        {
            return Task.FromResult(model.Echostr);
        }

        _logger.LogError("微信公众号消息认证失败");
        return Task.FromResult("");
    }

    /// <summary>
    /// 消息事件
    /// </summary>
    /// <param name="model"></param>
    /// <param name="bodyContent"></param>
    /// <returns></returns>
    public async Task<string> MessageEvent(AuthMessageEventModel model, string bodyContent)
    {
        var message = "success";
        try
        {
            // var ciphertext = Util.XmlDeserialize<MsgEncryptModel>(bodyContent);
            _logger.LogInformation("微信POST消息事件，Query:{Serialize},消息:{BodyContent}", Util.Serialize(model), bodyContent);
            var (code, decryptMsg) =
                MsgCrypt.DecryptMsg(model.MsgSignature, model.Timestamp, model.Nonce, bodyContent);
            if (code != MsgCryptCodeEnum.Success)
                throw new Exception("ERR: Decrypt fail, ret: " + code);

            var doc = new XmlDocument();
            doc.LoadXml(decryptMsg);
            var rootNode = doc.FirstChild;
            var eventType = rootNode?["Event"]?.InnerText;
            var msgType = rootNode?["MsgType"]?.InnerText;
            if (!string.IsNullOrEmpty(eventType))
            {
                switch (eventType.ToLower())
                {
                    case "subscribe":
                        await Subscribe(decryptMsg);
                        break;
                    case "unsubscribe":
                        await UnSubscribe(decryptMsg);
                        break;
                }
            }


            if (!string.IsNullOrEmpty(msgType))
            {
                switch (msgType.ToLower())
                {
                    case "text":
                        message = await ReplyMessage(decryptMsg);
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "微信公众号消息事件处理失败");
            message = "fail";
        }

        return message;
    }

    /// <summary>
    /// 获取关注的用户列表
    /// </summary>
    /// <returns></returns>
    public async Task<(bool, ErrorInfo)> GetSubscribeUser()
    {
        try
        {
            var userAccounts = await _userAccountRepository.GetListAsync(r => r.IsSubscribe == true);
            var accessToken = await WeChatHelper.GetOfficialAccountAccessTokenAsync();
            var url = $"cgi-bin/user/get?access_token={accessToken}";
            var (response, error) = await ClientPool.WeChatClient.GetAsync<SubscribeUserResponseModel>(url);
            if (error is not null)
            {
                throw error;
            }

            var openIds = response.Data.OpenIds;
            var userAccountOpenIds = userAccounts.Select(r => r.OpenId);
            var differenceOpenIds = openIds.Except(userAccountOpenIds).ToList();

            var now = DateTime.Now;
            var insertUserAccounts = differenceOpenIds.Select(openid => new UserAccount()
            {
                OpenId = openid, CreateTime = now, AccountType = AccountTypeEnum.WeChatOfficialAccount,
                IsSubscribe = true
            }).ToList();

            var result = await _userAccountRepository.InsertRangeAsync(insertUserAccounts);
            if (!result)
            {
                throw new Exception("更新订阅用户列表失败");
            }

            return (true, null);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "获取关注的用户列表异常");
            return (false, new ErrorInfo(ErrorEnum.DataError, ex.Message));
        }
    }

    /// <summary>
    /// 订阅公众号
    /// </summary>
    /// <returns></returns>
    private async Task Subscribe(string decryptMsg)
    {
        var subscribe = Util.XmlDeserialize<SubscribeUnSubscribeXModel>(decryptMsg);

        var userAccount = await _userAccountRepository.GetSingleAsync(r => r.OpenId == subscribe.FromUserName);
        if (userAccount is null)
        {
            userAccount = new UserAccount
            {
                OpenId = subscribe.FromUserName,
                AccountType = AccountTypeEnum.WeChatOfficialAccount,
                IsSubscribe = true,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };
            var insertSuccess = await _userAccountRepository.InsertAsync(userAccount);
            if (!insertSuccess)
            {
                throw new Exception("插入关注公众号用户信息失败");
            }
        }
        else
        {
            userAccount.IsSubscribe = true;
            userAccount.UpdateTime = DateTime.Now;
            var updateSuccess = await _userAccountRepository.UpdateAsync(userAccount);
            if (!updateSuccess)
            {
                throw new Exception("更新关注公众号用户信息失败");
            }
        }
    }

    /// <summary>
    /// 取消关注公众号
    /// </summary>
    /// <param name="decryptMsg"></param>
    /// <exception cref="Exception"></exception>
    private async Task UnSubscribe(string decryptMsg)
    {
        var subscribe = Util.XmlDeserialize<SubscribeUnSubscribeXModel>(decryptMsg);

        var userAccount = await _userAccountRepository.GetSingleAsync(r => r.OpenId == subscribe.FromUserName);
        if (userAccount is not null)
        {
            userAccount.IsSubscribe = false;
            var insertSuccess = await _userAccountRepository.UpdateAsync(userAccount);
            if (!insertSuccess)
            {
                throw new Exception("更新关注公众号用户信息失败");
            }
        }
    }

    /// <summary>
    /// 文本消息
    /// </summary>
    /// <param name="decryptMsg"></param>
    /// <returns></returns>
    private async Task<string> ReplyMessage(string decryptMsg)
    {
        var message = Util.XmlDeserialize<TextMessageModel>(decryptMsg);

        if (message.Content.Contains("pic"))
        {
            return await ImageReply(message);
        }

        return await TextReply(message);
    }

    /// <summary>
    /// 图片回复
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private async Task<string> ImageReply(TextMessageModel message)
    {
        var (result, error) = await _ernieBotHandle.StableDiffusionXlImage(new ErnieBotTurboChatModel()
        {
            Message = message.Content
        }, message.FromUserName);

        if (error is not null)
        {
            throw new Exception(error.ErrorMessage);
        }

        // var allBase64String = $"data:image/png;base64,{result.FirstOrDefault()?.B64Image}";
        var allBase64String = result.FirstOrDefault()?.B64Image;
        _logger.LogInformation("图片Base64:{AllBase64String}", allBase64String);
        var (mediaId, errorInfo) = await UploadTemporaryMaterial(allBase64String);
        if (errorInfo is not null)
        {
            throw new Exception(errorInfo.ErrorMessage);
        }

        _logger.LogInformation("图片MediaId:{MediaId}", mediaId);
        var replyMessage = Util.XmlSerialize(new ReplyImageXModel()
        {
            ToUserName = message.FromUserName.ToXmlNode(),
            FromUserName = message.ToUserName.ToXmlNode(),
            CreateTime = Util.ToUnixTimestampBySeconds(DateTime.Now),
            MsgType = "image".ToXmlNode(),
            Image = new ImageMsgXModel()
            {
                MediaId = mediaId.ToXmlNode()
            }
        });
        var (eCode, encryptMsg) = MsgCrypt.EncryptMsg(replyMessage);
        if (eCode != 0)
            throw new Exception($"加密回复消息失败 错误：{eCode}");
        return encryptMsg;
    }

    /// <summary>
    /// 回复文字消息
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private async Task<string> TextReply(TextMessageModel message)
    {
        var (result, error) = await _ernieBotHandle.ErnieBotTurboChat(new ErnieBotTurboChatModel
        {
            Message = message.Content
        }, message.FromUserName);

        if (error is not null)
        {
            throw new Exception(error.ErrorMessage);
        }

        _logger.LogInformation("问题：{MessageContent},答案：{Result}", message.Content, result);

        var replyMessage = Util.XmlSerialize(new ReplyTextXModel
        {
            ToUserName = message.FromUserName.ToXmlNode(),
            FromUserName = message.ToUserName.ToXmlNode(),
            CreateTime = Util.ToUnixTimestampBySeconds(DateTime.Now),
            MsgType = "text".ToXmlNode(),
            Content = result.ToXmlNode()
        });
        var (eCode, encryptMsg) = MsgCrypt.EncryptMsg(replyMessage);
        if (eCode != 0)
            throw new Exception($"加密回复消息失败 错误：{eCode}");
        return encryptMsg;
    }


    /// <summary>
    /// 上传临时素材
    /// </summary>
    /// <param name="base64String"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<(string, ErrorInfo)> UploadTemporaryMaterial(string base64String)
    {
        try
        {
            base64String =
                "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAECAQIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACikzRmgBaKTNGaAFopM0ZFAC0UmaKAFopNw9aM0ALRSUZHrQAtFJS0AFFJmjNAC0UlAINAC0UUmaAFopM8UZoAWikoyPWgBaKTNGaAFopM0ZoAWiiigAooooAKQ8ilpD0oA/ID9vT9vP46/BX9rHx14N8GeOf7G8N6b9h+y2X9kWE/l+ZYW8r/ADywM5y8jnljjOBwAK8A/wCHo37Tv/RTP/KBpf8A8jUv/BUf/k+z4m/9wz/012lfun8Uvin4Y+C3gTU/GXjLU/7H8N6b5X2q9+zyz+X5kqRJ8kSs5y8iDhTjOTwCaAPwr/4ejftO/wDRTP8AygaX/wDI1H/D0b9p3/opn/lA0v8A+Rq/VY/8FRf2YwcH4lnP/YA1T/5GpP8Ah6L+zH/0Uw/+CDVP/kagD8qv+Ho37Tv/AEUz/wAoGl//ACNX3/8A8Epv2ovid+0p/wALR/4WP4m/4SL+xf7L+wf6Ba2vk+d9r8z/AFESbs+VH97ONvGMnPq3/BUf/kxP4m/9wz/06WlfKn/BDL/mtn/cE/8Ab+gDlP29P28/jr8Ff2sfHXg3wZ45/sbw3pv2H7LZf2RYT+X5lhbyv88sDOcvI55Y4zgcACv0A/b1+KPif4L/ALJ3jnxl4N1P+xvEmmfYfsl79nin8vzL+3if5JVZDlJHHKnGcjkA1+QP/BUf/k+z4m/9wz/012lfKtAH7T/8Epf2ovid+0mPiiPiP4m/4SIaL/Zf2D/QLW18nzvtfmf6iJN2fKj+9nG3jGTnwD9vT9vP46/BX9rHx14N8GeOf7G8N6b9h+y2X9kWE/l+ZYW8r/PLAznLyOeWOM4HAAr81wMmv1//AGCv28/gV8Fv2TvAvg3xn44OjeJNN+3farL+yL+fy/Mv7iVPnigZDlJEPDHGcHkEUAfVP7evxR8T/Bf9k7xz4y8G6n/Y3iTTPsP2S9+zxT+X5l/bxP8AJKrIcpI45U4zkcgGvyA/4ei/tOYx/wALM46Y/sDS/wD5Gr9f/wBvX4XeJ/jR+yd458G+DdM/tnxJqf2H7JZfaIoPM8u/t5X+eVlQYSNzywzjA5IFfFX7DH/GtkeNv+Gjf+Ld/wDCafYf7B/5in2z7J9o+0/8ePneXs+12/8ArNu7f8udrYAPtX9gr4o+J/jR+yd4G8ZeMtT/ALZ8San9u+13v2eKDzPLv7iJPkiVUGEjQcKM4yeSTX0BX4rftSfsufE79tD47eJvjJ8G/DP/AAmPw38S/Zf7K1r7fa2P2n7PaxWs37m6limTbNbyp86DO3IypBP0B+3r+3n8CvjT+yd468G+DPHB1nxJqX2H7LZf2RfweZ5d/byv88sCoMJG55YZxgckCgDqf+Crf7UfxO/ZrPwu/wCFceJv+Ed/tr+1Pt/+gWt153k/ZPK/18T7cebJ93Gd3OcDHwB/w9G/ad/6KZ/5QNL/APkavqv/AIIZDB+Nn/cE/wDb+v1UoA+f/wBvX4o+J/gv+yd458ZeDdT/ALG8SaZ9h+yXv2eKfy/Mv7eJ/klVkOUkccqcZyOQDXz/AP8ABKT9qL4nftJ/8LQHxG8Tf8JENF/sv7B/oFra+T532vzP9REm7PlR/ezjbxjJz9Aft6/C7xP8aP2TvHPg3wbpn9s+JNT+w/ZLL7RFB5nl39vK/wA8rKgwkbnlhnGByQK+Kv2F/wDjWyfGv/DRv/Fu/wDhNPsP9g4/4mn2z7J9o+0/8ePn+Xs+12/+s27t/wAudrYAP1Vr8f8A9gv9vP46/Gr9rHwL4N8Z+Of7Z8N6l9u+1WX9kWEHmeXYXEqfPFArjDxoeGGcYPBIr9Vfhb8U/DHxp8CaZ4y8G6n/AGx4b1Lzfst79nlg8zy5Xif5JVVxh43HKjOMjgg0fFL4p+GPgt4E1Pxl4y1P+x/Dem+V9qvfs8s/l+ZKkSfJErOcvIg4U4zk8AmgDqsfLivyA/b0/bz+OvwV/ax8deDfBnjn+xvDem/Yfstl/ZFhP5fmWFvK/wA8sDOcvI55Y4zgcACus/boH/Dyc+CR+zl/xcQ+C/t39vf8wv7H9r+z/Zv+P7yPM3/ZLj/V7tuz5sblz8qf8Ouf2nf+iZ/+V/S//kmgD9f/ANvX4o+J/gv+yd458ZeDdT/sbxJpn2H7Je/Z4p/L8y/t4n+SVWQ5SRxypxnI5ANfkB/w9F/acHA+JnH/AGANL/8Akav1V/4Kj/8AJifxN/7hn/p0tK+VP+CGRx/wuwnoP7E/9v6APlX/AIejftO/9FM/8oGl/wDyNR/w9G/ad/6KZ/5QNL/+Rq/YD4o/t6fAr4L+OtT8G+MvHB0bxJpvlfarL+yL+fy/MiSVPnigZDlJEPDHGcHBBFcp/wAPRf2Y/wDoph/8EGqf/I1AH5Vf8PRv2nf+imf+UDS//kaj/h6N+07/ANFM/wDKBpf/AMjV+1PwN/ak+GP7Sf8AbY+HHib/AISL+xfI+3/6BdWvk+d5nlf6+JN2fKk+7nG3nGRn8V/+Co//ACfZ8Tf+4Z/6a7SgD9/qKKKACiiigApD0paQ9KAPwC/4Kj/8n2fE3/uGf+mu0r9VP+Co/wDyYp8TP+4Z/wCnO0r8q/8AgqP/AMn2fE3/ALhn/prtK/VT/gqP/wAmJ/E3/uGf+nS0oA/AHNGaKKAP3+/4Kj/8mJ/E3/uGf+nS0r5U/wCCGX/NbP8AuCf+39fVf/BUf/kxP4m/9wz/ANOlpXyr/wAEMevxs/7gn/t/QB6p+1F/wSl/4aU+Ovib4j/8LR/4Rz+2vsv/ABLf+Ef+1eT5NrFB/rftSbs+Vu+6Mbsc4yfKv+HGX/VbP/LU/wDu2v1Vr5//AG9fij4n+C/7J3jnxl4N1P8AsbxJpn2H7Je/Z4p/L8y/t4n+SVWQ5SRxypxnI5ANAH5A/tzfsMj9i7/hCf8Aitv+Ex/4ST7b/wAwr7D9m+z/AGf/AKby793n+2NvfPHq37Lv/BKX/hpT4FeGfiOPij/wjn9tfav+Jb/wj/2ryfJupYP9b9qTdnyt33RjdjnGT8qfHL9qP4nftJDRB8RvE3/CRDRfP+wf6Ba2vk+d5fmf6iJN2fKj+9nG3jGTn9qf+CXH/Jifwy/7if8A6dLugD1T9qT45/8ADNnwK8TfEb+xP+Ei/sX7L/xLPtf2XzvOuooP9bsfbjzd33TnbjjOR8Af8poP+qOn4bf9xz+0f7Q/8BvJ8v7B/t7vN/h2/N+lXxS+Fnhj40+BNT8G+MtM/tjw3qXlfarL7RLB5nlypKnzxMrjDxoeGGcYPBIr81v26D/w7YPgk/s5f8W7PjT7d/b3/MU+2fZPs/2b/j+8/wAvZ9ruP9Xt3b/mztXAB9/fsufAz/hmz4FeGfhz/bY8Rf2L9q/4mf2T7L53nXUs/wDqt77cebt+8c7c8ZwP5reTX1T/AMPRv2nf+imf+UDS/wD5Grlf2Cvhd4Y+NH7WPgbwb4y0z+2PDep/bvtdl9olg8zy7C4lT54mVxh40PDDOMHgkUAfan/BDM4/4XZn/qCf+39eq/tRf8FWj+zZ8dfE3w4/4Vd/wkf9i/Zv+Jn/AMJB9l87zrWKf/VfZX2483b945254zgeV/tzn/h2yfBJ/Zy/4t2fGn27+3v+Yp9s+yfZ/s3/AB++d5ez7Xcf6vbu3/NnauPVf2W/2XPhj+2h8CfDPxk+Mnhn/hMfiR4l+1f2rrX9oXVj9p+z3UtrD+5tZYoU2w28SfIgztycsSSAff3FflZ/wXM6/BPH/Ub/APbCvlT/AIejftO/9FM/8oGl/wDyNXlfxz/ak+J/7SY0T/hY/ib/AISL+xfP+wf6Ba2vk+d5fm/6iJN2fKj+9nG3jGTkA+qv2Xf+Crf/AAzX8CvDPw4/4Vd/wkf9i/av+Jn/AMJB9l87zrqWf/VfZX2483b945254zgff/8AwVG/5MU+Jn/cM/8ATnaV+ANf0+/FL4WeGPjT4E1Pwb4y0z+2PDepeV9qsvtEsHmeXKkqfPEyuMPGh4YZxg8EigD8LP2Gf25v+GLv+E2/4on/AITE+JfsX/MV+w/Zvs/2j/phLv3faPbG3vnj6p/4fm/9UT/8uv8A+4q+rD/wS7/Zkzn/AIVoc9c/2/qn/wAk1+P/AO3r8LvDHwX/AGsfHPg3wbpn9j+G9M+w/ZLL7RLP5fmWFvK/zysznLyOeWOM4HAAoA/X/wD4Kj/8mJ/E3/uGf+nS0r5U/wCCGX/NbP8AuCf+39fVf/BUf/kxP4m/9wz/ANOlpXyp/wAEMv8Amtn/AHBP/b+gD5W/4Kjcft1/Ez/uGf8ApstK+Vc19Vf8FR/+T7Pib/3DP/TXaV8q0Afqn/wQy/5rZ/3BP/b+vlb/AIKj/wDJ9nxN/wC4Z/6a7Svqn/ghl/zWz/uCf+39fK3/AAVH/wCT7Pib/wBwz/012lAH7/UUUUAFFFFABSHpS0h6UAfgF/wVH/5Ps+Jv/cM/9NdpX6qf8PRf2Y/+imH/AMEGqf8AyNXlP7UX/BKX/hpT46+JviP/AMLR/wCEc/tr7L/xLf8AhH/tXk+TaxQf637Um7PlbvujG7HOMnyv/hxl/wBVs/8ALU/+7aAPqr/h6L+zH/0Uw/8Agg1T/wCRqP8Ah6L+zH/0Uw/+CDVP/kavlX/hxn/1W3/y1P8A7to/4cZf9Vs/8tT/AO7aAOr/AG9f28/gV8af2TvHXg3wZ44Os+JNS+w/ZbL+yL+DzPLv7eV/nlgVBhI3PLDOMDkgVyn/AAQzGP8Ahdn/AHBP/b+j/hxl/wBVs/8ALU/+7a+qP2Gf2Gf+GL/+E2/4rb/hMf8AhJfsX/MJ+w/Zvs/2j/pvLv3ef7Y2988AH5W/8FR/+T7Pib/3DP8A012leq/st/sufE79i/47eGfjJ8ZPDP8Awh3w38Nfav7V1r7fa332b7Ray2sP7m1llmfdNcRJ8iHG7JwoJHlX/BUf/k+z4m/9wz/012lftR+1H8DP+Gk/gV4m+HP9tjw7/bX2X/iZ/ZPtXk+TdRT/AOq3puz5W37wxuzzjBAD4GftR/DH9pP+2x8OfE3/AAkX9i+R9v8A9AurXyfO8zyv9fEm7PlSfdzjbzjIz+K//BUf/k+z4m/9wz/012lfqn+wz+w1/wAMX/8ACbZ8bf8ACY/8JL9i/wCYV9h+zfZ/tH/TeXfu+0e2NvfPH5V/8FRv+T6/ib/3DP8A02WlAH6q/wDBUf8A5MT+Jv8A3DP/AE6Wlfit8DP2W/if+0n/AG2fhx4Z/wCEi/sXyPt/+n2tr5PneZ5X+vlTdnypPu5xt5xkZ/f39qT4Gf8ADSfwJ8TfDj+2/wDhHf7a+y/8TL7J9q8nybqKf/Vb03Z8rb94Y3Z5xg/AGP8Ahy+P+ixf8LJ/7gf9nf2f/wCBPm+Z9v8A9jb5X8W7gA9W/Zb/AGo/hj+xf8CfDPwb+Mnib/hDviR4a+1f2rov9n3V99m+0XUt1D++tYpYX3Q3ET/I5xuwcMCB8q/st/sufE79i/47eGfjJ8ZPDP8Awh3w38Nfav7V1r7fa332b7Ray2sP7m1llmfdNcRJ8iHG7JwoJHqv/DDH/Dyf/jI3/hNv+Fdf8Jp/zLX9lf2p9j+yf6B/x8+dD5m/7J5n+rXbv287dx+/v2o/gZ/w0l8CvE3w5/tv/hHf7a+y/wDEz+yfavJ8m6in/wBVvTdnytv3hjdnnGCAfAH7dH/GyYeCf+Gcv+Lif8IX9u/t7/mF/Y/tf2f7N/x++T5m/wCyXH+r3bdnzY3Ln81/il8LfE/wW8d6n4N8ZaZ/Y3iTTfK+1WXnxT+X5kSSp88TMhykiHhjjODyCK/Sj/lDB/1WL/hZP/cD/s7+z/8AwJ83zPt/+xt8r+Ld8q/8MMf8PJ/+Mjv+E2/4V1/wmn/Mtf2V/an2P7J/oH/Hz50Pmb/snmf6tdu/bzt3EA/Sn4pfFPwx8FvAmp+MvGWp/wBj+G9N8r7Ve/Z5Z/L8yVIk+SJWc5eRBwpxnJ4BNcp8Df2o/hj+0kdbHw58Tf8ACRHRfJ+35sLq18nzvM8v/XxJuz5Un3c4284yM/lZ+1F/wVb/AOGlPgV4m+HH/Crv+Ec/tr7L/wATP/hIPtXk+TdRT/6r7Km7PlbfvDG7POMH1T/ghmf+S2Z/6gn/ALf0AfK3/BUf/k+z4m/9wz/012lfun8Uvin4Y+C3gTU/GXjLU/7H8N6b5X2q9+zyz+X5kqRJ8kSs5y8iDhTjOTwCa+Kv2ov+CUv/AA0p8dfE3xH/AOFo/wDCO/219l/4lv8Awj/2ryfJtYoP9b9qTdnyt33RjdjnGT9VftR/A3/hpP4FeJvhx/bf/CO/219l/wCJl9k+1eT5N1FP/qt6bs+Vt+8Mbs84wQD4A/bp/wCNk3/CFf8ADOX/ABcT/hC/t39vZ/4lf2P7X9n+zf8AH95Pmb/slx/q923Z82Ny5/Nf4pfC3xP8FvHep+DfGWmf2N4k03yvtVl58U/l+ZEkqfPEzIcpIh4Y4zg8giv3R/YZ/YZ/4Yv/AOE2/wCK2/4TH/hJfsX/ADCfsP2b7P8AaP8ApvLv3faPbG3vnj8rP+Co3/J9fxN/7hn/AKbLSgD9Vf8AgqP/AMmJ/E3/ALhn/p0tK+AP+CUv7UXwx/Zr/wCFo/8ACx/E3/CO/wBtf2X9g/0C6uvO8n7X5v8AqIn2482P72M7uM4OP1T/AGo/gb/w0n8CvE3w4/tv/hHf7a+y/wDEy+yfavJ8m6in/wBVvTdnytv3hjdnnGD8A/8ADjPj/ktn/lqf/dtAH1V/w9F/Zj/6KYf/AAQap/8AI1H/AA9F/Zj/AOimH/wQap/8jV8q/wDDjL/qtn/lqf8A3bR/w4y/6rZ/5an/AN20AfVX/D0X9mM8D4lnP/YA1T/5Gr8gP29fij4Y+NH7WPjnxl4N1P8Atjw3qf2H7Je/Z5YPM8uwt4n+SVVcYeNxyozjI4INfav/AA4z/wCq2/8Alqf/AHbSf8OMv+q2f+Wp/wDdtAH6q0UUUAFFFFABRRSHpQAtfP8A+3r8UfE/wX/ZO8c+MvBup/2N4k0z7D9kvfs8U/l+Zf28T/JKrIcpI45U4zkcgGvyB/4Kj/8AJ9nxN/7hn/prtK/f6gD8AP8Ah6L+04OB8TOP+wBpf/yNR/w9G/ad/wCimf8AlA0v/wCRq/f48CvyA/b0/YM+Ovxq/ax8deMvBngb+2fDepfYfst7/a9hB5nl2FvE/wAks6uMPG45UZxkcEGgD9AP29fij4n+C/7J3jnxl4N1P+xvEmmfYfsl79nin8vzL+3if5JVZDlJHHKnGcjkA18//wDBKX9qL4nftJj4oj4jeJv+EiGi/wBl/YP9AtbXyfO+1+Z/qIk3Z8qP72cbeMZOfzW/YK+KPhj4L/tY+BvGXjLU/wCx/Demfbvtd79nln8vzLC4iT5IlZzl5EHCnGcngE19Af8ABVr9qL4Y/tJ/8Ku/4Vz4m/4SI6L/AGp9v/0C6tfJ877J5f8Ar4k3Z8qT7ucbecZGQDyr/gqP/wAn2fE3/uGf+mu0pP8Ah6N+07/0Uz/ygaX/API1fqr/AMEuP+TE/hl/3E//AE6XdfhZ8Lfhb4n+NPjvTPBvg3TP7Z8Sal5v2Wy8+KDzPLieV/nlZUGEjc8sM4wOSBQB78f+Cov7ThGD8TMj/sAaX/8AI1eA/FL4peJ/jT471Pxl4y1P+2fEmpeV9qvfIig8zy4kiT5IlVBhI0HCjOMnkk179/w66/acxn/hWgx1z/b+mf8AyTXgPxS+Fvif4LeO9T8G+MtM/sbxJpvlfarLz4p/L8yJJU+eJmQ5SRDwxxnB5BFAH7+/t6/FHxP8F/2TvHPjLwbqf9jeJNM+w/ZL37PFP5fmX9vE/wAkqshykjjlTjORyAa/Cz45ftSfE/8AaR/sT/hY3ib/AISL+xfO+wf6Ba2vk+d5fmf6iJN2fKj+9nG3jGTnqv2Cvij4Y+C/7WPgbxl4y1P+x/Demfbvtd79nln8vzLC4iT5IlZzl5EHCnGcngE1+6fwM/aj+GP7SR1sfDnxN/wkR0XyPt/+gXVr5PneZ5f+viTdnypPu5xt5xkZAPws+F37enx1+C3gXTPBvgzxwNG8N6b5v2Wy/siwn8vzJXlf55YGc5eRzyxxnAwABX7U/t6/FHxP8F/2TvHPjLwbqf8AY3iTTPsP2S9+zxT+X5l/bxP8kqshykjjlTjORyAa+gK+Vf8AgqP/AMmJ/E3/ALhn/p0tKAPxW+OX7UfxO/aSGiD4jeJv+EiGi+f9g/0C1tfJ87y/M/1ESbs+VH97ONvGMnP7U/8ABLj/AJMT+GX/AHE//Tpd18Af8Epf2ovhj+zX/wALR/4WP4m/4R3+2v7L+wf6BdXXneT9r83/AFET7cebH97Gd3GcHB+1J+y58Tv20Pjt4m+Mnwb8M/8ACY/DfxL9l/srWvt9rY/afs9rFazfubqWKZNs1vKnzoM7cjKkEgHwBXqvwM/aj+J37No1sfDnxN/wjo1ryPt/+gWt153k+Z5f+vifbjzZPu4zu5zgY9V/4Jcf8n2fDL/uJ/8Apru6/f3tQB4B+wV8UfE/xo/ZO8DeMvGWp/2z4k1P7d9rvfs8UHmeXf3ESfJEqoMJGg4UZxk8kmvyA/4ejftO/wDRTP8AygaX/wDI1L/wVH/5Ps+Jv/cM/wDTXaV+qn/D0X9mP/oph/8ABBqn/wAjUAeU/wDBKX9qP4nftKf8LR/4WP4m/wCEi/sX+y/sH+gWtr5Pnfa/N/1ESbs+VH97ONvGMnPwD/wVH/5Ps+Jv/cM/9NdpX6qf8PRf2Y/+imH/AMEGqf8AyNR/w9F/Zj/6KYf/AAQap/8AI1AH5Vf8PRv2nf8Aopn/AJQNL/8Akaj/AIei/tOHg/Ezj/sAaX/8jVyvxR/YL+OvwW8C6n4y8Z+Bxo3hvTfK+1Xv9r2E/l+ZKkSfJFOznLyIOFOM5OACa8Bwd2O9AH9E/wCwV8UfE/xo/ZO8DeMvGWp/2z4k1P7d9rvfs8UHmeXf3ESfJEqoMJGg4UZxk8kmj9vX4o+J/gv+yd458ZeDdT/sbxJpn2H7Je/Z4p/L8y/t4n+SVWQ5SRxypxnI5ANfK37BX7efwK+C37J3gXwb4z8cHRvEmm/bvtVl/ZF/P5fmX9xKnzxQMhykiHhjjODyCK+Kf+HXP7Tv/RM//K/pf/yTQAf8PRf2nBwPiZx/2ANL/wDkaj/h6N+07/0Uz/ygaX/8jV9//wDBKX9l34nfs1f8LRPxH8M/8I4Na/sv7B/p9rded5P2vzf9RK+3Hmx/exndxnBx9A/FH9vT4FfBfx1qfg3xl44OjeJNN8r7VZf2Rfz+X5kSSp88UDIcpIh4Y4zg4IIoA9/ooooAKKKKACkPSlpDyKAPgD9qL/glL/w0p8dfE3xH/wCFo/8ACOf219l/4lv/AAj/ANq8nybWKD/W/ak3Z8rd90Y3Y5xk/f8AmvyA/b0/bz+OvwV/ax8deDfBnjn+xvDem/Yfstl/ZFhP5fmWFvK/zywM5y8jnljjOBwAKP2C/wBvP46/Gr9rHwL4N8Z+Of7Z8N6l9u+1WX9kWEHmeXYXEqfPFArjDxoeGGcYPBIoA+1f25/25/8Ahi//AIQn/iif+Ex/4SX7b/zFvsP2f7P9n/6YS7932j2xt754+Vv+H5v/AFRP/wAuv/7ipP8AguZx/wAKTx/1G/8A2wrrP2Cv2DPgV8af2TvAvjLxn4HOs+JNS+3far3+17+DzPLv7iJPkinVBhI0HCjOMnkk0Acn/wAOMv8Aqtn/AJan/wB20p/4IZ8f8ls/8tT/AO7a+VP+Ho37Tv8A0Uz/AMoGl/8AyNR/w9F/acPB+JnH/YA0v/5GoA/an9lv4Gf8M2fAnwz8OP7b/wCEi/sX7V/xMvsn2XzvOupZ/wDVb32483b945254zgfgF+y58cv+GbPjr4Z+I/9if8ACRf2L9q/4lv2v7L53nWssH+t2Ptx5u77pztxxnI/dP8AYK+KPif40fsneBvGXjLU/wC2fEmp/bvtd79nig8zy7+4iT5IlVBhI0HCjOMnkk1yv/Drr9mP/omZ/wDB/qn/AMk0AJ+wz+3N/wANo/8ACbf8UT/wh3/CNfYv+Yr9u+0/aPtH/TCLZt+z++d3bHP5V/8ABUb/AJPr+Jv/AHDP/TZaV+1PwN/Zb+GP7Nn9tn4ceGf+Ed/tryPt/wDp91ded5PmeV/r5X2482T7uM7uc4GPxX/4Kj/8n2fE3/uGf+mu0oA8q/Zc+Bv/AA0n8dfDPw4/tv8A4R3+2vtX/Ey+yfavJ8m1ln/1W9N2fK2/eGN2ecYP3/n/AIcv/wDVYv8AhZP/AHA/7O/s/wD8CfN8z7f/ALG3yv4t3y/mt8Lfil4n+C3jvTPGXg3U/wCxvEmm+b9lvfIin8vzInif5JVZDlJHHKnGcjkA11Xxx/ak+J/7SP8AYn/CxvE3/CQ/2L532DFha2vk+d5fmf6iJN2fKj+9nG3jGTkA/f39lz45/wDDSfwK8M/Ef+xf+Ed/tr7V/wASz7X9q8nybqWD/W7E3Z8rd90Y3Y5xkr+1H8DP+Gk/gV4m+HH9t/8ACO/219l/4mX2T7V5Pk3UU/8Aqt6bs+Vt+8Mbs84wfwr+F37enx1+C3gXTPBvgzxwNG8N6b5v2Wy/siwn8vzJXlf55YGc5eRzyxxnAwABX9FNAH4Bfty/sM/8MXf8IT/xW3/CY/8ACSfbf+YT9h+zfZ/s/wD02l37vtHtjb3zx6p+y7/wVZ/4Zs+BXhn4cf8ACrv+Ej/sX7V/xM/+Eg+y+d511LP/AKr7K+3Hm7fvHO3PGcD9U/jn+y38Mf2kzoh+I/hn/hIv7F8/7B/p91a+T53l+b/qJU3Z8qP72cbeMZOfK/8Ah11+zH/0TM/+D/VP/kmgD5VH7DH/AA7YP/DR3/Cbf8LF/wCEL/5lr+yv7L+2fa/9A/4+fOm8vZ9r8z/Vtu2beM7gf8PzD0/4Un/5df8A9xV5V+y3+1H8Tv20Pjt4Z+Dfxk8Tf8Jj8N/Ev2r+1dF+wWtj9p+z2st1D++tYopk2zW8T/I4ztwcqSD9/j/gl1+zHwf+FaHP/Yf1T/5JoA+Vf+GGP+Hk/wDxkd/wm3/Cuv8AhNP+Za/sr+1Psf2T/QP+PnzofM3/AGTzP9Wu3ft527j5V+1F/wAEpf8Ahmz4FeJviP8A8LR/4SP+xfsv/Es/4R/7L53nXUUH+t+1Ptx5u77pztxxnI/X74W/Czwx8FvAmmeDfBumf2P4b03zfstl9oln8vzJXlf55WZzl5HPLHGcDgAV+APxR/b0+Ovxp8C6n4N8Z+OBrPhvUvK+1WX9kWEHmeXKkqfPFArjDxoeGGcYOQSKAPAec19/fsu/8Epv+Gk/gV4Z+I//AAtH/hHP7a+1f8Sz/hH/ALV5Pk3UsH+t+1Juz5W77oxuxzjJP+CU37L3wx/aVPxRPxH8M/8ACRHRf7L+wf6fdWvk+d9r83/USpuz5Uf3s428Yycn7Un7UfxO/Yv+O3ib4N/BvxN/wh3w38NfZf7K0X7Ba332b7RaxXU3766ilmfdNcSv87nG7AwoAAB9/wD/AAVG4/YU+Jn/AHDP/TnaV+AQ65r9/f8AgqP/AMmJ/E3/ALhn/p0tK+AP+CUn7Lvwx/aTPxQ/4WN4Z/4SI6L/AGX9gxf3Vr5Pnfa/M/1Eqbs+VH97ONvGMnIAv7Lv/BKX/hpP4FeGfiOPij/wjn9tfav+JZ/wj/2ryfJupYP9b9qTdnyt33RjdjnGT+0/HtXK/C34WeGPgt4E0zwb4N0z+x/Dem+b9lsvtEs/l+ZK8r/PKzOcvI55Y4zgcACvwr/4ejftO/8ARTP/ACgaX/8AI1AH7+nHNfAP7UX/AASm/wCGk/jr4m+I/wDwtH/hHP7a+zf8Sz/hH/tXk+TaxQf637Um7PlbvujG7HOMk/4JTftRfE79pT/haP8AwsfxN/wkX9i/2X9g/wBAtbXyfO+1+Z/qIk3Z8qP72cbeMZOfn/8Ab0/bz+OvwV/ax8deDfBnjn+xvDem/Yfstl/ZFhP5fmWFvK/zywM5y8jnljjOBwAKAP2AooooAKKKKACkJwCT0FLSHpQB4D8Uf29PgV8F/HWp+DfGXjg6N4k03yvtVl/ZF/P5fmRJKnzxQMhykiHhjjODggik/b1+F3if40fsneOfBvg3TP7Z8San9h+yWX2iKDzPLv7eV/nlZUGEjc8sM4wOSBXz/wDtRf8ABKU/tJ/HXxN8R/8AhaP/AAjn9tfZv+JZ/wAI/wDavJ8m1ig/1v2pN2fK3fdGN2OcZPlf/D83/qif/l1//cVAHqn/AASl/Zd+J37Ng+KJ+I3hn/hHRrX9l/YP9PtbrzvJ+1+Z/qJX2482P72M7uM4OPgH/gqP/wAn2fE3/uGf+mu0r6pH/Bcv0+Cf/l1//cVKf2GP+Hk5/wCGjv8AhNv+Fdf8Jp/zLX9lf2p9j+yf6B/x8+dD5m/7J5n+rXbv2843EA+Kv2Cvij4Y+C/7WPgbxl4y1P8Asfw3pn277Xe/Z5Z/L8ywuIk+SJWc5eRBwpxnJ4BNfr//AMPRf2YxwfiWc/8AYA1T/wCRq/Fb9lz4Gf8ADSfx18M/Dj+2/wDhHP7a+1f8TP7J9q8nybWWf/Vb03Z8rb94Y3Z5xg+q/ty/sNf8MX/8ITjxt/wmP/CS/bf+YV9h+zfZ/s//AE3l37vP9sbe+eAD9VP+Hov7Mf8A0Uw/+CDVP/kaj/gqP/yYn8Tf+4Z/6dLSvgD9l3/glL/w0n8CvDPxH/4Wj/wjn9tfav8AiWf8I/8AavJ8m6lg/wBb9qTdnyt33RjdjnGT9/8A/BUf/kxP4m/9wz/06WlAHwB/wSl/ai+GP7Nn/C0f+FjeJv8AhHTrX9l/YP8AQLq687yftfmf6iJ9uPNj+9jO7jODj5//AG9fij4Y+NH7WPjnxl4N1P8Atjw3qf2H7Je/Z5YPM8uwt4n+SVVcYeNxyozjI4INeAjNff8A+y7/AMEpf+GlPgV4Z+I//C0f+Ec/tr7T/wASz/hH/tXk+TdSwf637Um7PlbvujG7HOMkA800f/glb+0tqOqW1rceA7bSoJpAj3t3rtg0MAJ++4imdyB/sqx9Aa+rNF/4Io+G/sMaan8V9RutQRQLj+ztGURo/dRmRjx05weOgzgfUn7GP7Zj/tmeEfFepJ4SPgw6RdwWQVdU+2tJ5qk7w3kx7SPoa+oIYkgjWONQiKMBQOBQB+ZX/Dk7wX/0U3xD/wCCdP8AGvuT9pH4O6f+0f8ABfxF8O7/AFK90a01n7Pvvra0aSSPybmKcYUgA5MQH0NepFsHrRu96APzK/4cn+DM5/4Wb4gz/wBgdP8AGvuP9m74O6f+zh8F/Dvw7sNSvdZtNG+0bL65tGjkk865lnOVAIGDKR9BXqWR60vUUAeUftNfCDT/ANor4I+JPh7f6vceH7TWPsxk1FLUyGEQ3MU/3W2g5MQHJHWvCP8Agn3+xp4i/Y98VfFa11PVrPxD4e1xNKk0jWLVfKa4WL7X5okhLMY2XzU/iZSGBDE5A+zs5rzD49fFMfAH4ReNPHSaX/bceh2i366b9o+ziQlghUSbH2jvnaeSaAOQ+KP7enwK+C/jrU/BvjLxwdG8Sab5X2qy/si/n8vzIklT54oGQ5SRDwxxnBwQRX4q/sFfFHwx8F/2sfA3jLxlqf8AY/hvTPt32u9+zyz+X5lhcRJ8kSs5y8iDhTjOTwCa+1f+GGP+Hk3/ABkb/wAJv/wrr/hM/wDmWv7K/tT7H9j/ANA/4+fOg8zf9k8z/VLt37edu4+VftRf8Epf+Ga/gV4m+I//AAtH/hI/7F+zf8Sz/hH/ALL53nXUUH+t+1Ptx5u77pztxxnIAPv/AP4ei/sxjj/hZZz/ANgDVP8A5Gr4A/ak/Zc+J37aHx28TfGT4N+Gf+Ex+G/iX7L/AGVrX2+1sftP2e1itZv3N1LFMm2a3lT50GduRlSCfgHnPFfv7/wS5H/GCnwz/wC4n/6c7ugD4A/Zb/Zc+J37F/x28M/GT4yeGf8AhDvhv4a+1f2rrX2+1vvs32i1ltYf3NrLLM+6a4iT5EON2ThQSPVP25/+Nk//AAhP/DOX/Fxf+EL+3f29/wAwv7H9r+z/AGb/AI/fJ8zf9kuP9Xuxs+bG5c+V/tRf8FWh+0n8CvE3w4/4Vd/wjn9tfZf+Jn/wkH2ryfJuop/9V9lTdnytv3hjdnnGD6p/wQ05/wCF2Z5/5An/ALf0Aerfst/tR/DH9i/4E+Gfg38ZPE3/AAh3xI8Nfav7V0X+z7q++zfaLqW6h/fWsUsL7obiJ/kc43YOGBA/IH4W/C3xP8afHemeDfBumf2z4k1Lzfstl58UHmeXE8r/ADysqDCRueWGcYHJAr9ff2ov+CU3/DSfx18TfEf/AIWj/wAI5/bX2b/iWf8ACP8A2ryfJtYoP9b9qTdnyt33RjdjnGSfsu/8Epf+Ga/jr4Z+I/8AwtH/AISP+xftP/Et/wCEf+y+d51rLB/rftT7cebu+6c7ccZyADyr9hc/8O2f+E2P7Rv/ABbseNPsP9g/8xT7Z9k+0faf+PHz/L2fa7f/AFm3dv8AlztbH1X/AMPRf2Y/+imH/wAEGqf/ACNTf25v2Gh+2j/whP8AxW3/AAh3/CNfbf8AmE/bvtH2j7P/ANN4tm37P753dsc/ix+1H8DP+GbPjr4m+HA1s+Iv7F+y/wDEz+yfZfO861in/wBVvfbjzdv3jnbnjOAAf0p0UUUAFFFFABRRRQAV8q/8Ouv2Y/8AomZ/8H+qf/JNfVVfAH7Un7Ufwx/bQ+BPib4N/BvxN/wmPxI8S/Zf7K0X+z7qx+0/Z7qK6m/fXUUUKbYbeV/ncZ24GWIBAPVT/wAEuv2Y/wDomf8A5X9U/wDkmvgD9qT9qP4nfsX/AB28TfBv4N+Jv+EO+G/hr7L/AGVov2C1vvs32i1iupv311FLM+6a4lf53ON2BhQAPVP2GP8AjWx/wm3/AA0b/wAW6/4TT7F/YP8AzFPtn2T7R9p/48vO8vZ9rt/9Ztzv+XO1seV/tSfsufE79tD47eJvjJ8G/DP/AAmPw38S/Zf7K1r7fa2P2n7PaxWs37m6limTbNbyp86DO3IypBIB9VftSfsufDH9i/4E+JvjJ8G/DP8Awh3xI8NfZf7K1r+0Lq++zfaLqK1m/c3UssL7obiVPnQ43ZGGAI/Kv45ftS/E/wDaS/sT/hY3ib/hIv7F8/7B/oFra+T53l+Z/qIk3Z8qP72cbeMZOfqr9lv9lz4nfsX/AB28M/GT4yeGf+EO+G/hr7V/autfb7W++zfaLWW1h/c2sssz7priJPkQ43ZOFBIP+CrP7UXwx/aU/wCFXf8ACuPE3/CRf2L/AGp9v/0C6tfJ877J5f8Ar4k3Z8qT7ucbecZGQD5/+F37enx1+C3gXTPBvgzxwNG8N6b5v2Wy/siwn8vzJXlf55YGc5eRzyxxnAwABX6//wDBUf8A5MT+Jv8A3DP/AE6WleAfsFft5/Ar4LfsneBfBvjPxwdG8Sab9u+1WX9kX8/l+Zf3EqfPFAyHKSIeGOM4PIIr6p/b1+F3if40fsneOfBvg3TP7Z8San9h+yWX2iKDzPLv7eV/nlZUGEjc8sM4wOSBQB+a3/BKT9l34Y/tJn4of8LG8M/8JEdF/sv7Bi/urXyfO+1+Z/qJU3Z8qP72cbeMZOf1++Fvws8MfBbwJpng3wbpn9j+G9N837LZfaJZ/L8yV5X+eVmc5eRzyxxnA4AFfhX/AMOuv2nDyPhnx/2H9L/+SaP+HXP7Tv8A0TP/AMr+l/8AyTQB9kf8ETf+SY/E3/sN2H/oBr9NSSBXwp/wS8/Zv+Iv7OPgTx1p/wARPDv/AAj13qWq2c9pH9ttrrzEVSrHMEjgYJHBwa+5p2IibaSGxgEDP6UAcx4u8aab4WRrjVLv+y1yEjlmYLHKeoAZiEz14JBwDiuC1L4karqWm6hq2kXyavpyfuUttJiIurd2I2M6NkuPdSMggjjkeO/F39qW88NatrOgeIPDk1rai+trG0l1S2dI7qEzRRXLNxtIGXKspdeVy2SAeY8P69p3hC+mm8IzWB85CrR6d4jkubhlUlgREsD425OC2TjPvXoRwzcLta9Oz/yPjcbmD9qoqVoap7pp9+zX3H0h4R+Id1dpNDcarFHfQ432moSLDMv+y0ZJI9Rz6+2fSbLXZpY0WS2JnOOInBRucHDZxxycdeO9fDfxK+I3/CYWsA/s9ZvEscYaC+QQk3MW04SeF5U4J6TRFxhRz95BseBfiFr3hazhvfEHhptD0uUSIjpcu9tbSBd4yQzAghSACxzkggHis1hHJ6Oz7f5HNTzWrhE3JOUF9rrbu1Z/nY+5Q2a5H4k+A9C+KHhLXvC3iax/tLQdUtY4Lu086SLzU3k43xsrDkDkEVt+GNYg8QeH9N1S2Ia2vLaO4jI6bXUMP0NS3f8ArLn/AK5R/wDoTVyNWdj7iElOKnHZn4yftQ/tSfE/9i/46eJfg38G/E3/AAh/w38N/Zf7K0X+z7W++zfaLWK6m/fXUUsz7priV/nc43YGAAB4D8Uf29Pjr8afAup+DfGfjgaz4b1LyvtVl/ZFhB5nlypKnzxQK4w8aHhhnGDkEiv6J0+4v0r8WP2W/wBlz4nfsX/Hbwz8ZPjJ4Z/4Q74b+GvtX9q619vtb77N9otZbWH9zayyzPumuIk+RDjdk4UEhFi/8Epf2Xvhl+0o3xRPxH8M/wDCRHRf7LNgft91a+T532vzP9RKm7PlR/ezjbxjJz+vvwt+Fnhj4LeBNM8G+DdM/sfw3pvm/ZbL7RLP5fmSvK/zysznLyOeWOM4HAArwD/h6L+zHj/kpn/lA1T/AORq+AP2pP2XPid+2h8dvE3xk+Dfhn/hMfhv4l+y/wBla19vtbH7T9ntYrWb9zdSxTJtmt5U+dBnbkZUgkA+AK9V+Bn7UnxP/ZsGt/8ACuPE3/CO/wBteR9v/wBAtbrzvJ8zyv8AXxPtx5sn3cZ3c5wMeq/8EuP+T7Phl/3E/wD013dff3/BVr9lz4nftKf8Ku/4Vx4Z/wCEi/sX+1Pt/wDp9ra+T532Tyv9fKm7PlSfdzjbzjIyAfAH/D0b9p3/AKKZ/wCUDS//AJGr9f8A9vX4o+J/gv8AsneOfGXg3U/7G8SaZ9h+yXv2eKfy/Mv7eJ/klVkOUkccqcZyOQDXgH7Lf7Ufwx/Yv+BPhn4N/GTxN/wh3xI8Nfav7V0X+z7q++zfaLqW6h/fWsUsL7obiJ/kc43YOGBA/P8A/wCHXP7Tv/RM/wDyv6X/APJNAH39/wAEpf2ofib+0oPiiPiP4m/4SIaL/ZYsB9gtbXyfO+1+b/qIk3Z8qP72cbeMZOfoL4o/sF/Ar40eOtT8ZeMvA51nxJqXlfar3+17+DzPLiSJPkinVBhI0HCjOMnJJNfFP7C//Gtj/hNv+Gjv+Ldf8Jp9h/sH/mKfbPsn2j7T/wAePn+Xs+1W/wDrNu7f8udrY+q/+Hov7Mf/AEUw/wDgg1T/AORqAPqqiiigAooooAKKKQ8igAzX81v7Lnxz/wCGbPjr4Z+I/wDYn/CR/wBi/av+JZ9r+y+d51rLB/rdj7cebu+6c7ccZyPtP9vT9vP46/BX9rHx14N8GeOf7G8N6b9h+y2X9kWE/l+ZYW8r/PLAznLyOeWOM4HAAr5W/YK+F3hj40ftY+BvBvjLTP7Y8N6n9u+12X2iWDzPLsLiVPniZXGHjQ8MM4weCRQB1f7cv7c3/DZ//CE48E/8Id/wjX23/mK/bvtH2j7P/wBMItm37P753dsc+q/su/8ABVn/AIZs+BXhn4cf8Ku/4SP+xftX/Ez/AOEg+y+d511LP/qvsr7cebt+8c7c8ZwPv/8A4ddfsxnk/DQ5/wCw/qn/AMk0f8Ouv2Y/+iZn/wAH+qf/ACTQB8qf8Nz/APDyf/jHH/hCf+Fdf8Jp/wAzL/av9qfY/sn+n/8AHt5MPmb/ALJ5f+sXbv3c7dpX/hxpn/mtn/lqf/dtfmt8Lfil4n+C3jvTPGXg3U/7G8Sab5v2W98iKfy/MieJ/klVkOUkccqcZyOQDX6+/wDBKX9qL4nftJj4oj4jeJv+EiGi/wBl/YP9AtbXyfO+1+Z/qIk3Z8qP72cbeMZOQD8rP2o/gZ/wzX8dfE3w4/tv/hIv7F+y/wDEy+yfZfO861in/wBVvfbjzdv3jnbnjOB9/f8AD83/AKon/wCXX/8AcVfK3/BUf/k+z4m/9wz/ANNdpXyrQB+/v7DP7cw/bQ/4TX/iif8AhDv+Ea+xf8xX7d9p+0faP+mEWzb5Hvnd2xz9VcV/Nb8DP2o/id+zaNbHw58Tf8I6Na8j7f8A6Ba3XneT5nl/6+J9uPNk+7jO7nOBj90/2Cvij4n+NH7J3gbxl4y1P+2fEmp/bvtd79nig8zy7+4iT5IlVBhI0HCjOMnkk0Ac7+xZ+2WP2yfDHibVx4P/AOEQ/sW/tbXyf7T+2+dv+bdnyY9uMYxg19B+N4byfwtqP2DU00a9SPzIb6VA8cTKQwLg9U4w3TgnBBwa/Of/AIIm/wDJMfib/wBhuw/9ANfphc28d1bvDKiyROCro4yGBGCCO4prcmSumj5C8Y678Q7a81BviLo+n6z4aaC1MY8MXsu2znimcs7RNtlIZZUDA5j2R4Y4zn52sPAOoaz8QdQtvBenX9joq3C3G/7Jb+XDhVG37Q75WM43BWVWBwRkopH1p8Rv2aB4Stjqvwxsrq1uYiZm0W31B0hdgQQYRK7RxMOQAqjg4DL0Pzhq1pqvxD1dLXWLjx9pG+Xbd6LPYTBBKw4KmUbI8EkluU5ONq8V7OGSfvU2v68j4jMZVKL5aqbXR7/jY2PEXwQ8E6zrTS6b8SNP8Pa7K++9srOSS6tXYjLEA4+clQSGZhyTgE5qh+09q/8AYmleGdA8G+KLaLSLnTZbC80+YQyRXbx7GWaOE5YSMWYll+b5Ewc4B57xN8Cfgp4LittM1Hx/cw6hPKbi8jWVbi5QENhPPVkjjbdgncGzjGOVNexeCf2dtLv4NG1Pwv4Gt77R4pFv4dZn1X/TLhwV2uouIpQW4B6oOBh8dH8E41pN2W11occY1KkXRpJXa1te/wA/+GPpb9nuG4tPg94VsrqZJ7iy063tJdqsrJLHEiSowYA5EiuOnTFdxd/6y5/65R/+hNVXw+1xEr290JHnREcysm1GyMHbycHKnK5OMjsRXlX7Yvj3Xfhf+zj8SPFXhm+/szXtL0qOe0u/Jjl8p/NxnZIrKeCeoIrx5vmk2ffUI8lOMeyPbYzlB9K8r/aj+Bn/AA0l8CvE3w5/tv8A4R3+2vsv/Ez+yfavJ8m6in/1W9N2fK2/eGN2ecYPKfsEfFDxP8Z/2TvA3jLxjqf9seI9S+3far37PFB5nl31xEnyRKqDCRoOFGcZPJJpf29fij4n+C/7J3jnxl4N1P8AsbxJpn2H7Je/Z4p/L8y/t4n+SVWQ5SRxypxnI5ANQbnxV/w407/8Lt/8tT/7tpf+G5/+HbH/ABjl/wAIT/wsX/hC/wDmZf7V/sv7Z9r/ANP/AOPbyJvL2fa/L/1jbtm7jdtHqf8AwSl/ah+Jv7Sg+KI+I/ib/hIhov8AZYsB9gtbXyfO+1+b/qIk3Z8qP72cbeMZOfoL4o/sF/Ar40eOtT8ZeMvA51nxJqXlfar3+17+DzPLiSJPkinVBhI0HCjOMnJJNAH4V/sufHP/AIZr+Ovhn4j/ANif8JF/Yv2r/iW/a/svnedaywf63Y+3Hm7vunO3HGcj7+P/AAXMB/5on/5df/3FX5WUUAeq/tR/HT/hpP46+JviP/Yn/CO/219l/wCJZ9r+1eT5NrFB/rdibs+Vu+6Mbsc4yf6UuPav5WK+qf8Ah6N+07/0Uz/ygaX/API1AH1X/wAFzOvwTx/1G/8A2wryv9l3/glL/wANKfArwz8R/wDhaP8Awjn9tfav+JZ/wj/2ryfJupYP9b9qTdnyt33RjdjnGT6p+wuf+Hk58bH9o3/i4h8F/Yf7B/5hf2P7X9o+0/8AHj5Hmb/slv8A6zdt2fLjc2f0p+Fvws8MfBbwJpng3wbpn9j+G9N837LZfaJZ/L8yV5X+eVmc5eRzyxxnA4AFAHV0UUUAFFFFABSE4BJ6ClpD0NAHgPxR/b0+BXwX8dan4N8ZeODo3iTTfK+1WX9kX8/l+ZEkqfPFAyHKSIeGOM4OCCK9U+KXxT8MfBbwJqfjLxlqf9j+G9N8r7Ve/Z5Z/L8yVIk+SJWc5eRBwpxnJ4BNfhZ/wVG/5Pr+JmP+oZ/6bLSvVv2ov+CrX/DSfwK8TfDg/C7/AIRz+2vsv/Ez/wCEg+1eT5N1FP8A6r7Km7PlbfvDG7POMEA/VP4G/tR/DH9pI62Phz4m/wCEiOi+R9vzYXVr5PneZ5f+viTdnypPu5xt5xkZ9Vr8Af2Gf25/+GLv+E2/4on/AITH/hJPsX/MW+w/Zvs/2j/phLv3faPbG3vnj6q/4fnf9UT/APLr/wDuKgD6q/4ei/sx/wDRTD/4INU/+Rq+AP8Agq1+1F8Mf2lP+FXf8K48Tf8ACRf2L/an2/8A0C6tfJ877J5X+viTdnypPu5xt5xkZ+Vf2XPgZ/w0n8dfDPw4/ts+Hf7a+1f8TP7J9q8nybWWf/Vb03Z8rb94Y3Z5xg+q/tzfsND9i/8A4Qn/AIrb/hMf+El+3f8AMK+w/Zvs/wBn/wCm0u/d9o9sbe+eAD9VP+CXH/Jifwy/7if/AKdLuvyr/wCCXH/J9nwy/wC4n/6a7uvVv2Xf+CrX/DNfwK8M/DgfC7/hI/7F+1f8TL/hIPsvneddSz/6r7K+3Hm7fvHO3PGcD6o/Zd/4JS/8M1/HXwz8R/8AhaP/AAkf9i/av+Jb/wAI/wDZfO861lg/1v2p9uPN3fdOduOM5AB9VfHL9qT4Y/s2f2IPiP4m/wCEd/trz/sH+gXV153k+X5v+oifbjzY/vYzu4zg48r/AOHov7Mf/RTD/wCCDVP/AJGpv7c37DX/AA2j/wAIT/xW3/CHf8I19t/5hP277R9o+z/9N4tm37P753dsc/ix+1H8DP8Ahmz46+JvhwNbPiL+xfsv/Ez+yfZfO861in/1W99uPN2/eOdueM4AB+sn/BMH9nT4ifs1+AfHtl8R/D3/AAjlxqGpWl3bKb62uQ8SKQ7ZhkcDHvivu4cgVQ1RzAjOYvPiKkPEADuGPfivNfiN+0d8OPgn/Z8fjLxxZeHF1DzPsaanFIS4j27wpUchd69c/eHNAHrJAJrD8Q+CNA8VqV1jRrHU/l27rm3V2A9ASMivD/8Ah4F8AP8Aornh7/vzN/hXsv8AwnOlH/mL23/gPJ/jQrrYTSejMHQf2d/hl4avVvNN8B+H7e8Vt63H9nRNIp9VZgSD9K9DVAoA7DtXN/8ACc6Vn/kL23/gPJ/jXmXjH9tD4NfD7xHd6B4i+JeiaTrNps8+zuIZg8e5Fdc49VZT+NU5Sl8TuTGEYaRVj3PpXiH7YfgfW/in+zj8SvCvheyGqa9qOmR2traCaOLzJPMDbd8jKq/LzyR1rnbn/goJ+z+IWz8X9CT/AGooJiw+g2n+RqX9nj9rfwd+1B4v8aaP4BS8u9C8L/Ynm1y6jMQ1GS48/lI2AdVXyPvOASTgKAoLSWeG/stftR/DH9i/4EeGfg38ZPE3/CH/ABI8N/av7V0X+z7q++z/AGi6luof31rFLC+6G4if5HON2Dgggff9fgB/wVF4/bq+JgH/AFDP/TZaV+1P7Ufxz/4Zs+BXib4j/wBi/wDCRf2L9l/4ln2v7L53nXUUH+t2Ptx5u77pztxxnIAD45/tSfDH9mw6IPiP4m/4R3+2vP8AsH+gXV153k+X5v8AqIn2482P72M7uM4OPK/+Hov7Mf8A0Uw/+CDVP/kavlX/AJTQ/wDVHf8AhW3/AHHP7R/tD/wG8ry/sH+3u83+Hb83wB+1H8Df+Ga/jr4m+HH9t/8ACRf2L9l/4mX2T7L53nWsU/8Aqt77cebt+8c7c8ZwAD90/wBvX4XeJ/jR+yd458G+DdM/tnxJqf2H7JZfaIoPM8u/t5X+eVlQYSNzywzjA5IFfkB/w66/acxn/hWfHXP9v6X/APJNfv8AGvlT9ub9uYfsX/8ACFY8E/8ACY/8JJ9t/wCYr9h+zfZ/s/8A0wl37vP9sbe+eADrP2Cvhd4n+C/7J3gbwb4y0z+xvEmmfbvtdl9oin8vzL+4lT54mZDlJEPDHGcHkEVyv/BUf/kxP4m/9wz/ANOlpXqn7Lfxz/4aT+BPhn4j/wBif8I7/bX2r/iW/a/tXk+TdSwf63Ym7PlbvujG7HOMk/ak+Bn/AA0n8CfE3w4/tv8A4R3+2vsv/Ey+yfavJ8m6in/1W9N2fK2/eGN2ecYIB+AXwM/Zb+J/7Sf9tn4ceGf+Ei/sXyPt/wDp9ra+T53meV/r5U3Z8qT7ucbecZGf1U/Zb/aj+GP7F/wJ8M/Bv4yeJv8AhDviR4a+1f2rov8AZ91ffZvtF1LdQ/vrWKWF90NxE/yOcbsHDAgeU4/4cwcf8li/4WT/ANwP+zv7P/8AAnzfM+3/AOxt8r+Ld8q/8MMf8PJ/+Mjf+E2/4V1/wmn/ADLX9lf2p9j+yf6D/wAfPnQ+Zv8Asnmf6tcb9vONxAP1UooooAKKKKACkIyCD0NLSHpQB4D8Uf2C/gV8aPHWp+MvGXgc6z4k1LyvtV7/AGvfweZ5cSRJ8kU6oMJGg4UZxk5JJr5V/b1/YM+BXwW/ZO8deMvBngc6N4k037D9lvf7Xv5/L8y/t4n+SWdkOUkccqcZyOQDXxV/wVH/AOT7Pib/ANwz/wBNdpX7p/FL4p+GPgt4E1Pxl4y1P+x/Dem+V9qvfs8s/l+ZKkSfJErOcvIg4U4zk8AmgD+YInJor+lL4HftSfDH9pH+2/8AhXPib/hIf7F8n7fmwurXyfN8zy/9fEm7PlSfdzjbzjIzy3xR/b0+BXwX8dan4N8ZeODo3iTTfK+1WX9kX8/l+ZEkqfPFAyHKSIeGOM4OCCKAPn/9qT9lz4Y/sX/AnxN8ZPg34Z/4Q74keGvsv9la1/aF1ffZvtF1FazfubqWWF90NxKnzocbsjDAEeVfsMf8bJz42/4aO/4uL/whf2H+wf8AmF/Y/tf2j7T/AMePk+Zv+yW/+s3bdny43Nnyr9lv9lz4nfsX/Hbwz8ZPjJ4Z/wCEO+G/hr7V/autfb7W++zfaLWW1h/c2sssz7priJPkQ43ZOFBI9U/bn/42T/8ACE/8M5f8XF/4Qv7d/b3/ADC/sf2v7P8AZv8Aj98nzN/2S4/1e7Gz5sblyAfVf/Drr9mP/omZ/wDB/qn/AMk1+VX/AA9G/ad/6KZ/5QNL/wDkav0A/Zb/AGo/hj+xf8CfDPwb+Mnib/hDviR4a+1f2rov9n3V99m+0XUt1D++tYpYX3Q3ET/I5xuwcMCB+K1AH7Uf8Epf2ovid+0r/wALRHxH8Tf8JGNF/sv7B/oFra+T532vzf8AURJuz5Uf3s428Yyc/QPxR/YL+BXxo8dan4y8ZeBzrPiTUvK+1Xv9r38HmeXEkSfJFOqDCRoOFGcZOSSa/NX/AIJSftRfDH9mv/haP/Cx/E3/AAjv9tf2X9g/0C6uvO8n7X5v+oifbjzY/vYzu4zg4+//APh6L+zH/wBFMP8A4INU/wDkagD6oZA4wRkV5J8bv2T/AIWftGNo7fELwx/b50jzvsW2/ubXyvN2eZ/qJE3Z8qP72cbeMZOf5tK9V+Bn7LfxO/aTGtn4ceGf+Ei/sXyPt/8Ap9ra+T53meV/r5U3Z8qT7ucbecZGQD9pv+HWv7Mn/RN3/wDB9qX/AMk18M/sF/tn/GP41ftYeBvBnjLxcmseG9S+3farL+yLGDzPLsLiVPnigVxh40PBGcYPBIr6U/Zb/aj+GP7F/wACfDPwb+Mnib/hDviR4a+1f2rov9n3V99m+0XUt1D++tYpYX3Q3ET/ACOcbsHDAgfQHwu/b0+BXxo8daZ4N8G+ODrPiTUvN+y2X9kX8HmeXE8r/PLAqDCRueWGcYGSQKAPmn/gqx8fPHf7NQ+F/wDwrnW08P8A9t/2p9vzYW1153k/ZPL/ANdG+3HmyfdxndznAx+SXxK+JPiP4veNNQ8WeLNQGqeINQ8sXN2LeKAP5caxJ8kSqgwiKOAM4yeSTX9Fnxy/al+GH7Nv9if8LG8Tf8I7/bXn/YP9AurrzvJ8vzP9RE+3Hmx/exndxnBx5X/w9F/Zj/6KYf8AwQap/wDI1AH4A5xXqnwN/ai+J37Nv9tf8K58S/8ACO/2z5P2/wD0C1uvO8nzPL/18T7cebJ93Gd3OcDHLfC34W+J/jT470zwb4N0z+2fEmpeb9lsvPig8zy4nlf55WVBhI3PLDOMDkgV+lH7C5/4ds/8Jsf2jf8Ai3Y8afYf7B/5in2z7J9o+0/8ePn+Xs+12/8ArNu7f8udrYAPzW+KXxS8T/Gnx3qfjLxlqf8AbPiTUvK+1XvkRQeZ5cSRJ8kSqgwkaDhRnGTySa9V+KP7enx1+NPgXU/BvjPxwNZ8N6l5X2qy/siwg8zy5UlT54oFcYeNDwwzjByCRX6//wDD0X9mP/oph/8ABBqn/wAjUf8ABUf/AJMT+Jv/AHDP/TpaUAfKv/BDM5PxtJ7/ANif+39fKv8AwVH/AOT7Pib/ANwz/wBNdpXqv/BKT9qL4Y/s2H4of8LG8Tf8I6da/sv7BiwurrzvJ+1+Z/qIn2482P72M7uM4OP1++FvxT8MfGnwJpnjLwbqf9seG9S837Le/Z5YPM8uV4n+SVVcYeNxyozjI4INAHlX7evxR8T/AAX/AGTvHPjLwbqf9jeJNM+w/ZL37PFP5fmX9vE/ySqyHKSOOVOM5HIBr8LPjl+1H8Tv2khog+I3ib/hIhovn/YP9AtbXyfO8vzP9REm7PlR/ezjbxjJz9p/sF/sGfHX4K/tY+BfGXjPwN/Y3hvTft32q9/tewn8vzLC4iT5Ip2c5eRBwpxnJ4BNfQH/AAVZ/Zd+J37Sn/Crv+FceGf+Ei/sX+1Pt/8Ap9ra+T532Ty/9fKm7PlSfdzjbzjIyAerf8EuP+TE/hl/3E//AE6XddV+3r8UfE/wX/ZO8c+MvBup/wBjeJNM+w/ZL37PFP5fmX9vE/ySqyHKSOOVOM5HIBrwD9lv9qP4Y/sX/Anwz8G/jJ4m/wCEO+JHhr7V/aui/wBn3V99m+0XUt1D++tYpYX3Q3ET/I5xuwcMCB6r/wAFR/8AkxP4m/8AcM/9OlpQB8qfsMf8bJh42/4aN/4uJ/whf2H+wf8AmF/Y/tf2j7T/AMePk+Zv+yW/+s3bdny43Nnyv9qT9qP4nfsX/HbxN8G/g34m/wCEO+G/hr7L/ZWi/YLW++zfaLWK6m/fXUUsz7priV/nc43YGFAA+AAMmv1//YK/bz+BXwW/ZO8C+DfGfjg6N4k037d9qsv7Iv5/L8y/uJU+eKBkOUkQ8McZweQRQB+lNFFFABRRRQAUh6UtIelAH4Bf8FR/+T7Pib/3DP8A012lfqp/wVG/5MU+Jn/cM/8ATnaV+Vf/AAVH/wCT7Pib/wBwz/012lfun8UvhZ4Y+NPgTU/BvjLTP7Y8N6l5X2qy+0SweZ5cqSp88TK4w8aHhhnGDwSKAPzV/wCCGh/5LZn/AKgn/t/Xyt/wVG/5Pr+JmOn/ABLP/TZaV9Vft0H/AIdsnwT/AMM5f8W7PjT7d/b3/MU+2fZPs/2b/j+87y9n2u4/1e3dv+bO1ceq/st/sufDH9tD4E+GfjJ8ZPDP/CY/EjxL9q/tXWv7QurH7T9nupbWH9zayxQptht4k+RBnbk5YkkA8p/4bn/4eT/8Y4/8IT/wrr/hNP8AmZf7V/tT7H9k/wBP/wCPbyYfM3/ZPL/1i7d+7nbtP1T+w1+wz/wxh/wmxPjb/hMf+El+xf8AMK+w/Zvs/wBo/wCm8u/d9o9sbe+ePwu+FvxS8T/Bbx3pnjLwbqf9jeJNN837Le+RFP5fmRPE/wAkqshykjjlTjORyAa9+/4ei/tOYx/wssY6Y/sDTP8A5GoAX/gqMf8AjOv4mY/6hn/pstK8q/Zc+Bn/AA0n8dfDPw4Otf8ACO/219q/4mf2T7V5Pk2ss/8Aqt6bs+Vt+8Mbs84weV+KXxS8T/Gnx3qfjLxlqf8AbPiTUvK+1XvkRQeZ5cSRJ8kSqgwkaDhRnGTySa/f74XfsF/Ar4L+OtM8ZeDfA50bxJpvm/Zb3+17+fy/MieJ/klnZDlJHHKnGcjBANAH5AftzfsM/wDDF3/CE/8AFbf8Jj/wkv23/mE/Yfs/2f7P/wBN5d+7z/bG3vnj1X9l3/glL/w0n8CvDPxH/wCFo/8ACOf219q/4ln/AAj/ANq8nybqWD/W/ak3Z8rd90Y3Y5xk+qf8FzPl/wCFJY4x/beP/JCviv4Xft6fHX4LeBdM8G+DPHA0bw3pvm/ZbL+yLCfy/MleV/nlgZzl5HPLHGcDAAFAHgFfqn/wQzxj42Z/6gn/ALf1+Vleq/Az9qT4nfs2DWx8OPE3/CO/215H2/8A0C1uvO8nzPK/18T7cebJ93Gd3OcDAB6r/wAFRv8Ak+v4mY6f8Sz/ANNlpX39+y7/AMEpT+zZ8dfDPxH/AOFo/wDCR/2L9p/4ln/CP/ZfO861lg/1v2p9uPN3fdOduOM5C/st/sufDH9tD4E+GfjJ8ZPDP/CY/EjxL9q/tXWv7QurH7T9nupbWH9zayxQptht4k+RBnbk5Ykn7/oA+U/25v2Gf+G0P+EJ/wCK2/4Q3/hGvtv/ADCft32n7R9n/wCm8Wzb5Hvnd2xz8r/8OMv+q2f+Wp/9216p/wAFW/2ovid+zZ/wq8fDnxN/wjo1r+1Pt/8AoFrded5P2Ty/9fE+3HmyfdxndznAx9AfsFfFHxP8aP2TvA3jLxlqf9s+JNT+3fa737PFB5nl39xEnyRKqDCRoOFGcZPJJoA+f/2Xf+CUv/DNnx18M/Ef/haP/CR/2L9q/wCJb/wj/wBl87zrWWD/AFv2p9uPN3fdOduOM5HlX/Bcz/mif/cb/wDbCvlX/h6N+07/ANFM/wDKBpf/AMjV5X8c/wBqT4n/ALSf9if8LH8Tf8JF/Yvn/YP9AtbXyfO8vzf9REm7PlR/ezjbxjJyAfVf7Lv/AASl/wCGlPgV4Z+I4+KP/COf219q/wCJb/wj/wBq8nybqWD/AFv2pN2fK3fdGN2OcZP39/wVG5/YU+Jn/cM/9OdpR/wS4/5MT+GX/cT/APTpd1+QHxR/b0+Ovxp8C6n4N8Z+OBrPhvUvK+1WX9kWEHmeXKkqfPFArjDxoeGGcYOQSKAPAQDniv39/wCCXJ/4wU+Gf/cT/wDTnd18Af8ABKb9l74Y/tKn4on4j+Gf+EiOi/2X9g/0+6tfJ877X5v+olTdnyo/vZxt4xk5P2pP2o/id+xf8dvE3wb+Dfib/hDvhv4a+y/2Vov2C1vvs32i1iupv311FLM+6a4lf53ON2BhQAAD9qOKQgYr8Av+Ho37Tv8A0Uz/AMoGl/8AyNR/w9G/ad/6KZ/5QNL/APkagD7/AP2ov+CU3/DSfx18TfEf/haP/COf219m/wCJZ/wj/wBq8nybWKD/AFv2pN2fK3fdGN2OcZP1V+1J8DP+Gk/gT4m+HH9t/wDCO/219l/4mX2T7V5Pk3UU/wDqt6bs+Vt+8Mbs84wfxW/4ejftO/8ARTP/ACgaX/8AI1fr/wDt6/FHxP8ABf8AZO8c+MvBup/2N4k0z7D9kvfs8U/l+Zf28T/JKrIcpI45U4zkcgGgD8gf25f2GR+xd/whP/Fbf8Jj/wAJJ9t/5hX2H7N9n+z/APTeXfu8/wBsbe+ePVv2Xf8AglL/AMNKfArwz8Rx8Uf+Ec/tr7V/xLf+Ef8AtXk+TdSwf637Um7PlbvujG7HOMn5U+OX7UfxO/aSGiD4jeJv+EiGi+f9g/0C1tfJ87y/M/1ESbs+VH97ONvGMnP7U/8ABLj/AJMT+GX/AHE//Tpd0AfVVFFFABRRRQAUhOASegpaQ9KAPAfij+3p8Cvgv461Pwb4y8cHRvEmm+V9qsv7Iv5/L8yJJU+eKBkOUkQ8McZwcEEVyn/D0X9mP/oph/8ABBqn/wAjV+Vf/BUbj9uv4mf9wz/02WlfVX/DjL/qtn/lqf8A3bQB9Vf8PRf2Y/8Aoph/8EGqf/I1H/D0X9mP/oph/wDBBqn/AMjV8q/8OM/+q2/+Wp/920f8OMv+q2f+Wp/920AfVX/D0X9mP/oph/8ABBqn/wAjV6p8Df2pPhh+0n/bf/CufE3/AAkX9i+R9v8A9AurXyfO8zyv9fEm7PlSfdzjbzjIz+Vv7UX/AASl/wCGa/gV4m+I5+KP/CR/2L9l/wCJb/wj/wBl87zrqKD/AFv2p9uPN3fdOduOM5Hqn/BDLn/hdnp/xJP/AG/oA5P9vT9gz46/Gr9rHx14y8GeBv7Z8N6l9h+y3v8Aa9hB5nl2FvE/ySzq4w8bjlRnGRwQa9X/AG9f28/gV8af2TvHXg3wZ44Os+JNS+w/ZbL+yL+DzPLv7eV/nlgVBhI3PLDOMDkgV+lHFfzW/sufAz/hpT46+Gfhx/bf/CO/219q/wCJl9k+1eT5NrLP/qt6bs+Vt+8Mbs84wQD7/wD+CGQwfjZ/3BP/AG/r9VK+VP2Gf2Gf+GLv+E2P/Cbf8Jj/AMJL9i/5hP2H7N9n+0f9N5d+7z/bG3vnj6qzQB+a/wC3r+3n8CvjT+yd468G+DPHB1nxJqX2H7LZf2RfweZ5d/byv88sCoMJG55YZxgckCvzV+Bv7LfxO/aSGtn4c+Gf+EiGi+R9v/0+1tfJ87zPL/18qbs+VJ93ONvOMjP3/wD8OMv+q2f+Wp/920f8oX/+qxf8LK/7gf8AZ39n/wDgT5vmfb/9jb5X8W75QD81vil8LfE/wW8d6n4N8ZaZ/Y3iTTfK+1WXnxT+X5kSSp88TMhykiHhjjODyCK/dP8A4ei/sx/9FMP/AIINU/8AkavxW/aj+OX/AA0n8dfE3xH/ALE/4R3+2vsv/Et+1/avJ8m1ig/1uxN2fK3fdGN2OcZJ+y58Df8AhpT46+Gfhx/bf/CO/wBtfav+Jl9k+1eT5NrLP/qt6bs+Vt+8Mbs84wQD6q/4Kt/tRfDH9pT/AIVd/wAK48Tf8JF/Yv8Aan2//QLq18nzvsnlf6+JN2fKk+7nG3nGRn4Ar9Uz/wAEM/X42f8Alqf/AHbR/wAOMv8Aqtn/AJan/wB20AflZXqvwN/Zc+J37SQ1s/Dnwz/wkQ0XyPt/+n2tr5PneZ5f+vlTdnypPu5xt5xkZT9lz4Gf8NJ/HXwz8OP7b/4R3+2vtX/Ey+yfavJ8m1ln/wBVvTdnytv3hjdnnGD9/wCP+HL/AP1WL/hZP/cD/s7+z/8AwJ83zPt/+xt8r+Ld8oB+a/xS+Fvif4LeO9T8G+MtM/sbxJpvlfarLz4p/L8yJJU+eJmQ5SRDwxxnB5BFfan7Lf7LnxO/Yv8Ajt4Z+Mnxk8M/8Id8N/DX2r+1da+32t99m+0WstrD+5tZZZn3TXESfIhxuycKCR6p/wAMMf8ADyf/AIyO/wCE2/4V1/wmn/Mtf2V/an2P7J/oH/Hz50Pmb/snmf6tdu/bzt3H6q/4Kjf8mKfEzHX/AIln/pztKAPgH/gq1+1F8Mf2k/8AhV3/AArjxN/wkX9i/wBqfb/9AurXyfO+yeV/r4k3Z8qT7ucbecZGfgCvqv8AYa/Ya/4bR/4TbPjb/hDv+Ea+xf8AMK+3faftH2j/AKbRbNv2f3zu7Y58o/aj+Bn/AAzZ8dfE3w4/tv8A4SL+xfsv/Ey+yfZfO861in/1W99uPN2/eOdueM4AB/RT8Uvin4Y+C3gTU/GXjLU/7H8N6b5X2q9+zyz+X5kqRJ8kSs5y8iDhTjOTwCa5T4G/tR/DH9pI62Phz4m/4SI6L5P2/NhdWvk+d5nl/wCviTdnypPu5xt5xkZP2o/gb/w0n8CvE3w4/tv/AIR3+2vsv/Ey+yfavJ8m6in/ANVvTdnytv3hjdnnGD5V+wz+wz/wxf8A8Jt/xW3/AAmP/CS/Yv8AmFfYfs32f7R/03l37vtHtjb3zwAflZ/wVH/5Ps+Jv/cM/wDTXaV+v/wu/b0+BXxo8daZ4N8G+ODrPiTUvN+y2X9kX8HmeXE8r/PLAqDCRueWGcYGSQK/H/8A4Kjf8n1/E3/uGf8ApstK+/8A9l7/AIJS/wDDNfx18M/Ef/haP/CR/wBi/af+JZ/wj/2XzvOtZYP9b9qfbjzd33TnbjjOQAeVf8FzPm/4Uljv/bf/ALYV1n7BX7efwK+C37J3gXwb4z8cHRvEmm/bvtVl/ZF/P5fmX9xKnzxQMhykiHhjjODyCK5P/guZ/wA0Tx/1G/8A2wr8rOaAP6qKKKKACiiigApD0paQ9KAPwC/4Kj/8n2fE3/uGf+mu0r9fv29fij4n+C/7J3jnxl4N1P8AsbxJpn2H7Je/Z4p/L8y/t4n+SVWQ5SRxypxnI5ANfkD/AMFR/wDk+z4m/wDcM/8ATXaV+qn/AAVH/wCTE/ib/wBwz/06WlAH5Vf8PRf2nBwPiZx/2ANL/wDkaj/h6N+07/0Uz/ygaX/8jV8rUUAfv9/wVH/5MT+Jv/cM/wDTpaV8qf8ABDLn/hdn/cE/9v6+q/8AgqP/AMmJ/E3/ALhn/p0tK+VP+CGX/NbP+4J/7f0Acp+3p+3n8dfgr+1j468G+DPHP9jeG9N+w/ZbL+yLCfy/MsLeV/nlgZzl5HPLHGcDgAV4D/wS4/5Ps+GX/cT/APTXd0f8FR/+T7Pib/3DP/TXaV+qn/D0X9mP/oph/wDBBqn/AMjUAfVJ5WvyA/b0/bz+OvwV/ax8deDfBnjn+xvDem/Yfstl/ZFhP5fmWFvK/wA8sDOcvI55Y4zgcACur/boP/Dyb/hCT+zl/wAXE/4Qv7d/b3/ML+x/a/s/2b/j+8jzN/2S4/1e7bs+bG5c/av7BXwu8T/Bf9k7wN4N8ZaZ/Y3iTTPt32uy+0RT+X5l/cSp88TMhykiHhjjODyCKAPz/wD2C/28/jr8av2sfAvg3xn45/tnw3qX277VZf2RYQeZ5dhcSp88UCuMPGh4YZxg8Eiur/4Lmcf8KTx/1G//AGwrlP2C/wBgz46/BX9rHwL4y8Z+Bv7G8N6b9u+1Xv8Aa9hP5fmWFxEnyRTs5y8iDhTjOTwCa/Sr45ftSfDD9mz+xP8AhY3ib/hHf7a8/wCwf6BdXXneT5fm/wCoifbjzY/vYzu4zg4APiz9gr9gz4FfGn9k7wL4y8Z+BzrPiTUvt32q9/te/g8zy7+4iT5Ip1QYSNBwozjJ5JNfFX/BLj/k+z4Zf9xP/wBNd3X6qf8AD0X9mP8A6KYf/BBqn/yNXVft6/C7xP8AGj9k7xz4N8G6Z/bPiTU/sP2Sy+0RQeZ5d/byv88rKgwkbnlhnGByQKAPn/8A4KtftRfE79mwfC4fDnxN/wAI6Na/tT7f/oFrded5P2Ty/wDXxPtx5sn3cZ3c5wMfQH7BXxR8T/Gj9k7wN4y8Zan/AGz4k1P7d9rvfs8UHmeXf3ESfJEqoMJGg4UZxk8kmvyA/wCHXX7Th5Hwz4/7D+l//JNH/Drn9p3/AKJn/wCV/S//AJJoA8B+FvxS8T/Bbx3pnjLwbqf9jeJNN837Le+RFP5fmRPE/wAkqshykjjlTjORyAa/Sj9hf/jZMPG3/DRv/FxP+EL+w/2D/wAwv7H9r+0faf8Ajy8nzN/2S3/1m7bs+XG5s/Vf/D0X9mP/AKKYf/BBqn/yNXqnwN/ak+GP7Sf9tj4ceJv+Ei/sXyPt/wDoF1a+T53meV/r4k3Z8qT7ucbecZGQD8rP2pP2o/id+xf8dvE3wb+Dfib/AIQ74b+Gvsv9laL9gtb77N9otYrqb99dRSzPumuJX+dzjdgYUAA/Zb/aj+J37aHx28M/Bv4yeJv+Ex+G/iX7V/aui/YLWx+0/Z7WW6h/fWsUUybZreJ/kcZ24OVJB8q/4Kj/APJ9nxN/7hn/AKa7Sv3T+KXxT8MfBbwJqfjLxlqf9j+G9N8r7Ve/Z5Z/L8yVIk+SJWc5eRBwpxnJ4BNAH5q/t0f8a2R4J/4Zy/4t3/wmn27+3v8AmKfbPsn2f7N/x++d5ez7Xcf6vbu3/NnauPzX+KXxS8T/ABp8d6n4y8Zan/bPiTUvK+1XvkRQeZ5cSRJ8kSqgwkaDhRnGTySa+1f+CrX7UPwx/aUPwvHw48Tf8JEdF/tT7fmwurXyfO+yeX/r4k3Z8qT7ucbecZGff/2Cv28/gV8Fv2TvAvg3xn44OjeJNN+3farL+yL+fy/Mv7iVPnigZDlJEPDHGcHkEUAfFP8Aw9G/ad/6KZ/5QNL/APkag/8ABUX9pwjB+JmR/wBgDS//AJGr9f8A9vX4XeJ/jR+yd458G+DdM/tnxJqf2H7JZfaIoPM8u/t5X+eVlQYSNzywzjA5IFfhZ8cv2XPid+zaNEPxG8M/8I6Na8/7B/p9rded5Pl+Z/qJX2482P72M7uM4OADlfil8UvE/wAafHep+MvGWp/2z4k1LyvtV75EUHmeXEkSfJEqoMJGg4UZxk8kmv39/b1+KPif4L/sneOfGXg3U/7G8SaZ9h+yXv2eKfy/Mv7eJ/klVkOUkccqcZyOQDXK/wDBLj/kxP4Zf9xP/wBOl3XgH7ev7efwK+NP7J3jrwb4M8cHWfEmpfYfstl/ZF/B5nl39vK/zywKgwkbnlhnGByQKAOT/YXH/Dyb/hNh+0b/AMXE/wCEL+w/2D/zC/sf2v7R9p/48fI8zf8AZLf/AFm7bs+XG5s/Vf8Aw66/Zj/6Jmf/AAf6p/8AJNfKn/BDP5f+F257f2J/7f19rfFH9vT4FfBfx1qfg3xl44OjeJNN8r7VZf2Rfz+X5kSSp88UDIcpIh4Y4zg4IIoA9/ooooAKKKKACkPSlpD0oA/AL/gqP/yfZ8Tf+4Z/6a7Sv2o/aj+Bv/DSfwK8TfDj+2/+Ed/tr7L/AMTL7J9q8nybqKf/AFW9N2fK2/eGN2ecYP4r/wDBUf8A5Ps+Jv8A3DP/AE12lJ/w9G/ad/6KZ/5QNL/+RqAPqr/hxl/1Wz/y1P8A7to/4cZf9Vs/8tT/AO7a+Vf+Ho37Tv8A0Uz/AMoGl/8AyNR/w9G/ad/6KZ/5QNL/APkagD9Vf+Co3P7CnxM/7hn/AKc7SvlT/ghl/wA1s/7gn/t/XxX8Uf29Pjr8afAup+DfGfjgaz4b1LyvtVl/ZFhB5nlypKnzxQK4w8aHhhnGDkEivtX/AIIZHJ+NhP8A1BP/AG/oA9U/ai/4JS/8NKfHXxN8R/8AhaP/AAjn9tfZf+Jb/wAI/wDavJ8m1ig/1v2pN2fK3fdGN2OcZP4r8+9f1UV8q/8ADrr9mP8A6Jmf/B/qn/yTQB+Vf7DP7c3/AAxf/wAJt/xRP/CY/wDCS/Yf+Yr9h+z/AGf7R/0wl37vtHtjb3zx9Vf8Pzcf80T/APLr/wDuKvqr/h11+zGOR8NDn/sP6p/8k1+QH7evwu8MfBf9rHxz4N8G6Z/Y/hvTPsP2Sy+0Sz+X5lhbyv8APKzOcvI55Y4zgcACgD7U/wCH5v8A1RP/AMuv/wC4qUf8bof+qO/8K2/7jn9o/wBof+A3leX9g/293m/w7fm/Kuv1T/4IZf8ANbP+4J/7f0AfAP7UfwN/4Zr+Ovib4cf23/wkX9i/Zf8AiZfZPsvnedaxT/6re+3Hm7fvHO3PGcD9U/2Xf+CrX/DSnx18M/Dj/hV3/CO/219q/wCJl/wkH2ryfJtZZ/8AVfZU3Z8rb94Y3Z5xg/AP/BUf/k+z4m/9wz/012lH/BLj/k+z4Zf9xP8A9Nd3QB+qf7c37c3/AAxb/wAIT/xRP/CY/wDCS/bf+Yr9h+zfZ/s//TCXfu8/2xt754+Vf+H5v/VE/wDy6/8A7io/4Lm/80T/AO43/wC2FflZQB+qn/DjL/qtn/lqf/dtH/KF/wD6rF/wsn/uB/2d/Z//AIE+b5n2/wD2Nvlfxbvl+1P29fij4n+C/wCyd458ZeDdT/sbxJpn2H7Je/Z4p/L8y/t4n+SVWQ5SRxypxnI5ANfFX7DH/GyYeNv+Gjf+Lif8IX9h/sH/AJhf2P7X9o+0/wDHj5Pmb/slv/rN23Z8uNzZAFP7DH/Dyc/8NHf8Jt/wrr/hNP8AmWv7K/tT7H9k/wBA/wCPnzofM3/ZPM/1a7d+3nG4/VX/AAVH/wCTFPiZ/wBwz/052lfAH7Un7UfxO/Yv+O3ib4N/BvxN/wAId8N/DX2X+ytF+wWt99m+0WsV1N++uopZn3TXEr/O5xuwMKAB9/8A/BUf/kxP4m/9wz/06WlAH4A5JNHNAODX6/8A7BX7BnwK+NP7J3gXxl4z8DnWfEmpfbvtV7/a9/B5nl39xEnyRTqgwkaDhRnGTySaAPtP9qP45/8ADNvwK8TfEb+xP+Ei/sX7L/xLPtf2XzvOuooP9bsfbjzd33TnbjjOR8Af8poP+qO/8K2/7jn9o/2h/wCA3leX9g/293m/w7efiv4o/t6fHX40+BdT8G+M/HA1nw3qXlfarL+yLCDzPLlSVPnigVxh40PDDOMHIJFcr8Df2pfif+zb/bf/AArnxN/wjv8AbXkfb/8AQLW687yfM8v/AF8T7cebJ93Gd3OcDAB9/wD/AA3P/wAO2P8AjHH/AIQn/hYv/CF/8zL/AGr/AGX9s+1/6f8A8e3kzeXs+1+X/rG3bN3Gdo+AP2XPgb/w0n8dfDPw4/tv/hHf7a+1f8TL7J9q8nybWWf/AFW9N2fK2/eGN2ecYP6qfst/sufDH9tD4E+GfjJ8ZPDP/CY/EjxL9q/tXWv7QurH7T9nupbWH9zayxQptht4k+RBnbk5Ykn8gfhb8UvE/wAFvHemeMvBup/2N4k03zfst75EU/l+ZE8T/JKrIcpI45U4zkcgGgD90v2Gf2Gf+GL/APhNv+K2/wCEx/4SX7F/zCvsP2f7P9o/6by793n+2NvfPH5Wf8FRj/xnX8TMf9Qz/wBNlpSf8PRf2nMY/wCFljHTH9gaZ/8AI1eA/FL4peJ/jT471Pxl4y1P+2fEmpeV9qvfIig8zy4kiT5IlVBhI0HCjOMnkk0Af0+0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//9k=";
            var accessToken = await WeChatHelper.GetOfficialAccountAccessTokenAsync();

            var url = $"/cgi-bin/media/upload?access_token={accessToken}&type={MaterialEnum.Image.GetDescription()}";

            // var bytes = Convert.FromBase64String(base64String);
            var (response, err) =
                await ClientPool.WeChatClient.SteamPostAsync<TemporaryMaterialResponseModel>(
                    base64String
                    , Util.GetUuid(), url);
            if (err != null)
                throw err;
            if (response.ErrCode != 0)
                throw new Exception(response.ErrMsg);

            return (response.MediaId, null);
        }
        catch (Exception ex)
        {
            return (default, new ErrorInfo(ErrorEnum.DataError, ex.Message));
        }
    }
}