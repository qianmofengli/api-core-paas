﻿using ApiCorePaas.DataModels.ChatGpt;
using ApiCorePaas.DataModels.ErnieBot;
using ApiCorePaas.Models;
using ApiCorePaas.Models.CacheServices;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.DependencyInjection;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.ErrorAndResult;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;

namespace ApiCorePaas.Handlers;

/// <summary>
/// ErnieBot处理类
/// </summary>
public class ErnieBotHandle : Application, ITransient
{
    private readonly ILogger<ErnieBotHandle> _logger;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    public ErnieBotHandle(ILogger<ErnieBotHandle> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// ERNIE-Bot-turbo是百度自行研发的高效语言模型，基于海量高质数据训练，具有更强的文本理解、内容创作、对话问答等能力
    /// </summary>
    /// <returns></returns>
    public async Task<(string, ErrorInfo)> ErnieBotTurboChat(ErnieBotTurboChatModel model, string userId = "779189225")
    {
        try
        {
            var (accessToken, success) = await CachePool.ErnieBotRedisCache.Get(
                $"{RedisKeyEnum.ErnieBotAccessToken.GetDescription()}_{AppSettings.ErnieBotOption.AppId}");
            if (success == false || string.IsNullOrEmpty(accessToken))
            {
                throw new Exception("获取accessToken失败");
            }

            var url = $"/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/eb-instant?access_token={accessToken}";


            var messageList = new List<ErnieBotTurboMessageModel>
            {
                new()
                {
                    Role = "user",
                    Content = model.Message
                }
            };
            var ernieBotTurboRequestModel = new ErnieBotTurboRequestModel
            {
                Messages = messageList,
                UserId = userId,
                Stream = false
            };
            var (response, error) =
                await ClientPool.ErnieBotClient.PostAsync<ErnieBotTurboResponseModel>(
                    Util.Serialize(ernieBotTurboRequestModel), url);
            if (error != null)
            {
                throw error;
            }

            return (response.Result, null);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "聊天异常");
            return (null, new ErrorInfo(ErrorEnum.DataError, ex.Message));
        }
    }

    /// <summary>
    /// Stable-Diffusion-XL是业内知名的跨模态大模型，由StabilityAI研发并开源，有着业内领先的图像生成能力
    /// </summary>
    /// <param name="model"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<(List<ImageData>, ErrorInfo)> StableDiffusionXlImage(ErnieBotTurboChatModel model,
        string userId = "779189225")
    {
        try
        {
            var (accessToken, success) = await CachePool.ErnieBotRedisCache.Get(
                $"{RedisKeyEnum.ErnieBotAccessToken.GetDescription()}_{AppSettings.ErnieBotOption.AppId}");
            if (success == false || string.IsNullOrEmpty(accessToken))
            {
                throw new Exception("获取accessToken失败");
            }

            var url = $"rpc/2.0/ai_custom/v1/wenxinworkshop/text2image/sd_xl?access_token={accessToken}";

            var stableDiffusionXlRequestModel = new StableDiffusionXlRequestModel
            {
                Prompt = model.Message,
                NegativePrompt = "",
                Size = "768x768",
                Count = 1,
                Steps = 20,
                SamplerIndex = "Euler a",
                Seed = 0,
                CfgScale = 5,
                Style = "Base",
                UserId = userId
            };

            var (response, error) =
                await ClientPool.ErnieBotClient.PostAsync<StableDiffusionXlResponseModel>(
                    Util.Serialize(stableDiffusionXlRequestModel), url);
            if (error != null)
            {
                throw error;
            }

            return (response.Data, null);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "图片生成异常");
            return (null, new ErrorInfo(ErrorEnum.DataError, ex.Message));
        }
    }
}