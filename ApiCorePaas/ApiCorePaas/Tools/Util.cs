﻿using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.Tools;

/// <summary>
/// 
/// </summary>
public static class Util
{
    /// <summary>
    ///     枚举扩展方法 - 从枚举中获取Description
    /// </summary>
    /// <param name="enumName">需要获取枚举描述的枚举</param>
    /// <returns>描述内容</returns>
    public static string GetDescription(this Enum enumName)
    {
        var fieldInfo = enumName.GetType().GetField(enumName.ToString());

        var attributes = (DescriptionAttribute)fieldInfo?.GetCustomAttributes(typeof(DescriptionAttribute), false)
            .FirstOrDefault();
        return attributes?.Description ?? enumName.ToString();
    }

    /// <summary>
    ///     创建Content
    /// </summary>
    /// <param name="value">内容</param>
    public static StringContent CreateContent(string value)
    {
        return new StringContent(value, Encoding.UTF8, "application/json");
    }

    /// <summary>
    /// 把Json字符串序列化成对象
    /// </summary>
    /// <param name="jsonValue"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static (T result, Exception error) Deserialize<T>(string jsonValue)
    {
        try
        {
            var model = JsonConvert.DeserializeObject<T>(jsonValue);
            return (model, null);
        }
        catch (Exception e)
        {
            return (default, e);
        }
    }

    /// <summary>
    ///  序列化成Json字符串
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string Serialize(object obj)
    {
        var jsonStr = JsonConvert.SerializeObject(obj);
        return jsonStr;
    }

    /// <summary>
    /// 指定长度的随机字符串
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    public static string RandomString(int length)
    {
        var random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    // /// <summary>
    // /// 加分布式锁
    // /// </summary>
    // /// <param name="lockKey"></param>
    // /// <param name="expiryInSeconds"></param>
    // /// <returns></returns>
    // public static async Task<bool> AcquireLockAsync(string lockKey, int expiryInSeconds)
    // {
    //     // 尝试递增指定键，如果递增后的值为1，表示成功获取到锁
    //     var incrementedValue = await CachePool.AppletRedisCache.StringIncrementAsync(lockKey);
    //     if (incrementedValue == 1)
    //     {
    //         // 设置锁的过期时间
    //         return await CachePool.AppletRedisCache.KeyExpireAsync(lockKey, TimeSpan.FromSeconds(expiryInSeconds));
    //     }
    //
    //     return false;
    // }
    //
    // /// <summary>
    // /// 删除分布式锁
    // /// </summary>
    // /// <param name="lockKey"></param>
    // public static async Task ReleaseLockAsync(string lockKey)
    // {
    //     await CachePool.AppletRedisCache.Delete(lockKey);
    // }


    /// <summary>
    /// 根据GUID获取19位的唯一数字序列
    /// </summary>
    /// <returns></returns>
    public static long GetGuidToLongId()
    {
        var buffer = Guid.NewGuid().ToByteArray();
        return BitConverter.ToInt64(buffer, 0);
    }


    /// <summary>
    /// 时间戳转本地时间-时间戳精确到秒
    /// </summary>
    /// <param name="timespan"></param>
    /// <returns></returns>
    public static DateTime TimeSpanToDateTime(long timespan)
    {
        var dto = DateTimeOffset.FromUnixTimeSeconds(timespan);
        return dto.ToLocalTime().DateTime;
    }

    /// <summary>
    ///  时间转时间戳Unix-时间戳精确到秒
    /// </summary> 
    public static long ToUnixTimestampBySeconds(DateTime dt)
    {
        var dto = new DateTimeOffset(dt);
        return dto.ToUnixTimeSeconds();
    }

    /// <summary>
    /// 将对象序列化成XML字符串
    /// 实体对象增加[XmlRoot("xml")]属性
    /// 实体属性增加[XmlElement("")]属性
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string XmlSerialize<T>(T obj)
    {
        var xmlWriterSettings = new XmlWriterSettings
        {
            OmitXmlDeclaration = true
        };
        using var memoryStream = new MemoryStream();
        using (var xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings))
        {
            //去除默认命名空间xmlns:xsd和xmlns:xsi
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            //序列化对象
            var xmlSerializer = new XmlSerializer(obj.GetType());
            xmlSerializer.Serialize(xmlWriter, obj, ns);
        }

        return Encoding.UTF8.GetString(memoryStream.ToArray());
    }

    /// <summary>
    /// 将XML字符串序列化成对象
    /// 实体对象增加[XmlRoot("xml")]属性
    /// 实体属性增加[XmlElement("")]属性
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="xml"></param>
    /// <returns></returns>
    public static T XmlDeserialize<T>(string xml)
    {
        var serializer = new XmlSerializer(typeof(T));
        var reader = new StringReader(xml);
        var res = (T)serializer.Deserialize(reader);
        reader.Close();
        reader.Dispose();
        return res;
    }

    /// <summary>
    /// 单个符串转换成XML
    /// 一般用于手动拼接xml对象
    /// </summary> 
    public static XmlNode ToXmlNode(this string str)
    {
        var node = new XmlDocument().CreateNode(XmlNodeType.CDATA, "", "");
        node.InnerText = str;
        return node;
    }

    /// <summary>
    /// Sha1 加密
    /// </summary>
    /// <param name="inputData"></param>
    /// <returns></returns>
    public static string Sha1Encrypt(string inputData)
    {
        using var sha1 = SHA1.Create();
        var inputBytes = Encoding.UTF8.GetBytes(inputData);
        var hashBytes = sha1.ComputeHash(inputBytes);
        return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
    }

    /// <summary>
    /// 获取Guid
    /// </summary>
    /// <returns></returns>
    public static string GetUuid()
    {
        var g = Guid.NewGuid();
        return g.ToString();
    }
}