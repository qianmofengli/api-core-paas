﻿using System.ComponentModel.DataAnnotations;
using ApiCorePaas.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels;

/// <summary>
/// 
/// </summary>
public class TestGetModel
{
    /// <summary>
    /// 
    /// </summary>
    [Required(ErrorMessage = "IntValue不能为空"), Range(1, 100, ErrorMessage = "IntValue必须在1-100之间")]
    [JsonProperty("int_value")]
    [FromQuery(Name = "int_value")]
    public int IntValue { get; set; }

    /// <summary>
    /// 数据库类型
    /// </summary>
    [JsonProperty("database_enum")]
    [Required(ErrorMessage = "DatabaseEnum不能为空"), Range(1, 2, ErrorMessage = "数据库类型必须在1-2之间")]
    [FromQuery(Name = "database_enum")]
    public DatabaseEnum DatabaseEnum { get; set; }
}