﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels;

/// <summary>
/// 
/// </summary>
public class TokenPayloadInfoModel
{
    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("nbf")]
    public string Nbf { get; set; }

    /// <summary>
    /// 有效期
    /// </summary>
    [JsonProperty("exp")]
    public long Exp { get; set; }

    /// <summary>
    /// 用户主键
    /// </summary>
    [JsonProperty("user_id")]
    public string UserId { get; set; }
}