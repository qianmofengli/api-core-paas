﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ChatGpt;

/// <summary>
/// 创建聊天补全
/// </summary>
public class ChatCompletionRequest
{
    /// <summary>
    /// 必需
    /// 要使用的模型的 ID。有关哪些模型可与聊天 API 一起使用的详细信息,请参阅模型端点兼容性表。
    /// </summary>
    [JsonProperty("model")]
    public string Model { get; set; }

    /// <summary>
    /// 必需
    /// 至今为止对话所包含的消息列表
    /// </summary>
    [JsonProperty("messages")]
    public List<Message> Messages { get; set; }

    /// <summary>
    /// 可选
    /// 使用什么采样温度，介于 0 和 2 之间。
    /// 较高的值（如 0.8）将使输出更加随机，而较低的值（如 0.2）将使输出更加集中和确定。
    /// 我们通常建议改变这个或top_p但不是两者。
    /// </summary>
    [JsonProperty("temperature")]
    public int Temperature { get; set; }

    /// <summary>
    /// 可选
    /// 一种替代温度采样的方法，称为核采样，其中模型考虑具有 top_p 概率质量的标记的结果。
    /// 所以 0.1 意味着只考虑构成前 10% 概率质量的标记。 我们通常建议改变这个或temperature但不是两者。
    /// </summary>
    [JsonProperty("top_p")]
    public int TopP { get; set; }

    /// <summary>
    /// 可选
    /// 默认为 1 为每个输入消息生成多少个聊天补全选择
    /// </summary>
    [JsonProperty("n")]
    public int N { get; set; }

    /// <summary>
    /// 可选
    /// 默认为 false 如果设置,则像在 ChatGPT 中一样会发送部分消息增量。
    /// 标记将以仅数据的服务器发送事件的形式发送,这些事件在可用时,并在 data: [DONE] 消息终止流
    /// </summary>
    [JsonProperty("stream")]
    public bool Stream { get; set; }

    /// <summary>
    /// 可选
    /// 默认为 null 最多 4 个序列,API 将停止进一步生成标记
    /// </summary>
    [JsonProperty("stop")]
    public string Stop { get; set; }

    /// <summary>
    /// 可选
    /// 默认为 inf
    /// 在聊天补全中生成的最大标记数。
    /// 输入标记和生成标记的总长度受模型的上下文长度限制
    /// </summary>
    [JsonProperty("max_tokens")]
    public int MaxTokens { get; set; }

    /// <summary>
    /// 可选
    /// -2.0 和 2.0 之间的数字。
    /// 正值会根据到目前为止是否出现在文本中来惩罚新标记，从而增加模型谈论新主题的可能性
    /// </summary>
    [JsonProperty("presence_penalty")]
    public double PresencePenalty { get; set; }

    /// <summary>
    /// 可选
    /// 默认为 0 -2.0 到 2.0 之间的数字。
    /// 正值根据文本目前的存在频率惩罚新标记,降低模型重复相同行的可能性
    /// </summary>
    [JsonProperty("frequency_penalty")]
    public double FrequencyPenalty { get; set; }

    /// <summary>
    /// 可选
    /// 修改指定标记出现在补全中的可能性。
    /// 接受一个 JSON 对象,该对象将标记(由标记器指定的标记 ID)映射到相关的偏差值(-100 到 100)。
    /// 从数学上讲,偏差在对模型进行采样之前添加到模型生成的 logit 中。
    /// 确切效果因模型而异,但-1 和 1 之间的值应减少或增加相关标记的选择可能性;如-100 或 100 这样的值应导致相关标记的禁用或独占选择。
    /// </summary>
    public Dictionary<string, double> LogitBias { get; set; }

    /// <summary>
    /// 可选
    /// 代表您的最终用户的唯一标识符，可以帮助 OpenAI 监控和检测滥用行为
    /// </summary>
    [JsonProperty("user")]
    public string User { get; set; }

    /// <summary>
    /// 可选
    /// 指定模型必须输出的格式的对象。
    /// 将 { "type": "json_object" } 启用 JSON 模式,这可以确保模型生成的消息是有效的 JSON。
    /// 重要提示:使用 JSON 模式时,还必须通过系统或用户消息指示模型生成 JSON。
    /// 如果不这样做,模型可能会生成无休止的空白流,直到生成达到令牌限制,从而导致延迟增加和请求“卡住”的外观。
    /// 另请注意,如果 finish_reason="length",则消息内容可能会被部分切断,这表示生成超过了 max_tokens 或对话超过了最大上下文长度。
    /// 显示属性
    /// </summary>
    [JsonProperty("response_format")]
    public ResponseFormat ResponseFormat { get; set; }

    /// <summary>
    /// 可选
    /// 此功能处于测试阶段。
    /// 如果指定,我们的系统将尽最大努力确定性地进行采样,以便使用相同的种子和参数进行重复请求应返回相同的结果。
    /// 不能保证确定性,您应该参考 system_fingerprint 响应参数来监控后端的更改。
    /// </summary>
    [JsonProperty("seen")]
    public int Seen { get; set; }

    /// <summary>
    /// 可选
    /// 模型可以调用的一组工具列表。目前,只支持作为工具的函数。
    /// 使用此功能来提供模型可以为之生成 JSON 输入的函数列表。
    /// </summary>
    [JsonProperty("tools")]
    public List<Stream> Tools { get; set; }
}

/// <summary>
/// 至今为止对话所包含的消息列表
/// </summary>
public class Message
{
    /// <summary>
    /// 可选
    /// </summary>
    public string Role { get; set; }

    /// <summary>
    /// 可选
    /// </summary>
    public string Content { get; set; }
}

/// <summary>
/// 
/// </summary>
public class ResponseFormat
{
    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("type")]
    public string Type { get; set; }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="type"></param>
    public ResponseFormat(string type)
    {
        Type = type;
    }
}

/// <summary>
/// 
/// </summary>
public class FunctionChoice
{
    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("name")]
    public string Name { get; set; }
}
