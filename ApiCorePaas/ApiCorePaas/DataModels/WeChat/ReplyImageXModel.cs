﻿using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 回复图片消息
/// </summary>
[XmlRoot("xml")]
public class ReplyImageXModel: MsgReplyBaseXModel
{
    /// <summary>
    /// 
    /// </summary>
    [XmlElement("Image")]
    [JsonProperty("Image")]
    public ImageMsgXModel Image { get; set; }
}

/// <summary>
/// 
/// </summary>
public class ImageMsgXModel
{
    /// <summary>
    /// 通过素材管理中的接口上传多媒体文件，得到的id
    /// </summary>
    [XmlElement("MediaId")]
    [JsonProperty("MediaId")]
    public XmlNode MediaId { get; set; }
}