﻿using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 回复文本消息
/// </summary>
[XmlRoot("xml")]
public class ReplyTextXModel : MsgReplyBaseXModel
{
    /// <summary>
    /// 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
    /// </summary>
    [XmlElement("Content")]
    [JsonProperty("Content")]
    public XmlNode Content { get; set; }
}