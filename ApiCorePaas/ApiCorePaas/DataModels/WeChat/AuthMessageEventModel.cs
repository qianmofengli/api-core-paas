﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 微信消息验证
/// </summary>
public class AuthMessageEventModel
{
    /// <summary>
    /// 消息签名
    /// </summary>
    [FromQuery(Name = "msg_signature")]
    [JsonProperty("msg_signature")]
    public string MsgSignature { get; set; }

    /// <summary>
    /// 时间戳
    /// </summary>
    [FromQuery(Name = "timestamp")]
    [JsonProperty("timestamp")]
    public string Timestamp { get; set; }

    /// <summary>
    /// 随机数
    /// </summary>
    [FromQuery(Name = "nonce")]
    [JsonProperty("nonce")]
    public string Nonce { get; set; }

    /// <summary>
    /// 用户openid
    /// </summary>
    [FromQuery(Name = "openid")]
    [JsonProperty("openid")]
    public string Openid { get; set; }

    /// <summary>
    /// 加密类型
    /// </summary>
    [FromQuery(Name = "encrypt_type")]
    [JsonProperty("encrypt_type")]
    public string EncryptType { get; set; }

    /// <summary>
    /// 签名
    /// </summary>
    [FromQuery(Name = "signature")]
    [JsonProperty("signature")]
    public string Signature { get; set; }
}