﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 公众号关注者响应模型
/// </summary>
public class SubscribeUserResponseModel : BaseResponseModel
{
    /// <summary>
    /// 关注该公众账号的总用户数
    /// </summary>
    [JsonProperty("total")]
    public int Total { get; set; }

    /// <summary>
    /// 拉取的OPENID个数，最大值为10000
    /// </summary>
    [JsonProperty("count")]
    public int Count { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("data")]
    public SubscribeOpenIdModel Data { get; set; }
}

/// <summary>
/// 
/// </summary>
public class SubscribeOpenIdModel
{
    /// <summary>
    /// 列表数据，OPENID的列表
    /// </summary>
    [JsonProperty("openid")]
    public List<string> OpenIds { get; set; }

    /// <summary>
    /// 拉取列表的最后一个用户的OPENID
    /// </summary>
    [JsonProperty("next_openid")]
    public string NextOpenid { get; set; }
}