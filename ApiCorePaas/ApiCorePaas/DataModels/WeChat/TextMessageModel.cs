﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 文本消息
/// </summary>
[XmlRoot("xml")]
public class TextMessageModel : MsgReceiveBaseXModel
{
    /// <summary>
    /// 消息类型
    /// </summary>
    [XmlElement("MsgType")]
    [JsonProperty("MsgType")]
    public string MsgType { get; set; }

    /// <summary>
    /// 文本消息内容
    /// </summary>
    [XmlElement("Content")]
    [JsonProperty("Content")]
    public string Content { get; set; }

    /// <summary>
    /// 消息ID
    /// </summary>
    [XmlElement("MsgId")]
    [JsonProperty("MsgId")]
    public long MsgId { get; set; }
    
    /// <summary>
    /// 消息的数据ID（消息如果来自文章时才有）
    /// </summary>
    [XmlElement("MsgDataId")]
    [JsonProperty("MsgDataId")]
    public string MsgDataId { get; set; }
    
    /// <summary>
    /// 多图文时第几篇文章，从1开始（消息如果来自文章时才有）
    /// </summary>
    [XmlElement("Idx")]
    [JsonProperty("Idx")]
    public string Idx { get; set; }
}