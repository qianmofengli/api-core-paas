﻿
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 
/// </summary>
public class UploadTemporaryMaterialModel
{
    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("base64")]
    public string Base64 { get; set; }
}