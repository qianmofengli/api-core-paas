﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 关注/取消关注事件模型
/// </summary>
[XmlRoot("xml")]
public class SubscribeUnSubscribeXModel : MsgReceiveBaseXModel
{
    /// <summary>
    /// 消息类型，event
    /// </summary>
    [XmlElement("MsgType")]
    [JsonProperty("MsgType")]
    public string MsgType { get; set; }

    /// <summary>
    /// 事件类型，subscribe(订阅)
    /// </summary>
    [XmlElement("Event")]
    [JsonProperty("Event")]
    public string Event { get; set; }
}