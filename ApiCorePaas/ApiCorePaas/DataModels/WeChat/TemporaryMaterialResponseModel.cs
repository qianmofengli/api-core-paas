﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 上传临时素材
/// </summary>
public  class TemporaryMaterialResponseModel:BaseResponseModel
{
    /// <summary>
    /// 媒体文件上传后，获取标识
    /// </summary>
    [JsonProperty("media_id")]
    public string MediaId { get; set; }

    /// <summary>
    /// 媒体文件上传时间戳
    /// </summary>
    [JsonProperty("created_at")]
    public long CreatedAt { get; set; }

    /// <summary>
    /// 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
    /// </summary>
    [JsonProperty("type")]
    public string Type { get; set; }
}