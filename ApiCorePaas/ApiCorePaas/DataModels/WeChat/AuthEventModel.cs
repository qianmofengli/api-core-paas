﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 微信公众号消息认证
/// </summary>
public class AuthEventModel
{
    /// <summary>
    /// 消息签名
    /// </summary>
    [FromQuery(Name = "signature")]
    [JsonProperty("signature")]
    [Required(ErrorMessage = "消息签名不能为空")]
    public string Signature { get; set; }

    /// <summary>
    /// 时间戳
    /// </summary>
    [FromQuery(Name = "timestamp")]
    [JsonProperty("timestamp")]
    [Required(ErrorMessage = "时间戳不能为空")]
    public string Timestamp { get; set; }

    /// <summary>
    /// 随机数
    /// </summary>
    [FromQuery(Name = "nonce")]
    [JsonProperty("nonce")]
    [Required(ErrorMessage = "随机数不能为空")]
    public string Nonce { get; set; }

    /// <summary>
    ///随机字符串
    /// </summary>
    [FromQuery(Name = "echostr")]
    [JsonProperty("echostr")]
    [Required(ErrorMessage = "随机字符串不能为空")]
    public string Echostr { get; set; }
}