﻿using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 微信公众号回复消息基类
/// </summary>
public class MsgReplyBaseXModel
{
    /// <summary>
    /// 开发者微信号
    /// </summary>
    [XmlElement("ToUserName")]
    [JsonProperty("ToUserName")]
    public XmlNode ToUserName { get; set; }

    /// <summary>
    /// 发送方帐号（一个OpenID）
    /// </summary>
    [XmlElement("FromUserName")]
    [JsonProperty("FromUserName")]
    public XmlNode FromUserName { get; set; }

    /// <summary>
    /// 消息创建时间 （整型）
    /// </summary>
    [XmlElement("CreateTime")]
    [JsonProperty("CreateTime")]
    public long CreateTime { get; set; }

    /// <summary>
    /// 消息类型
    /// </summary>
    [XmlElement("MsgType")]
    [JsonProperty("MsgType")]
    public XmlNode MsgType { get; set; }
}