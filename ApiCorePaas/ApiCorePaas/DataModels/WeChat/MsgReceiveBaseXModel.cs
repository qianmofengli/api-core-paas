﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 
/// </summary>
public class MsgReceiveBaseXModel
{
    /// <summary>
    /// 开发者微信号
    /// </summary>
    [XmlElement("ToUserName")]
    [JsonProperty("ToUserName")]
    public string ToUserName { get; set; }

    /// <summary>
    /// 发送方帐号（一个OpenID）
    /// </summary>
    [XmlElement("FromUserName")]
    [JsonProperty("FromUserName")]
    public string FromUserName { get; set; }

    /// <summary>
    /// 消息创建时间 （整型）
    /// </summary>
    [XmlElement("CreateTime")]
    [JsonProperty("CreateTime")]
    public long CreateTime { get; set; }
}