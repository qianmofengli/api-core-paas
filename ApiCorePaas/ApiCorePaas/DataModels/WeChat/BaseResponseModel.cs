﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.WeChat;

/// <summary>
/// 微信默认响应模型
/// </summary>
public class BaseResponseModel
{
    /// <summary>
    /// 错误信息
    /// </summary>
    [JsonProperty("errmsg")]
    public string ErrMsg { get; set; }

    /// <summary>
    /// 错误码
    /// </summary>
    [JsonProperty("errcode")]
    public int ErrCode { get; set; }
}