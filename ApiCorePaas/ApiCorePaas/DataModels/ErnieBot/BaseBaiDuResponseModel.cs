﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// 文心一言基础响应模型
/// </summary>
public class BaseBaiDuResponseModel
{
    /// <summary>
    /// 错误描述信息，帮助理解和解决发生的错误
    /// </summary>
    [JsonProperty("error_description")]
    public string ErrorDescription { get; set; }

    /// <summary>
    /// 错误码
    /// </summary>
    [JsonProperty("error")]
    public string Error { get; set; }
}