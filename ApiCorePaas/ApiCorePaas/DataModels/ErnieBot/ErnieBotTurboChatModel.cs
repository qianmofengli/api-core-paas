﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// 请求模型
/// </summary>
public class ErnieBotTurboChatModel
{
    /// <summary>
    /// 问题
    /// </summary>
    [JsonProperty("message")]
    [Required(ErrorMessage = "message不能为空")]
    public string Message { get; set; }
}