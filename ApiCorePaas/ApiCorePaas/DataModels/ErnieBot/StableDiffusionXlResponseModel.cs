﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// 
/// </summary>
public class StableDiffusionXlResponseModel : BaseErnieBotResponseModel
{
    /// <summary>
    /// 请求的id
    /// </summary>
    [JsonProperty("id")]
    public string Id { get; set; }

    /// <summary>
    /// 回包类型。image：图像生成返回
    /// </summary>
    [JsonProperty("object")]
    public string Object { get; set; }

    /// <summary>
    /// 时间戳
    /// </summary>
    [JsonProperty("created")]
    public int Created { get; set; }

    /// <summary>
    /// 生成图片结果
    /// </summary>
    [JsonProperty("data")]
    public List<ImageData> Data { get; set; }

    /// <summary>
    /// token统计信息，token数 = 汉字数+单词数*1.3 （仅为估算逻辑）
    /// </summary>
    [JsonProperty("usage")]
    public StableDiffusionXlUsageModel Usage { get; set; }
}

/// <summary>
/// 
/// </summary>
public class ImageData
{
    /// <summary>
    /// 固定值"image"
    /// </summary>
    /// <returns></returns>
    [JsonProperty("object")]
    public string Object { get; set; }

    /// <summary>
    /// 图片base64编码内容
    /// </summary>
    [JsonProperty("b64_image")]
    public string B64Image { get; set; }

    /// <summary>
    /// 序号
    /// </summary>
    [JsonProperty("index")]
    public int Index { get; set; }
}

/// <summary>
/// 
/// </summary>
public class StableDiffusionXlUsageModel
{
    /// <summary>
    /// 问题tokens数，包含提示词和负向提示词
    /// </summary>
    [JsonProperty("prompt_tokens")]
    public int PromptTokens { get; set; }

    /// <summary>
    /// tokens总数
    /// </summary>
    [JsonProperty("total_tokens")]
    public int TotalTokens { get; set; }
}