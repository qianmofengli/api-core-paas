﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// 文心一言获取 AccessToken 响应模型
/// </summary>
public class BaiDuAccessTokenResponseModel : BaseBaiDuResponseModel
{
    /// <summary>
    /// AccessToekn
    /// </summary>
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }

    /// <summary>
    /// 有效期，Access Token的有效期。 说明：单位是秒，有效期30天
    /// </summary>
    [JsonProperty("expires_in")]
    public int ExpiresIn { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("session_key")]
    public string SessionKey { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("refresh_token")]
    public string RefreshToken { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("scope")]
    public string Scope { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("session_secret")]
    public string SessionSecret { get; set; }
}