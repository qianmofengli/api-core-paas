﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// ErnieBot Turbo响应模型
/// </summary>
public class ErnieBotTurboResponseModel : BaseErnieBotResponseModel
{
    /// <summary>
    /// 本轮对话的id
    /// </summary>
    [JsonProperty("id")]
    public string Id { get; set; }

    /// <summary>
    /// 本轮对话的上下文信息
    /// </summary>
    [JsonProperty("object")]
    public string Object { get; set; }

    /// <summary>
    /// 时间戳
    /// </summary>
    [JsonProperty("created")]
    public int Created { get; set; }

    /// <summary>
    /// 表示当前子句的序号。只有在流式接口模式下会返回该字段
    /// </summary>
    [JsonProperty("sentence_id")]
    public int SentenceId { get; set; }

    /// <summary>
    /// 表示当前子句是否是最后一句。只有在流式接口模式下会返回该字段
    /// </summary>
    [JsonProperty("is_end")]
    public bool IsEnd { get; set; }

    /// <summary>
    /// 当前生成的结果是否被截断
    /// </summary>
    [JsonProperty("is_truncated")]
    public bool IsTruncated { get; set; }

    /// <summary>
    /// 对话返回结果
    /// </summary>
    [JsonProperty("result")]
    public string Result { get; set; }

    /// <summary>
    /// 表示用户输入是否存在安全，是否关闭当前会话，清理历史会话信息。
    /// true：是，表示用户输入存在安全风险，建议关闭当前会话，清理历史会话信息。
    /// false：否，表示用户输入无安全风险
    /// </summary>
    [JsonProperty("need_clear_history")]
    public bool NeedClearHistory { get; set; }

    /// <summary>
    /// 当need_clear_history为true时，此字段会告知第几轮对话有敏感信息，如果是当前问题，ban_round=-1
    /// </summary>
    [JsonProperty("ban_round")]
    public int BanRound { get; set; }

    /// <summary>
    /// token统计信息
    /// </summary>
    [JsonProperty("usage")]
    public ErnieBotTurboUsageModel ErnieBotTurboUsage { get; set; }
}

/// <summary>
/// token统计信息
/// </summary>
public class ErnieBotTurboUsageModel
{
    /// <summary>
    /// 问题tokens数
    /// </summary>
    [JsonProperty("prompt_tokens")]
    public int PromptTokens { get; set; }

    /// <summary>
    /// 回答tokens数
    /// </summary>
    [JsonProperty("completion_tokens")]
    public int CompletionTokens { get; set; }

    /// <summary>
    /// 总tokens数
    /// </summary>
    [JsonProperty("total_tokens")]
    public int TotalTokens { get; set; }
}