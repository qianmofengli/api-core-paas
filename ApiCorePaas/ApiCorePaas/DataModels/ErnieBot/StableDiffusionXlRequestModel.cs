﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// 图片请求模型
/// </summary>
public class StableDiffusionXlRequestModel
{
    /// <summary>
    /// 提示词，即用户希望图片包含的元素。长度限制为1024字符，建议中文或者英文单词总数量不超过150个
    /// </summary>
    [JsonProperty("prompt")]
    public string Prompt { get; set; }

    /// <summary>
    /// 反向提示词，即用户希望图片不包含的元素。长度限制为1024字符，建议中文或者英文单词总数量不超过150个
    /// </summary>
    [JsonProperty("negative_prompt")]
    public string NegativePrompt { get; set; }

    /// <summary>
    /// 生成图片长宽，默认值 1024x1024，取值范围如下：  ["768x768", "768x1024", "1024x768", "576x1024", "1024x576", "1024x1024"]
    /// </summary>
    [JsonProperty("size")]
    public string Size { get; set; }

    /// <summary>
    /// 生成图片数量，说明：  默认值为1 取值范围为1-4 单次生成的图片较多及请求较频繁可能导致请求超时
    /// </summary>
    [JsonProperty("n")]
    public int Count { get; set; }

    /// <summary>
    /// 迭代轮次，说明： 默认值为20  取值范围为10-50
    /// </summary>
    [JsonProperty("steps")]
    public int Steps { get; set; }

    /// <summary>
    /// 采样方式，默认值：Euler a
    /// </summary>
    [JsonProperty("sampler_index")]
    public string SamplerIndex { get; set; }

    /// <summary>
    /// 随机种子，说明： 不设置时，自动生成随机数 取值范围 [0, 4294967295]
    /// </summary>
    [JsonProperty("seed")]
    public int Seed { get; set; }

    /// <summary>
    /// 提示词相关性，说明：默认值为5，取值范围0-30
    /// </summary>
    [JsonProperty("cfg_scale")]
    public float CfgScale { get; set; }

    /// <summary>
    /// 生成风格。说明： 可选值： Base：基础风格
    /// </summary>
    [JsonProperty("style")]
    public string Style { get; set; } = "Base";

    /// <summary>
    /// 表示最终用户的唯一标识符，可以监视和检测滥用行为，防止接口恶意调用
    /// </summary>
    [JsonProperty("user_id")]
    public string UserId { get; set; }
}