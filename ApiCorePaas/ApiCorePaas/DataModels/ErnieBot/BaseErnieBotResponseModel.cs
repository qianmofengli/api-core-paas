﻿
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// 文心一言基础响应模型
/// </summary>
public class BaseErnieBotResponseModel
{
    /// <summary>
    /// 错误描述信息，帮助理解和解决发生的错误
    /// </summary>
    [JsonProperty("error_code")]
    public int ErrorCode { get; set; }

    /// <summary>
    /// 错误描述信息，帮助理解和解决发生的错误
    /// </summary>
    [JsonProperty("error_msg")]
    public string ErrorMsg { get; set; }
}