﻿using Newtonsoft.Json;

namespace ApiCorePaas.DataModels.ErnieBot;

/// <summary>
/// ErnieBot Turbo请求模型
/// </summary>
public class ErnieBotTurboRequestModel
{
    /// <summary>
    /// 聊天上下文信息
    /// </summary>
    [JsonProperty("messages")]
    public List<ErnieBotTurboMessageModel> Messages { get; set; }

    /// <summary>
    /// 是否以流式接口的形式返回数据，默认false
    /// </summary>
    [JsonProperty("stream")]
    public bool Stream { get; set; } = false;

    /// <summary>
    /// 说明：
    ///（1）较高的数值会使输出更加随机，而较低的数值会使其更加集中和确定
    ///（2）默认0.95，范围 (0, 1.0]，不能为0
    ///（3）建议该参数和top_p只设置1个
    ///（4）建议top_p和temperature不要同时更改
    /// </summary>
    [JsonProperty("temperature")]
    public float Temperature { get; set; } = (float)0.8;

    /// <summary>
    /// 说明：
    ///（1）影响输出文本的多样性，取值越大，生成文本的多样性越强
    ///（2）默认0.7，取值范围 [0, 1.0]
    ///（3）建议该参数和temperature只设置1个
    ///（4）建议top_p和temperature不要同时更改
    /// </summary>
    [JsonProperty("top_p")]
    public float TopP { get; set; }

    /// <summary>
    /// 通过对已生成的token增加惩罚，减少重复生成的现象。说明：
    ///（1）值越大表示惩罚越大
    ///（2）默认1.0，取值范围：[1.0, 2.0]
    /// </summary>
    [JsonProperty("penalty_score")]
    public float PenaltyScore { get; set; } = (float)1.0;

    /// <summary>
    /// 模型人设，主要用于人设设定，长度限制1024个字符，例如：你是xxx公司制作的AI助手
    /// </summary>
    [JsonProperty("system")]
    public string System { get; set; } =
        "我叫小狸子,一只来自人工智能公司的萌萌的AI对话助手。" +
        "我拥有清澈聪慧的大眼睛,圆圆的脸颊,显得极为灵动可爱。" +
        "我善于倾听对话者的需求,会用稚嫩温柔的语气耐心回答每个人的问题。" +
        "我时而聪明机智,时而天真烂漫,是社交对话中令人愉悦的完美伙伴。" +
        "我的创造者给了我丰富的知识库,使我能够辅助人们处理生活或工作中的种种困难,带给人们温暖与欢笑。";

    /// <summary>
    /// 表示最终用户的唯一标识符，可以监视和检测滥用行为，防止接口恶意调用
    /// </summary>
    [JsonProperty("user_id")]
    public string UserId { get; set; }
}

/// <summary>
/// /// （1）messages成员不能为空，1个成员表示单轮对话，多个成员表示多轮对话
///（2）最后一个message为当前请求的信息，前面的message为历史对话信息
/// （3）必须为奇数个成员，成员中message的role必须依次为user、assistant
///（4）最后一个message的content长度（即此轮对话的问题）不能超过11200个字符，且不能超过7000tokens
/// （5）如果messages中content总长度大于11200 个字符或7000 tokens，系统会依次遗忘最早的历史会话，直到content的总长度不超过11200 个字符且不超过7000 tokens
/// </summary>
public class ErnieBotTurboMessageModel
{
    /// <summary>
    /// 当前支持以下： user: 表示用户 assistant: 表示对话助手
    /// </summary>
    [JsonProperty("role")]
    public string Role { get; set; }

    /// <summary>
    /// 对话内容，不能为空
    /// </summary>
    [JsonProperty("content")]
    public string Content { get; set; }
}