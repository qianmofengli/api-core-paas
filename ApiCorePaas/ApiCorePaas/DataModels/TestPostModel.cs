﻿using System.ComponentModel.DataAnnotations;
using ApiCorePaas.Models.Enums;
using Newtonsoft.Json;

namespace ApiCorePaas.DataModels;

/// <summary>
/// 
/// </summary>
public class TestPostModel
{
    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("int_value")]
    [Required(ErrorMessage = "IntValue不能为空"), Range(1, 100, ErrorMessage = "IntValue必须在1-100之间")]
    public int IntValue { get; set; }

    /// <summary>
    /// 数据库类型
    /// </summary>
    [JsonProperty("database_enum")]
    [Required(ErrorMessage = "DatabaseEnum不能为空"), Range(1, 2, ErrorMessage = "数据库类型必须在1-2之间")]
    public DatabaseEnum DatabaseEnum { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [JsonProperty("string_value")]
    public string StringValue { get; set; }
}