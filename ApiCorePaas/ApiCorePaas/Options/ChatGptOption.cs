﻿namespace ApiCorePaas.Options;

/// <summary>
/// ChatGpt 密钥
/// </summary>
public class ChatGptOption
{
    /// <summary>
    /// 密钥
    /// </summary>
    public string SecretKey { get; set; }

    /// <summary>
    /// 组织ID
    /// </summary>
    public string OrganizationId { get; set; }

    /// <summary>
    /// 请求地址
    /// </summary>
    public string Url { get; set; }
}