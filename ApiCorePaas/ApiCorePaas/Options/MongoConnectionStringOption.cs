﻿namespace ApiCorePaas.Options
{
    /// <summary>
    /// mongoDb连接字符串
    /// </summary>
    public class MongoConnectionStringOption
    {
        /// <summary>
        /// IotLog
        /// </summary>
        public string IotLog { get; set; }

        /// <summary>
        /// OperationLog
        /// </summary>
        public string OperationLog { get; set; }
    }
}