﻿namespace ApiCorePaas.Options;

/// <summary>
/// redis 配置
/// </summary>
public class RedisOption
{
    /// <summary>
    /// redis 连接串
    /// </summary>
    public string RedisConnection { get; set; }

    /// <summary>
    /// 公众号缓存库
    /// </summary>
    public int OfficialAccountRedisCache { get; set; }

    /// <summary>
    /// 文心一言缓存库
    /// </summary>
    public int ErnieBotRedisCache { get; set; }
}