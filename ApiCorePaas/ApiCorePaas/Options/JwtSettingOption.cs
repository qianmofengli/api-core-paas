﻿namespace ApiCorePaas.Options;

/// <summary>
/// Jwt配置
/// </summary>
public class JwtSettingOption
{
    /// <summary>
    /// 密钥
    /// </summary>
    public string Key { get; set; }

    /// <summary>
    /// 发行者
    /// </summary>
    public string Issuer { get; set; }

    /// <summary>
    /// 受众
    /// </summary>
    public string Audience { get; set; }

    /// <summary>
    /// 令牌过期时间
    /// </summary>
    public int DurationInMinutes { get; set; }
}