﻿namespace ApiCorePaas.Options
{
    /// <summary>
    /// 配置
    /// </summary>
    public static class AppSettings
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public static MySqlConnectionStringOption MySqlConnectionStringOption { get; set; }

        /// <summary>
        /// Mongo
        /// </summary>
        public static MongoConnectionStringOption MongoConnectionStringOption { get; set; }

        /// <summary>
        /// redis
        /// </summary>
        public static RedisOption RedisOption { get; set; }

        /// <summary>
        /// jwt配置
        /// </summary>
        public static JwtSettingOption JwtSettingOption { get; set; }

        /// <summary>
        /// ChatGpt 配置
        /// </summary>
        public static ChatGptOption ChatGptOption { get; set; }

        /// <summary>
        /// 微信配置
        /// </summary>
        public static WeChatOption WeChatOption { get; set; }
        
        
        /// <summary>
        /// 文心一言配置
        /// </summary>
        public static ErnieBotOption ErnieBotOption { get; set; }
    }
}