﻿namespace ApiCorePaas.Options;

/// <summary>
/// mysql连接字符串
/// </summary>
public class MySqlConnectionStringOption
{
    /// <summary>
    /// core 主库连接字符串
    /// </summary>
    public string Core { get; set; }

    /// <summary>
    /// 物联网连接字符串
    /// </summary>
    public string Iot { get; set; }

    /// <summary>
    /// 自己的
    /// </summary>
    public string FarmhouseShop { get; set; }
}