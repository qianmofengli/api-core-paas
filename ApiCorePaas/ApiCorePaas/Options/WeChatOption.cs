﻿namespace ApiCorePaas.Options;

/// <summary>
/// 微信配置
/// </summary>
public class WeChatOption
{
    /// <summary>
    /// 主机地址
    /// </summary>
    public string HostUrl { get; set; }

    /// <summary>
    /// 微信公众号appid
    /// </summary>
    public string OfficialAccountAppId { get; set; }

    /// <summary>
    /// 微信公众号密钥
    /// </summary>
    public string OfficialAccountAppSecret { get; set; }

    /// <summary>
    /// 微信公众号消息解密密钥
    /// </summary>
    public string EncodingAesKey { get; set; }

    /// <summary>
    /// 微信公众号token
    /// </summary>
    public string OfficialAccountToken { get; set; }
}