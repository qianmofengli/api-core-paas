﻿namespace ApiCorePaas.Options;

/// <summary>
/// 文心一言配置
/// </summary>
public class ErnieBotOption
{
    /// <summary>
    /// 应用ID
    /// </summary>
    public string AppId { get; set; }

    /// <summary>
    /// ApiKey
    /// </summary>
    public string ApiKey { get; set; }

    /// <summary>
    /// SecretKey
    /// </summary>
    public string SecretKey { get; set; }

    /// <summary>
    /// 请求地址
    /// </summary>
    public string Url { get; set; }
}