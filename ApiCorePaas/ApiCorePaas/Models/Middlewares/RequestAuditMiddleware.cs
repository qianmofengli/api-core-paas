﻿using System.Text;
using Serilog;

namespace ApiCorePaas.Models.Middlewares;

/// <summary>
/// 请求审计
/// </summary>
public class RequestAuditMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="next"></param>
    public RequestAuditMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            // 在这里可以记录请求信息
            await LogRequest(context.Request);

            // 替换原始的响应流
            var originalBodyStream = context.Response.Body;
            using var responseBody = new MemoryStream();
            context.Response.Body = responseBody;

            await _next(context); // 调用其他中间件处理请求

            // 读取响应流
            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(context.Response.Body).ReadToEndAsync();
            context.Response.Body.Seek(0, SeekOrigin.Begin);

            // 记录响应日志
            LogResponse(context, text);

            // 将响应内容复制回原始的响应流
            await responseBody.CopyToAsync(originalBodyStream);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "RequestAuditMiddleware InvokeAsync Error：{ExMessage}", ex.Message);
            throw;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    private static async Task LogRequest(HttpRequest request)
    {
        try
        {
            request.EnableBuffering();

            var requestStream = request.Body;
            string requestBody = null;

            // 读取请求体
            requestStream.Position = 0;
            using (var reader = new StreamReader(requestStream, encoding: Encoding.UTF8,
                       detectEncodingFromByteOrderMarks: false, leaveOpen: true))
            {
                requestBody = await reader.ReadToEndAsync();
                requestStream.Position = 0; // 重置流的位置
            }

            var requestInfo = new
            {
                // TraceId = request.HttpContext.TraceIdentifier,
                TraceId = request.Headers["X-Trace-Id"].ToString(),
                request.Scheme,
                request.Method,
                Host = request.Host.ToString(),
                request.Path,
                QueryString = request.QueryString.ToString(),
                RequestBody = requestBody, // 假设您已经读取了请求体
                StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
            };
            Log.Information("HTTP Request: {@RequestInfo}", requestInfo);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "RequestAuditMiddleware LogRequest Error：{ExMessage}", ex.Message);
            throw;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="responseBody"></param>
    private static void LogResponse(HttpContext context, string responseBody)
    {
        try
        {
            var responseInfo = new
            {
                // TraceId = context.TraceIdentifier,
                TraceId = context.Request.Headers["X-Trace-Id"].ToString(),
                context.Request.Scheme,
                context.Request.Method,
                Host = context.Request.Host.ToString(),
                context.Request.Path,
                QueryString = context.Request.QueryString.ToString(),
                ResponseBody = responseBody,
                EndTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
            };

            Log.Information("HTTP Response: {@ResponseInfo}", responseInfo);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "RequestAuditMiddleware LogResponse Error：{ExMessage}", ex.Message);
            throw;
        }
    }
}