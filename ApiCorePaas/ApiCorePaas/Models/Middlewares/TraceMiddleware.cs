﻿using ApiCorePaas.Tools;

namespace ApiCorePaas.Models.Middlewares;

/// <summary>
/// TraceId中间件
/// </summary>
public class TraceMiddleware
{
    private const string TraceIdHeader = "X-Trace-Id";
    private readonly RequestDelegate _next;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="next"></param>
    public TraceMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public async Task InvokeAsync(HttpContext context)
    {
        // 检查请求头是否已有TraceId，如果没有则生成一个新的
        if (!context.Request.Headers.TryGetValue(TraceIdHeader, out var traceId))
        {
            traceId = Util.GetUuid();
            context.Request.Headers.Append(TraceIdHeader, traceId);
        }

        // 可以选择将TraceId添加到响应头
        context.Response.OnStarting(() =>
        {
            context.Response.Headers.Append(TraceIdHeader, traceId);
            return Task.CompletedTask;
        });

        // 在上下文中存储TraceId以供后续使用
        context.Items["TraceId"] = traceId;

        
        context.Request.EnableBuffering();

        // 读取请求体
        var requestBodyStream = new MemoryStream();
        await context.Request.Body.CopyToAsync(requestBodyStream);
        requestBodyStream.Seek(0, SeekOrigin.Begin);

        // 将流转换为字符串
        var requestBodyText = await new StreamReader(requestBodyStream).ReadToEndAsync();
        context.Request.Body.Seek(0, SeekOrigin.Begin);  // 重置流的位置
        context.Request.Headers["ReqBody"] = requestBodyText;        
        await _next(context);
        
        // 删除上下文中记录的返回值和请求值
        context.Request.Headers.Remove("ResBody");
        context.Request.Headers.Remove("ReqBody");
    }
}