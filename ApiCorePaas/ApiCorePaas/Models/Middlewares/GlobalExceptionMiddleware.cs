﻿using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.Exceptions;
using Serilog;

namespace ApiCorePaas.Models.Middlewares;

/// <summary>
/// 全局异常处理
/// </summary>
public class GlobalExceptionMiddleware
{
    private readonly RequestDelegate _next;
    private string _traceId;
    private string _requestBodyString;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="next"></param>
    public GlobalExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public async Task Invoke(HttpContext context)
    {
        try
        {
            _traceId = context.Request.HttpContext.Request.Headers["TraceId"];
            _requestBodyString = context.Request.Headers["ReqBody"];
            // 保留原有的逻辑
            await _next(context);
        }
        catch (Exception ex)
        {
            await HandleExceptionAsync(context, ex);
        }
    }

    /// <summary>
    /// 错误处理
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ex"></param>
    private async Task HandleExceptionAsync(HttpContext context, Exception ex)
    {
        if (context.Response.HasStarted)
        {
            return;
        }

        try
        {
            var response = context.Response;
            response.ContentType = "application/json";

            switch (ex)
            {
                // 处理自定义异常
                case CustomException customEx:
                    response.StatusCode = customEx.Code switch
                    {
                        ErrorEnum.Unauthorized or
                            ErrorEnum.AccessTokenNull or
                            ErrorEnum.RedisUserInfoNull => 401,
                        ErrorEnum.Forbidden => 403,
                        _ => 500
                    };

                    await WriteResponseAsync(response, (int)customEx.Code, customEx.Message);
                    break;
                case UnauthorizedAccessException uaEx:
                    response.StatusCode = 401;
                    await WriteResponseAsync(response, response.StatusCode, uaEx.Message);
                    break;
                default:
                    response.StatusCode = 500;
                    await WriteResponseAsync(response, response.StatusCode, ex.Message);

                    // 异步记录异常日志
                    await LogErrorAsync(context, ex);
                    break;
            }
        }
        catch (Exception e)
        {
            // 记录中间件内部的异常
            await LogErrorAsync(context, e);
            throw;
        }
    }

    /// <summary>
    /// 写入响应
    /// </summary>
    /// <param name="response"></param>
    /// <param name="code"></param>
    /// <param name="message"></param>
    private static async Task WriteResponseAsync(HttpResponse response, int code, string message)
    {
        var result = $"{{ \"ok\":false, \"err\":{{ \"code\":{code}, \"msg\":\"{message}\" }} }}";
        await response.WriteAsync(result);
    }

    /// <summary>
    /// 记录日志
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ex"></param>
    private Task LogErrorAsync(HttpContext context, Exception ex)
    {
        var errorInfo = new
        {
            TraceId = _traceId,
            Schema = context.Request.Scheme,
            context.Request.Host,
            context.Request.Path,
            QueryString = context.Request.QueryString.ToString(),
            RequestBody = _requestBodyString,
            ExecuteStartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
            Exception = ex,
            ExecuteEndTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
        };
        Log.Error("Request Error: {@ErrorInfo}", errorInfo);
        return Task.CompletedTask;
    }
}