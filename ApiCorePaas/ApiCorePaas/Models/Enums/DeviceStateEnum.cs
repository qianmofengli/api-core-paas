﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums
{    /// <summary>
     /// 设备状态
     /// </summary>
    public enum DeviceStateEnum
    {
        /// <summary>
        /// 在线
        /// </summary>
        [Description("在线")]
        OnLine = 1,
        /// <summary>
        /// 离线
        /// </summary>
        [Description("离线")]
        OffLine = 2,

        /// <summary>
        /// 未激活
        /// </summary>
        [Description("未激活")]
        NotActive = 3

           
    }
}
