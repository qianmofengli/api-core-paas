﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums;

/// <summary>
/// redis key 枚举
/// </summary>
public enum RedisKeyEnum
{
    /// <summary>
    /// 公众号调用凭据
    /// </summary>
    [Description("official_account_access_token")]
    OfficialAccountAccessToken,

    /// <summary>
    /// 文心一言调用凭据
    /// </summary>
    [Description("ernie_bot_access_token")]
    ErnieBotAccessToken
}