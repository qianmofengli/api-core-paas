﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums
{

    /// <summary>
    /// mqtt账号类型
    /// </summary>
    public enum MqttAccountTypeEnum
    {
        /// <summary>
        /// DMC服务
        /// </summary>
        [Description("GID_server")] Dmc = 1,

        /// <summary>
        /// 设备
        /// </summary>
        [Description("GID_sensor")] Device
    }
}
