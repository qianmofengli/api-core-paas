﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums
{
    /// <summary>
    /// 数据库枚举
    /// </summary>
    public enum DatabaseEnum
    {
        /// <summary>
        /// 主库Core
        /// </summary>
        [Description("farmhouseshop")]
        FarmhouseShop = 1,

    }
}
