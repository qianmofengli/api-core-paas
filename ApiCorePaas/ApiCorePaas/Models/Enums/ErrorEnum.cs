﻿namespace ApiCorePaas.Models.Enums
{
    /// <summary>
    /// 错误类型枚举
    /// </summary>
    public enum ErrorEnum
    {
        /// <summary>
        /// 一般错误码
        /// </summary>
        Default = 0,

        //
        // 摘要:
        //     Equivalent to HTTP status 400. System.Net.HttpStatusCode.BadRequest indicates
        //     that the request could not be understood by the server. System.Net.HttpStatusCode.BadRequest
        //     is sent when no other error is applicable, or if the exact error is unknown or
        //     does not have its own error code.
        /// <summary>
        ///    请求错误
        /// </summary>
        BadRequest = 400,

        //
        // 摘要:
        //     Equivalent to HTTP status 401. System.Net.HttpStatusCode.Unauthorized indicates
        //     that the requested resource requires authentication. The WWW-Authenticate header
        //     contains the details of how to perform the authentication.
        /// <summary>
        ///   未授权
        /// </summary>
        Unauthorized = 401,

        //
        // 摘要:
        //     Equivalent to HTTP status 402. System.Net.HttpStatusCode.PaymentRequired is reserved
        //     for future use.
        /// <summary>
        ///  付款要求
        /// </summary>
        PaymentRequired = 402,

        //
        // 摘要:
        //     Equivalent to HTTP status 403. System.Net.HttpStatusCode.Forbidden indicates
        //     that the server refuses to fulfill the request.
        /// <summary>
        /// 禁止访问
        /// </summary>
        Forbidden = 403,

        //
        // 摘要:
        //     Equivalent to HTTP status 404. System.Net.HttpStatusCode.NotFound indicates that
        //     the requested resource does not exist on the server.
        /// <summary>
        /// 未找到
        /// </summary>
        NotFound = 404,

        //
        // 摘要:
        //     Equivalent to HTTP status 405. System.Net.HttpStatusCode.MethodNotAllowed indicates
        //     that the request method (POST or GET) is not allowed on the requested resource.
        /// <summary>
        /// 方法不允许
        /// </summary>
        MethodNotAllowed = 405,

        //
        // 摘要:
        //     Equivalent to HTTP status 406. System.Net.HttpStatusCode.NotAcceptable indicates
        //     that the client has indicated with Accept headers that it will not accept any
        //     of the available representations of the resource.
        /// <summary>
        /// 不可接受
        /// </summary>
        NotAcceptable = 406,

        //
        // 摘要:
        //     Equivalent to HTTP status 407. System.Net.HttpStatusCode.ProxyAuthenticationRequired
        //     indicates that the requested proxy requires authentication. The Proxy-authenticate
        //     header contains the details of how to perform the authentication.
        /// <summary>
        /// 代理认证要求
        /// </summary>
        ProxyAuthenticationRequired = 407,

        //
        // 摘要:
        //     Equivalent to HTTP status 408. System.Net.HttpStatusCode.RequestTimeout indicates
        //     that the client did not send a request within the time the server was expecting
        //     the request.
        /// <summary>
        /// 请求超时
        /// </summary>
        RequestTimeout = 408,

        //
        // 摘要:
        //     Equivalent to HTTP status 409. System.Net.HttpStatusCode.Conflict indicates that
        //     the request could not be carried out because of a conflict on the server.
        /// <summary>
        /// 冲突
        /// </summary>
        Conflict = 409,

        //
        // 摘要:
        //     Equivalent to HTTP status 410. System.Net.HttpStatusCode.Gone indicates that
        //     the requested resource is no longer available.
        /// <summary>
        /// 已删除
        /// </summary>
        Gone = 410,

        //
        // 摘要:
        //     Equivalent to HTTP status 411. System.Net.HttpStatusCode.LengthRequired indicates
        //     that the required Content-length header is missing.
        /// <summary>
        /// 需要长度
        /// </summary>
        LengthRequired = 411,

        //
        // 摘要:
        //     Equivalent to HTTP status 412. System.Net.HttpStatusCode.PreconditionFailed indicates
        //     that a condition set for this request failed, and the request cannot be carried
        //     out. Conditions are set with conditional request headers like If-Match, If-None-Match,
        //     or If-Unmodified-Since.
        /// <summary>
        /// 前提条件失败
        /// </summary>
        PreconditionFailed = 412,

        //
        // 摘要:
        //     Equivalent to HTTP status 413. System.Net.HttpStatusCode.RequestEntityTooLarge
        //     indicates that the request is too large for the server to process.
        /// <summary>
        /// 请求实体太大
        /// </summary>
        RequestEntityTooLarge = 413,

        //
        // 摘要:
        //     Equivalent to HTTP status 414. System.Net.HttpStatusCode.RequestUriTooLong indicates
        //     that the URI is too long.
        /// <summary>
        /// 请求URI太长
        /// </summary>
        RequestUriTooLong = 414,

        //
        // 摘要:
        //     Equivalent to HTTP status 415. System.Net.HttpStatusCode.UnsupportedMediaType
        //     indicates that the request is an unsupported type.
        /// <summary>
        /// 不支持的媒体类型
        /// </summary>
        UnsupportedMediaType = 415,

        //
        // 摘要:
        //     Equivalent to HTTP status 416. System.Net.HttpStatusCode.RequestedRangeNotSatisfiable
        //     indicates that the range of data requested from the resource cannot be returned,
        //     either because the beginning of the range is before the beginning of the resource,
        //     or the end of the range is after the end of the resource.
        /// <summary>
        /// 请求范围不符合要求
        /// </summary>
        RequestedRangeNotSatisfiable = 416,

        //
        // 摘要:
        //     Equivalent to HTTP status 417. System.Net.HttpStatusCode.ExpectationFailed indicates
        //     that an expectation given in an Expect header could not be met by the server.
        /// <summary>
        /// 期望失败
        /// </summary>
        ExpectationFailed = 417,

        //
        // 摘要:
        //     Equivalent to HTTP status 421. System.Net.HttpStatusCode.MisdirectedRequest indicates
        //     that the request was directed at a server that is not able to produce a response.
        /// <summary>
        /// 误导的请求
        /// </summary>
        MisdirectedRequest = 421,

        //
        // 摘要:
        //     Equivalent to HTTP status 422. System.Net.HttpStatusCode.UnprocessableEntity
        //     indicates that the request was well-formed but was unable to be followed due
        //     to semantic errors.
        /// <summary>
        /// 无法处理的实体
        /// </summary>
        UnprocessableEntity = 422,

        //
        // 摘要:
        //     Equivalent to HTTP status 423. System.Net.HttpStatusCode.Locked indicates that
        //     the source or destination resource is locked.
        /// <summary>
        /// 锁定
        /// </summary>
        Locked = 423,

        //
        // 摘要:
        //     Equivalent to HTTP status 424. System.Net.HttpStatusCode.FailedDependency indicates
        //     that the method couldn't be performed on the resource because the requested action
        //     depended on another action and that action failed.
        /// <summary>
        /// 依赖失败
        /// </summary>
        FailedDependency = 424,

        //
        // 摘要:
        //     Equivalent to HTTP status 426. System.Net.HttpStatusCode.UpgradeRequired indicates
        //     that the client should switch to a different protocol such as TLS/1.0.
        /// <summary>
        /// 需要升级
        /// </summary>
        UpgradeRequired = 426,

        //
        // 摘要:
        //     Equivalent to HTTP status 428. System.Net.HttpStatusCode.PreconditionRequired
        //     indicates that the server requires the request to be conditional.
        /// <summary>
        /// 需要前提条件
        /// </summary>
        PreconditionRequired = 428,

        //
        // 摘要:
        //     Equivalent to HTTP status 429. System.Net.HttpStatusCode.TooManyRequests indicates
        //     that the user has sent too many requests in a given amount of time.
        /// <summary>
        /// 请求太多
        /// </summary>
        TooManyRequests = 429,

        //
        // 摘要:
        //     Equivalent to HTTP status 431. System.Net.HttpStatusCode.RequestHeaderFieldsTooLarge
        //     indicates that the server is unwilling to process the request because its header
        //     fields (either an individual header field or all the header fields collectively)
        //     are too large.
        /// <summary>
        /// 请求头字段太大
        /// </summary>
        RequestHeaderFieldsTooLarge = 431,

        //
        // 摘要:
        //     Equivalent to HTTP status 451. System.Net.HttpStatusCode.UnavailableForLegalReasons
        //     indicates that the server is denying access to the resource as a consequence
        //     of a legal demand.
        /// <summary>
        /// 因法律原因不可用
        /// </summary>
        UnavailableForLegalReasons = 451,

        //
        // 摘要:
        //     Equivalent to HTTP status 500. System.Net.HttpStatusCode.InternalServerError
        //     indicates that a generic error has occurred on the server.
        /// <summary>
        /// 服务器内部错误
        /// </summary>
        InternalServerError = 500,

        //
        // 摘要:
        //     Equivalent to HTTP status 501. System.Net.HttpStatusCode.NotImplemented indicates
        //     that the server does not support the requested function.
        /// <summary>
        /// 未实现
        /// </summary>
        NotImplemented = 501,

        //
        // 摘要:
        //     Equivalent to HTTP status 502. System.Net.HttpStatusCode.BadGateway indicates
        //     that an intermediate proxy server received a bad response from another proxy
        //     or the origin server.
        /// <summary>
        /// 错误的网关
        /// </summary>
        BadGateway = 502,

        //
        // 摘要:
        //     Equivalent to HTTP status 503. System.Net.HttpStatusCode.ServiceUnavailable indicates
        //     that the server is temporarily unavailable, usually due to high load or maintenance.
        /// <summary>
        /// 服务不可用
        /// </summary>
        ServiceUnavailable = 503,

        //
        // 摘要:
        //     Equivalent to HTTP status 504. System.Net.HttpStatusCode.GatewayTimeout indicates
        //     that an intermediate proxy server timed out while waiting for a response from
        //     another proxy or the origin server.
        /// <summary>
        /// 网关超时
        /// </summary>
        GatewayTimeout = 504,

        //
        // 摘要:
        //     Equivalent to HTTP status 505. System.Net.HttpStatusCode.HttpVersionNotSupported
        //     indicates that the requested HTTP version is not supported by the server.
        /// <summary>
        /// HTTP版本不支持
        /// </summary>
        HttpVersionNotSupported = 505,

        //
        // 摘要:
        //     Equivalent to HTTP status 506. System.Net.HttpStatusCode.VariantAlsoNegotiates
        //     indicates that the chosen variant resource is configured to engage in transparent
        //     content negotiation itself and, therefore, isn't a proper endpoint in the negotiation
        //     process.
        /// <summary>
        /// 506 错误码。表示服务器中的一个资源配置为在内容协商中自行确定表示形式，
        /// 这导致它不是内容协商过程中的适当终点。
        /// </summary>
        VariantAlsoNegotiates = 506,

        //
        // 摘要:
        //     Equivalent to HTTP status 507. System.Net.HttpStatusCode.InsufficientStorage
        //     indicates that the server is unable to store the representation needed to complete
        //     the request.
        /// <summary>
        /// 存储不足
        /// </summary>
        InsufficientStorage = 507,

        //
        // 摘要:
        //     Equivalent to HTTP status 508. System.Net.HttpStatusCode.LoopDetected indicates
        //     that the server terminated an operation because it encountered an infinite loop
        //     while processing a WebDAV request with "Depth: infinity". This status code is
        //     meant for backward compatibility with clients not aware of the 208 status code
        //     System.Net.HttpStatusCode.AlreadyReported appearing in multistatus response bodies.
        /// <summary>
        /// 检测到循环
        /// </summary>
        LoopDetected = 508,

        //
        // 摘要:
        //     Equivalent to HTTP status 510. System.Net.HttpStatusCode.NotExtended indicates
        //     that further extensions to the request are required for the server to fulfill
        //     it.
        /// <summary>
        /// 未扩展
        /// </summary>
        NotExtended = 510,

        #region 数据错误

        /// <summary>
        ///     主键错误
        /// </summary>
        MainKey = 1001,

        /// <summary>
        ///     传入的数据验证失败
        /// </summary>
        Model = 1002,

        /// <summary>
        ///     数据错误
        /// </summary>
        DataError = 1100,

        /// <summary>
        ///     数据不存在
        /// </summary>
        DataNotAvailable = 1101,

        /// <summary>
        ///     数据不完整
        /// </summary>
        DataIncomplete = 1102,

        /// <summary>
        ///     非法数据
        /// </summary>
        DataInvalid = 1103,

        /// <summary>
        ///     重复Post,Put,Delete相同的数据
        /// </summary>
        RateLimitPost = 1104,

        #endregion


        #region 账户错误

        /// <summary>
        ///     账号不存在
        /// </summary>
        LoginAccountError = 8001,

        /// <summary>
        ///     密码不正确
        /// </summary>
        LoginPwdError = 8002,

        /// <summary>
        ///     身份令牌不存在
        /// </summary>
        AccessTokenNull = 8003,

        /// <summary>
        ///     身份令牌不正确
        /// </summary>
        AccessTokenError = 8004,

        /// <summary>
        ///     访问令牌不存在(can't find token)
        /// </summary>
        RefreshTokenNull = 8005,

        /// <summary>
        ///     访问令牌超时(access_token timeout!)
        /// </summary>
        RefreshTokenTimeOut = 8006,

        /// <summary>
        ///     访问令牌不正确(invalid token type,need access token!)
        /// </summary>
        RefreshTokenError = 8007,

        /// <summary>
        ///     user not found!
        /// </summary>
        RefreshTokenUserNotFound = 8008,

        /// <summary>
        ///     没有权限操作
        /// </summary>
        NotAuthorize = 8010,

        /// <summary>
        ///     没有权限操作
        /// </summary>
        RedisUserInfoNull = 8011,

        #endregion


        #region 数据库错误

        /// <summary>
        ///     数据库操作
        /// </summary>
        DataBaseError = 9000,

        /// <summary>
        ///     事务创建失败
        /// </summary>
        TransactionCreateFail = 9001,

        /// <summary>
        ///     事务开启失败
        /// </summary>
        TransactionStartFail = 9002,

        /// <summary>
        ///     事务取消失败
        /// </summary>
        TransactionCancelFail = 9003,

        /// <summary>
        ///     事务提交失败
        /// </summary>
        TransactionCommitFail = 9004,

        /// <summary>
        ///     缓存发生未知错误
        /// </summary>
        CacheDefault = 9100,

        /// <summary>
        ///     缓存发生已知错误
        /// </summary>
        CacheError = 9101,

        #endregion


        #region 业务调用返回错误

        /// <summary>
        ///  其他錯誤
        /// </summary>
        OtherError = 10000,

        /// <summary>
        ///  參數序列化出錯
        /// </summary>
        ParameterSerializationError = 10001,

        /// <summary>
        ///  參數數據為空
        /// </summary>
        ParameterDataEmpty = 10002,

        /// <summary>
        /// 签名错误
        /// </summary>
        SignError = 10037,

        #endregion

        /// <summary>
        /// 字段内容错误（用户模型校验错误）
        /// </summary>
        FieldContentError = 11111
    }
}