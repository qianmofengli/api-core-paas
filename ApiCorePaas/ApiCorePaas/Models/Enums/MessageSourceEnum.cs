﻿namespace ApiCorePaas.Models.Enums;

/// <summary>
/// 消息来源
/// </summary>
public enum MessageSourceEnum
{
    /// <summary>
    /// 设备
    /// </summary>
    Device = 1,

    /// <summary>
    /// DMC服务
    /// </summary>
    Dmc = 2
}