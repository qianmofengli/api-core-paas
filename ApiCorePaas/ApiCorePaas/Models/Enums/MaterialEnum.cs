﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums;

/// <summary>
/// 微信公众号素材枚举
/// </summary>
public enum MaterialEnum
{
    /// <summary>
    /// 图片 10M，支持PNG\JPEG\JPG\GIF格式
    /// </summary>
    [Description("image")]
    Image = 1,

    /// <summary>
    /// 语音 2M，播放长度不超过60s，支持AMR\MP3格式
    /// </summary>
    [Description("voice")]
    Voice = 2,

    /// <summary>
    /// 视频 10MB，支持MP4格式
    /// </summary>
    [Description("video")]
    Video = 3,

    /// <summary>
    /// 缩略图 64KB，支持JPG格式
    /// </summary>
    [Description("thumb")]
    Thumb = 4,

    /// <summary>
    /// 图文素材
    /// </summary>
    [Description("news")]
    News = 5
}