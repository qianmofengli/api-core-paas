﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums;

/// <summary>
/// 设备事件类型
/// </summary>
public enum DeviceEventEnum
{
    /// <summary>
    /// 启动服务
    /// </summary>
    [Description("启动服务")] StartService = 0,

    /// <summary>
    /// 
    /// </summary>
    [Description("账号密码验证")] AccountVerify = 1,

    /// <summary>
    /// 开始连接
    /// </summary>
    [Description("上线")] ConnectStart = 2,

    /// <summary>
    /// 连接断开
    /// </summary>
    [Description("下线")] ConnectClose = 3,

    /// <summary>
    /// 消息发布
    /// </summary>
    [Description("消息发布")] Publish = 4,

    /// <summary>
    /// 订阅
    /// </summary>
    [Description("订阅主题")] Subscribe = 5,

    /// <summary>
    /// 取消订阅
    /// </summary>
    [Description("取消订阅")] Unsubscribe = 6,

    /// <summary>
    /// 无用的发布消息
    /// </summary>
    [Description("无用消息")] NonePublish = 7,

    /// <summary>
    /// 发布拦截
    /// </summary>
    [Description("发布拦截")] PublishInterceptor = 8,
}