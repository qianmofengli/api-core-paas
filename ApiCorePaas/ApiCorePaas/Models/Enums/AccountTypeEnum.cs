﻿namespace ApiCorePaas.Models.Enums;

/// <summary>
/// 账号类型枚举
/// </summary>
public enum AccountTypeEnum
{
    /// <summary>
    /// 微信公众号
    /// </summary>
    WeChatOfficialAccount = 1,
}