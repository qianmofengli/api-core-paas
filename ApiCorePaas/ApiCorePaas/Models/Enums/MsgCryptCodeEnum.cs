﻿namespace ApiCorePaas.Models.Enums;

/// <summary>
/// 
/// </summary>
public enum MsgCryptCodeEnum
{
    /// <summary>
    /// 成功
    /// </summary>
    Success = 0,

    /// <summary>
    /// 签名验证错误
    /// </summary>
    ValidateSignatureError = -40001,

    /// <summary>
    /// xml解析失败
    /// </summary>
    ParseXmlError = -40002,

    /// <summary>
    /// sha加密生成签名失败
    /// </summary>
    ComputeSignatureError = -40003,

    /// <summary>
    /// AESKey 非法
    /// </summary>
    IllegalAesKey = -40004,

    /// <summary>
    /// appid 校验错误
    /// </summary>
    ValidateAppidError = -40005,

    /// <summary>
    /// AES 加密失败
    /// </summary>
    EncryptAesError = -40006,

    /// <summary>
    /// AES 解密失败
    /// </summary>
    DecryptAesError = -40007,

    /// <summary>
    /// 解密后得到的buffer非法
    /// </summary>
    IllegalBuffer = -40008,

    /// <summary>
    /// base64加密异常
    /// </summary>
    EncodeBase64Error = -40009,

    /// <summary>
    /// base64解密异常
    /// </summary>
    DecodeBase64Error = -40010
}