﻿namespace ApiCorePaas.Models.Enums;

/// <summary>
/// MQTT服务质量等级
/// </summary>
public enum MqttQualityOfServiceLevelEnum
{
    /// <summary>
    /// 最多一次
    /// </summary>
    AtMostOnce,

    /// <summary>
    /// 至少一次
    /// </summary>
    AtLeastOnce,

    /// <summary>
    /// 只有一次
    /// </summary>
    ExactlyOnce,
}