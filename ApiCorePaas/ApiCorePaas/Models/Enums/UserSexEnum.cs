﻿using System.ComponentModel;

namespace ApiCorePaas.Models.Enums
{
    /// <summary>
    /// 用户性别
    /// </summary>
    public enum UserSexEnum
    {
        /// <summary>
        ///男
        /// </summary>
        [Description("男")] Man = 1,

        /// <summary>
        /// 女
        /// </summary>
        [Description("女")] Woman = 2,

        /// <summary>
        ///未知
        /// </summary>
        [Description("未知")] Unknown = 3
    }
}