﻿using ApiCorePaas.Models.Enums;
using SqlSugar;

namespace ApiCorePaas.Models.Entities;

/// <summary>
/// 用户账号
/// </summary>
[SugarTable("tab_user_account")]
[Tenant("farmhouseshop")]
public class UserAccount
{
    /// <summary>
    /// 用户账号主键
    /// </summary>
    [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
    public int UserAccountId { get; set; }

    /// <summary>
    /// 用户ID
    /// </summary>
    public int? UserId { get; set; }

    /// <summary>
    /// 开发ID
    /// </summary>
    public string OpenId { get; set; }

    /// <summary>
    /// 开放全局ID
    /// </summary>
    public string UnionId { get; set; }

    /// <summary>
    /// 账号类型
    /// </summary>
    public AccountTypeEnum AccountType { get; set; }

    /// <summary>
    /// 是否关注了公众号
    /// </summary>
    public bool IsSubscribe { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    public DateTime? UpdateTime { get; set; }
}