﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;

namespace ApiCorePaas.Models.Converts;

/// <summary>
/// json输入格式化器
/// </summary>
public class JsonInputFormatter : IInputFormatter
{
    /// <summary>
    ///     是否可以读
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public bool CanRead(InputFormatterContext context)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));

        var contentType = context.HttpContext.Request.ContentType;
        return contentType is null or "application/json";
    }

    /// <summary>
    /// 异步读取数据
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public Task<InputFormatterResult> ReadAsync(InputFormatterContext context)
    {
        if (context == null) throw new ArgumentNullException(nameof(context));

        var request = context.HttpContext.Request;

        if (request.ContentLength == 0)
            return InputFormatterResult.SuccessAsync(context.ModelType.GetTypeInfo().IsValueType
                ? Activator.CreateInstance(context.ModelType)
                : null);

        using var reader = new StreamReader(context.HttpContext.Request.Body);
        var serializer = new JsonSerializer();
        try
        {
            var model = serializer.Deserialize(reader, context.ModelType);
            return InputFormatterResult.SuccessAsync(model);
        }
        catch (Exception)
        {
            return InputFormatterResult.FailureAsync();
        }
    }
}