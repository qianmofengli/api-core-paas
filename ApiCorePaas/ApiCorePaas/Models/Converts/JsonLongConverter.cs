﻿using Newtonsoft.Json;

namespace ApiCorePaas.Models.Converts;

/// <summary>
/// Json格式化数字型转字符类型
/// </summary>
public class JsonLongConverter : JsonConverter
{
    /// <summary>
    ///   是否可以转换
    /// </summary>
    /// <param name="objectType"></param>
    /// <returns></returns>
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(long) || objectType == typeof(long?) || objectType == typeof(int) ||
               objectType == typeof(int?);
    }

    /// <summary>
    ///  读Json数据
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="objectType"></param>
    /// <param name="existingValue"></param>
    /// <param name="serializer"></param>
    /// <returns></returns>
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        return null; // throw new NotImplementedException();
    }

    /// <summary>
    ///  写入Json数据
    /// </summary>
    /// <param name="writer"></param>
    /// <param name="value"></param>
    /// <param name="serializer"></param>
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        writer.WriteValue(value?.ToString());
    }
}