﻿using Newtonsoft.Json;

namespace ApiCorePaas.Models.Converts;

/// <summary>
/// 空字符串转换器
/// </summary>
public class NullToEmptyStringConverter : JsonConverter
{
    /// <summary>
    /// 是否处理空值
    /// </summary>
    /// <param name="objectType"></param>
    /// <returns></returns>
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(string);
    }

    /// <summary>
    /// 读取json
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="objectType"></param>
    /// <param name="existingValue"></param>
    /// <param name="serializer"></param>
    /// <returns></returns>
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (reader.TokenType == JsonToken.Null)
            return string.Empty;

        return reader.Value?.ToString() ?? string.Empty;
    }

    /// <summary>
    /// 写入json
    /// </summary>
    /// <param name="writer"></param>
    /// <param name="value"></param>
    /// <param name="serializer"></param>
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        var stringValue = value as string;
        writer.WriteValue(stringValue ?? string.Empty);
    }
}