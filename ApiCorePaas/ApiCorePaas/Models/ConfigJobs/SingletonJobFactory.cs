﻿using Quartz;
using Quartz.Spi;

namespace ApiCorePaas.Models.ConfigJobs
{
    /// <summary>
    /// 单例作业工厂，用于创建 Quartz 作业
    /// </summary>
    public class SingletonJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;
        
        /// <summary>
        /// 构造函数，初始化作业工厂
        /// </summary>
        /// <param name="serviceProvider"></param>
        public SingletonJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// 创建 Quartz 作业实例
        /// </summary>
        /// <param name="bundle">触发包</param>
        /// <param name="scheduler">调度器</param>
        /// <returns>作业实例</returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            //返回 QuartzJobRunner 作为作业执行器
            return _serviceProvider.GetRequiredService<QuartzJobRunner>();
        }

        /// <summary>
        /// 释放作业实例（不做任何操作）
        /// </summary>
        /// <param name="job">作业实例</param>
        public void ReturnJob(IJob job) { }
    }
}
