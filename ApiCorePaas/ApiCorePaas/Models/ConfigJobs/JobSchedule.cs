﻿namespace ApiCorePaas.Models.ConfigJobs
{
    /// <summary>
    /// 定义作业调度配置，包括作业类型和 Cron 表达式和启动时是否执行一次
    /// </summary>
    public class JobSchedule
    {
        /// <summary>
        /// 构造函数，初始化作业调度配置
        /// </summary>
        /// <param name="jobType">作业类型</param>
        /// <param name="cronExpression">Cron 表达式</param>
        /// <param name="runOnceOnStartup">是否在项目启动时立即执行一次作业 默认false</param>
        public JobSchedule(Type jobType, string cronExpression, bool runOnceOnStartup = false)
        {
            JobType = jobType;
            CronExpression = cronExpression;
            RunOnceOnStartup = runOnceOnStartup;
        }

        /// <summary>
        /// 作业类型
        /// </summary>
        public Type JobType { get; }

        /// <summary>
        /// Cron 表达式
        /// </summary>
        public string CronExpression { get; }
        
        /// <summary>
        /// 是否在项目启动时立即执行一次作业（默认为 false）
        /// </summary>
        public bool RunOnceOnStartup { get; }
    }
}