﻿using Quartz;
using Quartz.Spi;

namespace ApiCorePaas.Models.ConfigJobs
{
    /// <summary>
    /// Quartz 托管服务，用于启动和管理 Quartz 作业和触发器
    /// </summary>
    public class QuartzHostedService : IHostedService
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IEnumerable<JobSchedule> _jobSchedules;

        /// <summary>
        /// 构造函数，初始化托管服务
        /// </summary>
        /// <param name="schedulerFactory">调度器工厂</param>
        /// <param name="jobFactory">作业工厂</param>
        /// <param name="jobSchedules">作业调度配置列表</param>
        public QuartzHostedService(
            ISchedulerFactory schedulerFactory,
            IJobFactory jobFactory,
            IEnumerable<JobSchedule> jobSchedules)
        {
            _schedulerFactory = schedulerFactory;
            _jobSchedules = jobSchedules;
            _jobFactory = jobFactory;
        }

        /// <summary>
        /// 调度器实例
        /// </summary>
        private IScheduler Scheduler { get; set; }

        /// <summary>
        /// 启动 Quartz 调度服务，创建并调度作业和触发器
        /// </summary>
        /// <param name="cancellationToken"></param>
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // 获取调度器实例
            Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = _jobFactory;

            //遍历所有作业调度配置
            foreach (var jobSchedule in _jobSchedules)
            {
                //创建作业
                var job = CreateJob(jobSchedule);
                //创建触发器
                var trigger = CreateTrigger(jobSchedule);
                //将作业和触发器添加到调度器
                await Scheduler.ScheduleJob(job, trigger, cancellationToken);
                // 如果 RunOnceOnStartup 为 true，则在项目启动时立即执行一次作业
                if (jobSchedule.RunOnceOnStartup)
                {
                    await Scheduler.TriggerJob(job.Key, cancellationToken);
                }
        
            }
            //启动调度器
            await Scheduler.Start(cancellationToken);
        }

        /// <summary>
        /// 停止 Quartz 调度服务
        /// </summary>
        /// <param name="cancellationToken"></param>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            //停止调度器并关闭
            await Scheduler?.Shutdown(cancellationToken)!;
        }

        /// <summary>
        /// 创建作业实例
        /// </summary>
        /// <param name="schedule">作业调度配置</param>
        /// <returns>作业实例</returns>
        private static IJobDetail CreateJob(JobSchedule schedule)
        {
            var jobType = schedule.JobType;
            return JobBuilder
                .Create(jobType)
                .WithIdentity(jobType.FullName!)
                .WithDescription(jobType.Name)
                .Build();
        }

        /// <summary>
        /// 创建触发器实例
        /// </summary>
        /// <param name="schedule">作业调度配置</param>
        /// <returns>触发器实例</returns>
        private static ITrigger CreateTrigger(JobSchedule schedule)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{schedule.JobType.FullName}.trigger")
                .WithCronSchedule(schedule.CronExpression)
                .WithDescription(schedule.CronExpression)
                .Build();
        }
    }
}