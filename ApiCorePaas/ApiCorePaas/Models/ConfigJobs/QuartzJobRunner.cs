﻿using Quartz;

namespace ApiCorePaas.Models.ConfigJobs
{
    /// <summary>
    /// Quartz 作业执行器，用于执行 Quartz 作业
    /// </summary>
    public class QuartzJobRunner : IJob
    {
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 构造函数，初始化作业执行器
        /// </summary>
        /// <param name="serviceProvider">服务提供者</param>
        public QuartzJobRunner(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// 执行 Quartz 作业
        /// </summary>
        /// <param name="context">作业执行上下文</param>
        public async Task Execute(IJobExecutionContext context)
        {
            using var scope = _serviceProvider.CreateScope();
            var jobType = context.JobDetail.JobType;
            //根据作业类型获取作业实例并执行作业
            if (scope.ServiceProvider.GetRequiredService(jobType) is IJob job)
            {
                await job.Execute(context);
            }
        }
    }
}