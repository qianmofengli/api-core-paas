﻿using SqlSugar;

namespace ApiCorePaas.Models.SqlSugar
{
    /// <summary>
    /// 
    /// </summary>
    public class SqlSugarRepository<T> : SimpleClient<T> where T : class, new()
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="context"></param>
        public SqlSugarRepository(IServiceProvider serviceProvider, ISqlSugarClient context = null) :
            base(context) //默认值等于null不能少
        {
            var iTenant = serviceProvider.GetRequiredService<ISqlSugarClient>().AsTenant();
            // 若实体贴有多库特性，则返回指定的连接
            if (!typeof(T).IsDefined(typeof(TenantAttribute), false)) return;
            Context = iTenant.GetConnectionScopeWithAttr<T>();
        }
    }
}