﻿using ApiCorePaas.Models.Enums;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;
using SqlSugar;

namespace ApiCorePaas.Models.SqlSugar
{
    /// <summary>
    /// 
    /// </summary>
    public static class SqlSugarSetup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddSqlSugar(this IServiceCollection services)
        {
            var farmhouseShop = DatabaseEnum.FarmhouseShop.GetDescription();

            var configConnection = new List<ConnectionConfig>()
            {
                // new()
                // {
                //     ConfigId = core,
                //     DbType = DbType.MySql,
                //     ConnectionString = AppSettings.MySqlConnectionStringOption.Core,
                //     IsAutoCloseConnection = true,
                //     MoreSettings = new ConnMoreSettings()
                //     {
                //         DisableNvarchar = true
                //     }
                // },
                new()
                {
                    ConfigId = farmhouseShop,
                    DbType = DbType.MySql,
                    ConnectionString = AppSettings.MySqlConnectionStringOption.FarmhouseShop,
                    IsAutoCloseConnection = true,
                    MoreSettings = new ConnMoreSettings()
                    {
                        DisableNvarchar = true
                    }
                }
            };
            // var userNo = App.User?.FindFirstValue("UserNo");
            // var enterpriseId = App.User?.FindFirstValue("EnterpriseId");
            // var SystemId = App.User?.FindFirstValue("SystemId");
            // var UserId = App.User?.FindFirstValue("UserId");
            var sqlSugar = new SqlSugarScope(configConnection,
                db =>
                {
                    db.GetConnection(farmhouseShop).Aop.OnLogExecuting = (sql, pars) =>
                    {
#if DEBUG
                        var sqlString = UtilMethods.GetSqlString(DbType.MySql, sql, pars);
                        Console.WriteLine($"[FarmhouseShop sql]：{sqlString}");
#endif
                    };
//                     db.GetConnection(iot).Aop.OnLogExecuting = (sql, pars) =>
//                     {
// #if DEBUG
//                         var sqlString = UtilMethods.GetSqlString(DbType.MySql, sql, pars);
//                         Console.WriteLine($"[paas_iot sql]：{sqlString}");
// #endif
//                     };

                    // 数据隔离
                    // db.GetConnection(iot).QueryFilter
                    //     .AddTableFilter<IIsolate>(it => it.EnterpriseId.Equals(Application.CurrentUser.EnterpriseId));
                });
            services.AddSingleton<ISqlSugarClient>(sqlSugar);
            services.AddScoped(typeof(SqlSugarRepository<>)); // 仓储注册
        }
    }
}