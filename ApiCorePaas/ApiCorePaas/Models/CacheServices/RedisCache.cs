﻿using System.Collections.Concurrent;
using System.Net;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;
using Serilog;
using StackExchange.Redis;

namespace ApiCorePaas.Models.CacheServices;

/// <summary>
/// 
/// </summary>
public class RedisCache : CacheBase
{
    private IDatabase _db;
    private EndPoint[] _EndPoint;
    private int _dbIndex = 7;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private static readonly object Locker = new object();
    private ConnectionMultiplexer _connection;
    private readonly ILogger<RedisCache> _logger;

    private static readonly ConcurrentDictionary<string, ConnectionMultiplexer> ConnectionCache =
        new ConcurrentDictionary<string, ConnectionMultiplexer>();

    /// <summary>
    /// 
    /// </summary>
    // private static ConnectionMultiplexer Instance
    // {
    //     get
    //     {
    //         // ReSharper disable once InvertIf
    //         if (_instance == null)
    //         {
    //             lock (Locker)
    //             {
    //                 _instance ??= GetManager(Startup.SystemConfig.RedisConnection, Startup.SystemConfig.RedisDbIndex);
    //             }
    //         }
    //         return _instance;
    //     }
    // }
    public RedisCache(int dbIndex,  IServiceCollection services = null)
    {
        try
        {
            if (services != null)
            {
                var provider = services.BuildServiceProvider();
                _httpContextAccessor = provider.GetRequiredService<IHttpContextAccessor>();
            }

            _connection = GetManager(AppSettings.RedisOption.RedisConnection, dbIndex);
        }
        catch (Exception e)
        {
            Log.Error($"连接Redis失败，请检查Redis状态，否则无法正常使用服务\r\nMsg:{e.Message}\r\nInfo:{e.StackTrace}");
        }
    }

    /// <summary>
    /// 检查Redis数据库状态
    /// </summary>
    /// <returns></returns>
    public override async Task<bool> CheckCacheStatus()
    {
        try
        {
            if (_db == null)
                throw new Exception("获取Redis数据库失败，请检查Redis状态，否则无法正常使用服务");

            await _db.PingAsync();
            return true;
        }
        catch (Exception e)
        {
            Log.Error($"PING Redis DB失败，请检查Redis状态，否则无法正常使用服务\r\nMsg:{e.Message}\r\nInfo:{e.StackTrace}");
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public override async Task<bool> Delete(string key)
    {
        try
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));
            return await _db.KeyDeleteAsync(key);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    //public override bool Exists(string key)
    //{
    //    try
    //    {
    //        if (key == null)
    //            throw new ArgumentNullException(nameof(key));

    //        var (_, e) = Get(key).Result;
    //        return e;
    //    }
    //    catch (Exception e)
    //    {
    //        ConsoleLog.Error(e.Message);
    //        ConsoleLog.Error(e.StackTrace);
    //        return false;
    //    }
    //}
    public override bool Exists(string key)
    {
        try
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            return _db.KeyExists(key);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public override async Task<(string, bool)> Get(string key)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));

            var result = await _db.StringGetAsync(key);
            return (result.ToString(), true);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return (default, false);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key"></param>
    /// <returns></returns>
    public override async Task<(T, bool)> Get<T>(string key)
    {
        try
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            var result = await _db.StringGetAsync(key);
            if (result.IsNullOrEmpty)
                return (default(T), true);
            var (value, error) = Util.Deserialize<T>(result.ToString());
            if (error != null)
                throw error;
            return (value, true);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return (default(T), false);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="expire"></param>
    /// <returns></returns>
    public override async Task<bool> Set(string key, string value, TimeSpan? expire = null)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            return await _db.StringSetAsync(key, value, expire);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="expire"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public async Task<bool> SetNotExists(string key, string value, TimeSpan? expire = null)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            return await _db.StringSetAsync(key, value, expire, When.NotExists);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="expire"></param>
    /// <returns></returns>
    public override async Task<bool> Set<T>(string key, T value, TimeSpan? expire = null)
    {
        return await Set(key, Util.Serialize(value), expire);
    }

    /// <summary>
    /// 增量
    /// </summary>
    /// <param name="key"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public async Task<long> StringIncrementAsync(string key, long quantity = 1)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));

            return await _db.StringIncrementAsync(key, quantity);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return default;
        }
    }

    /// <summary>
    /// 减量
    /// </summary>
    /// <param name="key"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public async Task<long> StringDecrementAsync(string key, long quantity = 1)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));
            return await _db.StringDecrementAsync(key, quantity);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return default;
        }
    }

    /// <summary>
    /// 设置有效期
    /// </summary>
    /// <param name="key"></param>
    /// <param name="expiry"></param>
    /// <returns></returns>
    public async Task<bool> KeyExpireAsync(string key, TimeSpan? expiry)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));
            return await _db.KeyExpireAsync(key, expiry);
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 模糊查询并删除
    /// </summary>
    /// <param name="fuzzyStr"></param>
    /// <returns></returns>
    public async Task<bool> FuzzyQueryWithDelete(string fuzzyStr)
    {
        try
        {
            foreach (var ep in _connection.GetEndPoints())
            {
                var server = _connection.GetServer(ep);
                //在指定服务器上使用 keys 或者 scan 命令来遍历key


                var redisResult = await _db.ScriptEvaluateAsync(LuaScript.Prepare(
                    " local res = redis.call('KEYS', @keypattern) " +
                    " return res "), new { @keypattern = fuzzyStr });

                //var redisResult = server.Keys(_dbIndex, fuzzyStr);
                //var aa = redisResult.ToArray();
                if (redisResult != null)
                {
                    var result = await _db.KeyDeleteAsync((RedisKey[])redisResult);
                    if (result < 1)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 模糊查询并删除
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    public async Task<bool> FuzzyQueryWithDelete(List<string> keys)
    {
        try
        {
            if (keys?.Any() ?? false)
                await _db.KeyDeleteAsync(keys.Select(x => (RedisKey)x).ToArray());
            return true;
        }
        catch (Exception e)
        {
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// 释放锁
    /// </summary>
    /// <param name="lockKey"></param>
    /// <param name="lockValue"></param>
    public async Task ReleaseLockAsync(string lockKey, string lockValue)
    {
        const string script = @"
            if redis.call('get', KEYS[1]) == ARGV[1] then
                return redis.call('del', KEYS[1])
            else
                return 0
            end";
        await _db.ScriptEvaluateAsync(script, new RedisKey[] { lockKey }, new RedisValue[] { lockValue });
    }

    private ConnectionMultiplexer GetManager(string connectionString = null, int dbIndex = -1)
    {
        var connect = ConnectionMultiplexer.Connect(connectionString);
        _db = connect.GetDatabase(dbIndex);
        _EndPoint = connect.GetEndPoints();
        //注册如下事件
        connect.ConnectionFailed += MuxerConnectionFailed;
        connect.ConnectionRestored += MuxerConnectionRestored;
        connect.ErrorMessage += MuxerErrorMessage;
        connect.ConfigurationChanged += MuxerConfigurationChanged;
        connect.HashSlotMoved += MuxerHashSlotMoved;
        connect.InternalError += MuxerInternalError;

        return connect;
    }

    #region 事件

    /// <summary>
    /// 配置更改时
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void MuxerConfigurationChanged(object sender, EndPointEventArgs e)
    {
        Log.Information($"Configuration changed: " + e.EndPoint);
    }

    /// <summary>
    /// 发生错误时
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void MuxerErrorMessage(object sender, RedisErrorEventArgs e)
    {
        Log.Error($"ErrorMessage: " + e.Message);
    }

    /// <summary>
    /// 重新建立连接之前的错误
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void MuxerConnectionRestored(object sender, ConnectionFailedEventArgs e)
    {
        Log.Error($"ConnectionRestored: " + e.EndPoint);
    }

    /// <summary>
    /// 连接失败 ， 如果重新连接成功你将不会收到这个通知
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void MuxerConnectionFailed(object sender, ConnectionFailedEventArgs e)
    {
        Log.Error($"重新连接：Endpoint failed: " + e.EndPoint + ", " + e.FailureType +
                  (e.Exception == null ? "" : (", " + e.Exception.Message)));
    }

    /// <summary>
    /// 更改集群
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void MuxerHashSlotMoved(object sender, HashSlotMovedEventArgs e)
    {
        Log.Information($"HashSlotMoved:NewEndPoint" + e.NewEndPoint + ", OldEndPoint" + e.OldEndPoint);
    }

    /// <summary>
    /// redis类库错误
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void MuxerInternalError(object sender, InternalErrorEventArgs e)
    {
        Log.Error($"InternalError:Message:{e.Exception.Message}");
    }

    #endregion 事件
}