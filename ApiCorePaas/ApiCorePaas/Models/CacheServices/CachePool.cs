﻿using ApiCorePaas.Options;

namespace ApiCorePaas.Models.CacheServices;

/// <summary>
/// 缓存
/// </summary>
public class CachePool
{
    /// <summary>
    /// 公众号
    /// </summary>
    public static RedisCache OfficialAccountRedisCache { get; private set; }

    /// <summary>
    /// 文心一言
    /// </summary>
    public static RedisCache ErnieBotRedisCache { get; private set; }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static bool Init(IServiceCollection services)
    {
        OfficialAccountRedisCache = new RedisCache(AppSettings.RedisOption.OfficialAccountRedisCache, services);
        ErnieBotRedisCache = new RedisCache(AppSettings.RedisOption.ErnieBotRedisCache, services);
        return true;
    }
}