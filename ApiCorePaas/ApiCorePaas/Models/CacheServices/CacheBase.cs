﻿namespace ApiCorePaas.Models.CacheServices;

/// <summary>
/// 缓存基类
/// </summary>
public abstract class CacheBase
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public abstract bool Exists(string key);

    /// <summary>
    /// 检查缓存状态
    /// </summary>
    /// <returns></returns>
    public abstract Task<bool> CheckCacheStatus();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="expire"></param>
    /// <returns></returns>
    public abstract Task<bool> Set(string key, string value, TimeSpan? expire = null);

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="expire"></param>
    /// <returns></returns>
    public abstract Task<bool> Set<T>(string key, T value, TimeSpan? expire = null);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public abstract Task<(string, bool)> Get(string key);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public abstract Task<(T, bool)> Get<T>(string key);


    /// <summary>
    /// 删除
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public abstract Task<bool> Delete(string key);
}