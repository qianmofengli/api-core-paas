﻿using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.ErrorAndResult;

namespace ApiCorePaas.Models.Exceptions;

/// <summary>
/// 自定义错误类
/// </summary>
public class CustomException : ApplicationException
{
    /// <summary>
    ///     错误码
    /// </summary>
    public ErrorEnum Code { get; }

    /// <summary>
    ///     构造函数
    /// </summary>
    public CustomException()
    {
        Code = ErrorEnum.Default;
    }

    /// <summary>
    ///     构造函数
    /// </summary>
    /// <param name="code">错误码，用ExceptionCode类定义</param>
    public CustomException(ErrorEnum code)
    {
        Code = code;
    }

    /// <summary>
    ///     构造函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="code">错误码，用ExceptionCode类定义</param>
    public CustomException(string message, ErrorEnum code = ErrorEnum.Default) : base(message)
    {
        Code = code;
    }

    /// <summary>
    ///     构造函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="error"></param>
    /// <param name="code">错误码，用ExceptionCode类定义</param>
    public CustomException(string message, ErrorInfo error, ErrorEnum code = ErrorEnum.Default) :
        base($"{message}; 上层错误代码：{error.ErrorCode},上层错误信息{error.ErrorMessage}")
    {
        Code = code;
    }

    /// <summary>
    ///     构造函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    /// <param name="code">错误码，用ExceptionCode类定义</param>
    public CustomException(string message, Exception innerException, ErrorEnum code = ErrorEnum.Default) : base(
        message, innerException)
    {
        Code = code;
    }

    /// <summary>
    ///     构造函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    /// <param name="error"></param>
    /// <param name="code"></param>
    public CustomException(string message, Exception innerException, ErrorInfo error,
        ErrorEnum code = ErrorEnum.Default) : base(
        $"{message}; 上层错误代码：{error.ErrorCode},上层错误信息{error.ErrorMessage}", innerException)
    {
        Code = code;
    }

    /// <summary>
    ///     重写ToString,添加了错误码
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return $"错误代码:{Code},错误详情{base.ToString()}";
    }
}