﻿using System.Reflection;
using ApiCorePaas.Jobs;
using ApiCorePaas.Mapster;
using ApiCorePaas.Models.CacheServices;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.ConfigJobs;
using ApiCorePaas.Models.Converts;
using ApiCorePaas.Models.DependencyInjection;
using ApiCorePaas.Models.Filters;
using ApiCorePaas.Models.SqlSugar;
using ApiCorePaas.Options;
using Mapster;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using QMFL.MongoMultiDbRepository;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace ApiCorePaas.Models.Extensions;

/// <summary>
/// 
/// </summary>
public static class CustomExtensionsMethodsExtension
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddTransientHandlers(this IServiceCollection services)
    {
        // 使用反射来批量注册处理类为瞬时服务

        var transientTypes = Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(type => typeof(ITransient).IsAssignableFrom(type) && !type.IsInterface && !type.IsAbstract);

        foreach (var transientType in transientTypes)
        {
            services.AddTransient(transientType, transientType);
        }

        return services;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    public static IServiceCollection AddCustomService(this IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddPolicy("any",
                corsPolicyBuilder => corsPolicyBuilder
                    .SetIsOriginAllowed(_ => true)
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("TraceId"));
        });

        //关闭模型验证
        services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);
        services.AddControllers(options =>
            {
                // options.Filters.Add(new ProducesAttribute("application/json"));
                options.InputFormatters.Insert(0, new JsonInputFormatter());
                options.Filters.Add(typeof(ModelValidationFilter)); // 注册全局过滤器
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.Converters.Add(new JsonLongConverter());
                options.SerializerSettings.Converters.Add(new NullToEmptyStringConverter());
                // options.SerializerSettings.ContractResolver = null;
                // options.SerializerSettings.ContractResolver = new DefaultContractResolver
                // {
                //     NamingStrategy = new SnakeCaseNamingStrategy() // 使用蛇形命名策略
                // };
            });

        // 创建 Mapster 配置实例
        var config = new TypeAdapterConfig();

        // 调用您的注册映射方法
        MappingConfig.RegisterMappings(config);

        // 将 Mapster 配置作为单例添加到服务中
        services.AddSingleton(config);

        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        services.AddHttpClient();

        ClientPool.Init(services);
        CachePool.Init(services);
        return services;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddCustomDbContext(this IServiceCollection services)
    {
        services.AddSqlSugar();

        // 配置MongoDB连接
        services.AddMongoMultiDbRepository(options =>
        {
            options.Connections = new Dictionary<string, string>
            {
                {
                    "IotLog", AppSettings.MongoConnectionStringOption.IotLog
                },
                {
                    "OperationLog", AppSettings.MongoConnectionStringOption.OperationLog
                }
            };
        });

        return services;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddQuartzConfig(this IServiceCollection services)
    {
        services.AddSingleton<QuartzJobRunner>();
        services.AddSingleton<IJobFactory, SingletonJobFactory>();
        services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

        services.AddScoped<WeChatOfficialAccountAccessTokenJob>();
        services.AddSingleton(new JobSchedule(jobType: typeof(WeChatOfficialAccountAccessTokenJob),
            cronExpression: "0 55 * * * ?",true));
        
        
        services.AddScoped<ErnieBotAccessTokenJob>();
        services.AddSingleton(new JobSchedule(jobType: typeof(ErnieBotAccessTokenJob),
            cronExpression: "0 0 0 5,20 * ? *",true));
        // services.AddSingleton(new JobSchedule(jobType: typeof(ErnieBotAccessTokenJob),
        //     cronExpression: "0/2 * * * * ?"));

        services.AddSingleton<IHostedService, QuartzHostedService>();

        return services;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGenNewtonsoftSupport();
        services.AddSwaggerGen(c =>
        {
            c.OrderActionsBy(apiDesc => apiDesc.HttpMethod);

            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "",
            });

            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
            c.SwaggerDoc("web", new OpenApiInfo {Title = "平台", Version = "v1"});
            c.SwaggerDoc("applet", new OpenApiInfo {Title = "小程序", Version = "v1"});
            c.SwaggerDoc("officialAccount", new OpenApiInfo {Title = "公众号", Version = "v1"});
            //添加请求头部参数
            //c.OperationFilter<AddRequiredHeaderParameter>();
        });

        return services;
    }
}