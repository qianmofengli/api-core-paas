﻿using System.Security.Cryptography;
using ApiCorePaas.DataModels;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.Exceptions;
using ApiCorePaas.Tools;
using JWT.Builder;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace ApiCorePaas.Models.Attributes;

/// <summary>
/// 登录认证
/// </summary>
public class LoginAuthorize : Attribute, IAsyncAuthorizationFilter
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
    {
        await AuthorizeDo(context);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    private Task AuthorizeDo(AuthorizationFilterContext context)
    {
        context.HttpContext.Response.ContentType = "application/json";
        var token = context.HttpContext.Request.Headers["Authorization"].ToString();
        if (string.IsNullOrEmpty(token))
        {
            throw new CustomException("请登录", ErrorEnum.Unauthorized);
        }
        try
        {
            var publicKey = Convert.FromBase64String(
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA69ySkbz9zSfPZWB7PMJu09NoRoD1StxBJKPGenBnQxwQKulyGaksVCAacp1ERNumJh7XsdwyYbFQxzsgQWz8Gfn+ONBXLBcEpEo+q1/BHOBTGFFYtVCynvyCTCA/KyYUBVKUCpBKISKT7vr8GevaMx/AD9dEF79cxqv/eosueMVdvttuTyVEKCWdvfQo70Q4cZOfgx+7PwSUi17C8TtVk3iMgLVupEc07ZJFeiQL8+qOQzfpsTFYvxMKPuKCfrubyqS+MuDBwCxdY37pGNIxAb7pCrjU98aVvJ/4zs4uKpPzfcrPx1nFC5ZlzOD0wYmQwM8xe1s4hNBgsL33oeFGLwIDAQAB");
            var rsa = RSA.Create();
            rsa.ImportSubjectPublicKeyInfo(publicKey, out _);

            var payloadInfoJson = JwtBuilder.Create()
                .WithAlgorithm(new JWT.Algorithms.RS256Algorithm(rsa))
                .WithSecret(publicKey)
                .MustVerifySignature()
                .Decode(token);

            var (payloadInfo, error) = Util.Deserialize<TokenPayloadInfoModel>(payloadInfoJson);
            if (error is not null)
            {
                throw error;
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex, "登录验证失败: {ExMessage}", ex.Message);
            if (context.HttpContext.Response.HasStarted)
                throw new Exception($"登录错误，请重新登录。{ex.Message}");
            if (ex.Message.Equals("Token has expired."))
                throw new CustomException("请登录", ErrorEnum.Unauthorized);

            throw new CustomException($"登录异常，请联系管理员 {ex.Message}", ErrorEnum.InternalServerError);
        }

        return Task.CompletedTask;
    }
}