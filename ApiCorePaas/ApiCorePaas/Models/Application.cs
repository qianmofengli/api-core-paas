﻿using ApiCorePaas.Entities;

namespace ApiCorePaas.Models;

/// <summary>
/// 
/// </summary>
public class Application
{
    /// <summary>
    /// 当前用户信息
    /// </summary>
    public static CurrentUserModel CurrentUser { get; set; }
}