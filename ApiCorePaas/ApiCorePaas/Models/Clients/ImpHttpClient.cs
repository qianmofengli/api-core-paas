﻿using System.Net.Http.Headers;
using System.Text;
using System.Threading.Channels;
using ApiCorePaas.Tools;
using Serilog;

namespace ApiCorePaas.Models.Clients;

/// <summary>
/// 
/// </summary>
public class ImpHttpClient : IClient
{
    private readonly IHttpClientFactory _clientFactory;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly string _clientName;
    private readonly Uri _uri;

    /// <summary>
    /// sse数据 队列
    /// </summary>
    public Channel<string> SseChannel { get; } = Channel.CreateUnbounded<string>();

    /// <summary>
    ///     初始化方法
    /// </summary>
    /// <param name="services"></param>
    /// <param name="uri"></param>
    /// <param name="clientName"></param>
    public ImpHttpClient(IServiceCollection services, Uri uri, string clientName)
    {
        var provider = services.BuildServiceProvider();
        _clientFactory = provider.GetRequiredService<IHttpClientFactory>();
        _httpContextAccessor = provider.GetRequiredService<IHttpContextAccessor>();
        _clientName = clientName;
        _uri = uri;
        services.AddHttpClient(_clientName, c => { c.BaseAddress = _uri; })
            .ConfigurePrimaryHttpMessageHandler(() =>
            {
                return new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, cert, chain, error) => true
                };
            });

        //管道
        services.BuildServiceProvider(validateScopes: true);
    }

    /// <summary>
    ///     Post方法 返回byte[]
    /// </summary>
    /// <param name="jsonStr"></param>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task<(byte[], Exception)> BytePostAsync(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var res = await httpClient.PostAsync(url, Util.CreateContent(jsonStr));
            var value = await res.Content.ReadAsByteArrayAsync();
            var contentType = res.Content.Headers.ContentType.MediaType;
            _httpContextAccessor.HttpContext.Response.ContentType = contentType;
            return (value, null);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, e);
        }
    }

    /// <summary>
    ///      Post方法 返回HttpResponseMessage
    /// </summary>
    /// <param name="jsonStr">Json字符串</param>
    /// <param name="url">请求路径或者地址</param>
    /// <param name="headers">请求头</param>
    /// <returns></returns>
    public async Task<(HttpResponseMessage, Exception)> HttpResponseMessagePostAsync(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var httpResponseMessage = await httpClient.PostAsync(url, Util.CreateContent(jsonStr));
            return (httpResponseMessage, null);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, e);
        }
    }

    public async Task<(T, Exception)> DeleteAsync<T>(string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var res = await httpClient.DeleteAsync(url);
            var value = await res.Content.ReadAsStringAsync();
            return Util.Deserialize<T>(value);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, e);
        }
    }

    public async Task<(T, Exception)> SendHttpPostSteamAsync<T>(MemoryStream stream, string fileName, string url = "")
    {
        try
        {
            var formData = new MultipartFormDataContent();
            formData.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("multipart/form-data");
            formData.Add(new StreamContent(stream), "buffer", "\"" + fileName + "\"");


            var httpClient = GetHttpClient();
            var res = await httpClient.PostAsync(url, formData);
            var value = await res.Content.ReadAsStringAsync();
            return Util.Deserialize<T>(value);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, e);
        }
    }

    /// <summary>
    ///     Get方法
    /// </summary>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task<(T, Exception)> GetAsync<T>(string url,
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var res = await httpClient.GetAsync(url);
            var value = await res.Content.ReadAsStringAsync();
            Log.Information($"{url} 相应内容为: {value}");
            return Util.Deserialize<T>(value);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, e);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task<(string, Exception)> StringGetAsync(string url, List<KeyValuePair<string, string>> headers)
    {
        var (value, getErr) = await GetAsync(url, headers);
        if (getErr != null)
            return (null, getErr);
        var result = await value.Content.ReadAsStringAsync();
        return (result, null);
    }

    /// <summary>
    ///     
    /// </summary> 
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task<(HttpResponseMessage, Exception)> GetHttpResponseMessageAsync(string url,
        List<KeyValuePair<string, string>> headers = null)
    {
        if (string.IsNullOrEmpty(url))
            return (null, new Exception("未配置"));
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            httpClient.Timeout = TimeSpan.FromSeconds(3);
            var res = await httpClient.GetAsync(url);
            return (res, null);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, e);
        }
    }

    public async Task<(T, Exception)> SendAsync<T>(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        return await PostAsync<T>(jsonStr, url, headers);
    }

    public async Task<(T, Exception)> SendPutAsync<T>(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        return await PutAsync<T>(jsonStr, url, headers);
    }

    public Task<(T, Exception)> SendAsync<T>(object obj, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        return PostAsync<T>(obj, url, headers);
    }

    /// <summary>
    /// Put
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public Task<(T, Exception)> SendPutAsync<T>(object obj, string url = "",
        List<KeyValuePair<string, string>> headers = null)
    {
        return PutAsync<T>(Util.Serialize(obj), url, headers);
    }

    /// <summary>
    ///     获取一个HttpClient
    /// </summary>
    /// <returns></returns>
    private HttpClient GetHttpClient()
    {
        var client = _clientFactory.CreateClient(_clientName);
        client.BaseAddress = _uri;
        return client;
    }

    /// <summary>
    ///     设置头部信息
    /// </summary>
    /// <param name="httpClient">HttpClient实例</param>
    /// <param name="headers">头部信息</param>
    private void SetHeaders(HttpClient httpClient, IEnumerable<KeyValuePair<string, string>> headers)
    {
        if (headers == null) return;
        foreach (var (key, s) in headers)
        {
            httpClient.DefaultRequestHeaders.Remove(key);
            httpClient.DefaultRequestHeaders.Add(key, s);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public async Task<(T, Exception)> PostAsync<T>(object obj, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        return await PostAsync<T>(Util.Serialize(obj), url, headers);
    }

    /// <summary>
    ///     Post方法
    /// </summary>
    /// <param name="jsonStr"></param>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task<(T, Exception)> PostAsync<T>(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        var (value, error) = await StringPostAsync(jsonStr, url, headers);
        return error != null ? (default, error) : Util.Deserialize<T>(value);
    }

    private async Task<(T, Exception)> PutAsync<T>(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        var (value, error) = await StringPutAsync(jsonStr, url, headers);
        return error != null ? (default, error) : Util.Deserialize<T>(value);
    }

    /// <summary>
    /// MemoryStream Post
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="fileName"></param>
    /// <param name="url"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public async Task<(T, Exception)> SteamPostAsync<T>(MemoryStream stream, string fileName,
        string url = "")
    {
        try
        {
            var formData = new MultipartFormDataContent();
            formData.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
            formData.Add(new StreamContent(stream), "buffer", "\"" + fileName + "\"");

            var httpClient = GetHttpClient();
            var res = await httpClient.PostAsync(url, formData);
            var value = await res.Content.ReadAsStringAsync();
            return Util.Deserialize<T>(value);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url,
                e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url,
                e.StackTrace);
            return (default, e);
        }
    }


    /// <summary>
    /// Post方法 
    /// </summary>
    /// <param name="base64String"></param>
    /// <param name="fileName"></param>
    /// <param name="url"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public async Task<(T, Exception)> SteamPostAsync<T>(string base64String, string fileName,
        string url = "")
    {
        try
        {
            var fileContent = new StreamContent(new MemoryStream(Convert.FromBase64String(base64String)));
          
            // 获取文件名
            var safeFileName = Path.GetFileName(fileName);

            // 设置文件类型
            fileContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg"); // 根据实际情况更改
            // 添加文件
            var formData = new MultipartFormDataContent();
            formData.Add(fileContent, "media", "\"" + safeFileName + "\"");
            
            var httpClient = GetHttpClient();
            var res = await httpClient.PostAsync(url, formData);
            var value = await res.Content.ReadAsStringAsync();
            return Util.Deserialize<T>(value);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url,
                e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url,
                e.StackTrace);
            return (default, e);
        }
    }

    /// <summary>
    ///     Post方法 返回string
    /// </summary>
    /// <param name="jsonStr"></param>
    /// <param name="url"></param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task<(string, Exception)> StringPostAsync(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var res = await httpClient.PostAsync(url, Util.CreateContent(jsonStr));
            var value = await res.Content.ReadAsStringAsync();
            return (value, null);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, e);
        }
    }


    private async Task<(string, Exception)> StringPutAsync(string jsonStr, string url = "",
        IEnumerable<KeyValuePair<string, string>> headers = null)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var res = await httpClient.PutAsync(url, Util.CreateContent(jsonStr));
            var value = await res.Content.ReadAsStringAsync();
            return (value, null);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3} Content{4}", e.Message, _uri.ToString(), url,
                e.StackTrace, jsonStr);
            return (default, e);
        }
    }

    private async Task<(HttpResponseMessage, Exception)> GetAsync(string url,
        IEnumerable<KeyValuePair<string, string>> headers)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            var res = await httpClient.GetAsync(url);
            return (res, null);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, e);
        }
    }

    /// <summary>
    /// 文件流
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="stream"></param>
    /// <param name="fileName"></param>
    /// <param name="url"></param>
    /// <param name="description"></param>
    /// <returns></returns>
    public async Task<(T, Exception)> SendHttpPostSteamAsync<T>(MemoryStream stream, string fileName, string url = "",
        string description = "")
    {
        try
        {
            string boundary = string.Format("--{0}", DateTime.Now.Ticks.ToString("x"));
            var requestContent = new MultipartFormDataContent(boundary);
            requestContent.Headers.ContentType =
                MediaTypeHeaderValue.Parse("multipart/form-data"); //这里必须添加

            if (!string.IsNullOrEmpty(description))
            {
                requestContent.Add(Util.CreateContent(description), "\"description\""); //这里主要必须要双引号
            }

            var byteArrayContent = new ByteArrayContent(stream.ToArray());
            byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            requestContent.Add(byteArrayContent, "\"media\"", "\"" + fileName + "\""); //这里主要必须要双引号
            var httpClient = GetHttpClient();

            var res = await httpClient.PostAsync(url, requestContent);
            var value = await res.Content.ReadAsStringAsync();

            return Util.Deserialize<T>(value);
        }
        catch (HttpRequestException e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, new Exception("请求异常：" + e.Message));
        }
        catch (Exception e)
        {
            Log.Error("Msg:{0} BaseUrl:{1} Url:{2} Stack:{3}", e.Message, _uri.ToString(), url, e.StackTrace);
            return (default, e);
        }
    }

    /// <summary>
    /// 连接到 SSE 服务并接收数据
    /// </summary>
    /// <param name="jsonStr"></param>
    /// <param name="url">SSE 服务的 URL</param>
    /// <param name="headers"></param>
    /// <returns></returns>
    public async Task ConnectToSseAsync(string jsonStr, string url,
        IEnumerable<KeyValuePair<string, string>> headers)
    {
        try
        {
            var httpClient = GetHttpClient();
            SetHeaders(httpClient, headers);
            using var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = Util.CreateContent(jsonStr);
            using var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
            response.EnsureSuccessStatusCode();

            await using var stream = await response.Content.ReadAsStreamAsync();
            using var reader = new StreamReader(stream);
            while (!reader.EndOfStream)
            {
                var line = await reader.ReadLineAsync();
                if (string.IsNullOrEmpty(line) || !line.StartsWith("data:"))
                {
                    continue;
                }

                var data = line[5..].Trim();
                await SseChannel.Writer.WriteAsync(data);
            }
        }
        catch (HttpRequestException e)
        {
            Log.Error("SSE Connection Error: " + e.Message);
        }
        catch (Exception e)
        {
            Log.Error("SSE General Error: " + e.Message);
        }
    }
}