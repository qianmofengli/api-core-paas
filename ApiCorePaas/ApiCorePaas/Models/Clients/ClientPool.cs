﻿using ApiCorePaas.Options;

namespace ApiCorePaas.Models.Clients;

/// <summary>
/// 
/// </summary>
public static class ClientPool
{
    /// <summary>
    /// ChatGpt
    /// </summary>
    public static ImpHttpClient ChatGptClient { get; private set; }

    /// <summary>
    /// 微信
    /// </summary>
    public static ImpHttpClient WeChatClient { get; private set; }

    /// <summary>
    /// 文心一言
    /// </summary>
    public static ImpHttpClient ErnieBotClient { get; private set; }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static bool Init(IServiceCollection services)
    {
        if (Uri.TryCreate($"https://{AppSettings.ChatGptOption.Url}/", UriKind.Absolute,
                out var chatBaseUrl))
            ChatGptClient =
                new ImpHttpClient(services, chatBaseUrl, nameof(AppSettings.ChatGptOption.Url));
        if (Uri.TryCreate($"https://{AppSettings.WeChatOption.HostUrl}/", UriKind.Absolute,
                out var weChatBaseUrl))
            WeChatClient =
                new ImpHttpClient(services, weChatBaseUrl, nameof(AppSettings.WeChatOption.HostUrl));
        if (Uri.TryCreate($"https://{AppSettings.ErnieBotOption.Url}/", UriKind.Absolute,
                out var ernieBotBaseUrl))
            ErnieBotClient =
                new ImpHttpClient(services, ernieBotBaseUrl, nameof(AppSettings.ErnieBotOption.Url));

        return true;
    }
}