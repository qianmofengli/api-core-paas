﻿namespace ApiCorePaas.Models.Clients;

/// <summary>
///     客户端抽象
/// </summary>
public interface IClient
{
    /// <summary>
    ///     异步发送数据
    /// </summary>
    /// <typeparam name="T">泛型对象</typeparam>
    /// <param name="jsonStr">Json字符串</param>
    /// <param name="url">地址</param>
    ///  <param name="headers">请求头</param>
    /// <returns></returns>
    Task<(T, Exception)> SendAsync<T>(string jsonStr, string url = "", IEnumerable<KeyValuePair<string, string>> headers = null);

    /// <summary>
    ///     异步发送数据
    /// </summary>
    /// <typeparam name="T">泛型对象</typeparam>
    /// <param name="obj">Json字符串</param>
    /// <param name="url">地址</param>
    /// <param name="headers">请求头</param>
    /// <returns></returns>
    Task<(T, Exception)> SendAsync<T>(object obj, string url = "", IEnumerable<KeyValuePair<string, string>> headers = null);
}