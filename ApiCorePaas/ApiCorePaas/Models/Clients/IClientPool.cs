﻿namespace ApiCorePaas.Models.Clients;

/// <summary>
///     客户端连接池
/// </summary>
public interface IClientPool
{
    /// <summary>
    ///     初始化池
    /// </summary>
    /// <param name="address"></param>
    /// <param name="httpMessageHandler"></param>
    /// <returns></returns>
    bool InitPool(string address, HttpMessageHandler httpMessageHandler = null);

    /// <summary>
    ///     获取一个客户端
    /// </summary>
    /// <returns></returns>
    IClient Get();

    /// <summary>
    ///     将客户端放入池中复用
    /// </summary>
    /// <param name="client">客户端对象</param>
    /// <returns></returns>
    bool Put(IClient client);
}