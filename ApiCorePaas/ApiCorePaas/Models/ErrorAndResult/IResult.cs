﻿namespace ApiCorePaas.Models.ErrorAndResult
{
    /// <summary>
    /// 
    /// </summary>
    public interface IResult
    {
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IResult<out T> : IResult
    {
    }
}
