﻿using MessagePack;
using Newtonsoft.Json;

namespace ApiCorePaas.Models.ErrorAndResult
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [MessagePackObject]
    public class ResultTemplate<T> : IResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        [JsonProperty("ok")]
        [Key("ok")]
        public bool Ok { get; set; }

        /// <summary>  
        ///     错误信息
        /// </summary>
        [JsonProperty("err", NullValueHandling = NullValueHandling.Ignore)]
        [Key("err")]
        public ErrorInfo Error { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [JsonProperty("des", NullValueHandling = NullValueHandling.Ignore)]
        [Key("des")]
        public string Describe { get; set; }

        /// <summary>
        /// 响应报文体
        /// </summary>  
        [JsonProperty("data")]
        [Key("data")]
        public T Data { get; set; }
    }
}