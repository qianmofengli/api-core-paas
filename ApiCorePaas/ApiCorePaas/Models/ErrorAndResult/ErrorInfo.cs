﻿using ApiCorePaas.Models.Enums;
using MessagePack;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiCorePaas.Models.ErrorAndResult
{
    /// <summary>
    /// 
    /// </summary>
    [MessagePackObject]
    public class ErrorInfo : IActionResult
    {
        /// <summary>
        ///     新建异常
        /// </summary>
        /// <param name="errorCode">错误枚举</param>
        /// <param name="errorMessage">错误详细信息</param>
        /// <param name="fields"></param>
        public ErrorInfo(ErrorEnum errorCode, string errorMessage, List<ValidationFieldErrorModel> fields = null)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            Fields = fields;
        }

        /// <summary>
        /// 错误编号
        /// </summary>
        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        [Key("code")]
        public ErrorEnum ErrorCode { get; set; }

        /// <summary>
        /// 错误详细信息
        /// </summary>
        [JsonProperty("msg", NullValueHandling = NullValueHandling.Ignore)]
        [Key("msg")]
        public string ErrorMessage { get; set; }


        // /// <summary>
        // /// 错误字段，用于模型校验
        // /// </summary>
        // [JsonProperty("field_name", NullValueHandling = NullValueHandling.Ignore)]
        // public string FieldName { get; set; }

        /// <summary>
        /// 错误字段，用于模型校验   
        /// </summary>
        [JsonProperty("fields", NullValueHandling = NullValueHandling.Ignore)]
        public List<ValidationFieldErrorModel> Fields { get; set; }

        /// <summary>
        ///     隐式转换成bool类型
        /// </summary>
        /// <param name="error"></param>
        public static implicit operator bool(ErrorInfo error)
        {
            return error != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task ExecuteResultAsync(ActionContext context)
        {
            var objectResult = new ObjectResult(this);
            return objectResult.ExecuteResultAsync(context);
        }
    }
}