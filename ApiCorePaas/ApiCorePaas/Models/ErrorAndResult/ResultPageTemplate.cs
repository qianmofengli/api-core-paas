﻿using MessagePack;
using Newtonsoft.Json;

namespace ApiCorePaas.Models.ErrorAndResult
{
    /// <summary>
    ///  分页返回Json  
    /// </summary>
    [MessagePackObject]
    public class ResultPageTemplate<T> : IResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        [JsonProperty("ok")]
        [Key("ok")]
        public bool Ok { get; set; }

        /// <summary>
        ///     错误信息
        /// </summary>
        [JsonProperty("err", NullValueHandling = NullValueHandling.Ignore)]
        [Key("err")]
        public ErrorInfo Error { get; set; }

        /// <summary>
        ///     分页索引
        /// </summary>
        [JsonProperty("page_index", NullValueHandling = NullValueHandling.Ignore)]
        [Key("page_index")]
        public int? PageIndex { get; set; }

        /// <summary>
        ///     每页显示记录数
        /// </summary>
        [JsonProperty("page_size", NullValueHandling = NullValueHandling.Ignore)]
        [Key("page_size")]
        public int? PageSize { get; set; }

        /// <summary>
        ///     分页总和
        /// </summary>
        [JsonProperty("page_total", NullValueHandling = NullValueHandling.Ignore)]
        [Key("page_total")]
        public long? PageTotal { get; set; }

        /// <summary>
        /// 响应报文体
        /// </summary>    
        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
        [Key("data")]
        public List<T> Data { get; set; }
    }
}
