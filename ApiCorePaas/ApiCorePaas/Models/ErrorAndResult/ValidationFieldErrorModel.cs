﻿namespace ApiCorePaas.Models.ErrorAndResult;

/// <summary>
/// 自定义错误响应格式
/// </summary>
public class ValidationFieldErrorModel
{
    /// <summary>
    /// 字段名称
    /// </summary>
    public string Field { get; set; }

    /// <summary>
    /// 提示消息
    /// </summary>
    public string Message { get; set; }
}