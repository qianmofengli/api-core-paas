﻿using ApiCorePaas.Controllers;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Models.ErrorAndResult;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ApiCorePaas.Models.Filters;

/// <summary>
/// 
/// </summary>
public class ModelValidationFilter : IActionFilter
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.ModelState.IsValid)
        {
            return;
        }

        var errors = context.ModelState 
            .Where(ms => ms.Value.Errors.Count > 0)
            .SelectMany(ms => ms.Value.Errors.Select(e => new ValidationFieldErrorModel
            {
                Field = ms.Key,
                Message = e.ErrorMessage
            }))
            .ToList();

        // 使用自定义的错误响应格式
        var errorInfo = new ErrorInfo(ErrorEnum.BadRequest, "数据验证失败", errors);

        // 使用返回模板生成响应
        var response = BaseController.Result(errors, errorInfo);

        context.Result = new BadRequestObjectResult(response);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public void OnActionExecuted(ActionExecutedContext context)
    {
        // 动作执行后的逻辑（如果需要）
    }
}