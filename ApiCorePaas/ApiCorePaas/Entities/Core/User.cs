﻿using ApiCorePaas.Models.Enums;
using SqlSugar;

namespace ApiCorePaas.Entities.Core;

/// <summary>
/// 用户表
/// </summary>
[SugarTable("b_user")]
[Tenant("core")]
public class User
{
    /// <summary> 
    ///  
    /// </summary>
    [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "ID")]
    public int? UserId { get; set; }

    /// <summary> 
    /// 用户编号 
    /// </summary> 
    public string UserNo { get; set; }

    /// <summary> 
    /// 姓名 
    /// </summary>
    [SugarColumn(ColumnName = "Uname")]
    public string UserName { get; set; }

    /// <summary> 
    /// 登录名 
    /// </summary> 
    public string LoginName { get; set; }

    /// <summary> 
    /// 密码 
    /// </summary>
    [SugarColumn(ColumnName = "Pws")]
    public string Password { get; set; }

    /// <summary> 
    /// 删除标记 
    /// </summary> 
    public bool IsDel { get; set; }

    /// <summary> 
    /// 企业名 
    /// </summary> 
    public string EntName { get; set; }

    /// <summary> 
    /// 手机号码 
    /// </summary> 
    public string Phone { get; set; }

    /// <summary> 
    /// 邮箱 
    /// </summary> 
    public string EmailAcc { get; set; }

    /// <summary> 
    /// 身份证 
    /// </summary>
    [SugarColumn(ColumnName = "IDCard")]
    public string CardId { get; set; }

    /// <summary> 
    /// [枚举值]性别 [1 男 2 女 3 未知] 
    /// </summary> 
    public UserSexEnum Gender { get; set; }

    /// <summary> 
    /// 令牌 
    /// </summary>
    [SugarColumn(ColumnName = "access_token")]
    public string AccessToken { get; set; }

    /// <summary> 
    /// [枚举值]数据权限：[1 同级/下级 2 同级 3 下级 4 全部 5 自己 6 所在同级/下级] 
    /// </summary> 
    public int DataPower { get; set; }

    /// <summary> 
    /// 是否单点登录 
    /// </summary> 
    public bool IsOneLogin { get; set; }

    /// <summary> 
    /// 是否开发人员 
    /// </summary> 
    public bool IsProgrammer { get; set; }

    /// <summary> 
    /// [枚举值]是否审核：[ 1 待审核 2 审核通过 3 审核不通过 ] 
    /// </summary> 
    public int IsCheck { get; set; }

    /// <summary> 
    /// 是否启用 0 未启用 1 已启用 
    /// </summary> 
    public bool IsEnable { get; set; }
}