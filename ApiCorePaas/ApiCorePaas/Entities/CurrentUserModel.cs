﻿namespace ApiCorePaas.Entities;

/// <summary>
/// 当前登录用户
/// </summary>
public class CurrentUserModel
{
    /// <summary>
    /// 用户ID
    /// </summary>
    public int? UserId { get; set; }

    /// <summary>
    /// 手机号
    /// </summary>
    public string Phone { get; set; }

    /// <summary>
    /// Ip地址
    /// </summary>
    public string IpAddress { get; set; }

    /// <summary>
    /// 令牌
    /// </summary>
    public string Token { get; set; }

    /// <summary>
    /// 刷新令牌
    /// </summary>
    public string RefreshToken { get; set; }

    /// <summary> 
    /// 所属系统ID 
    /// </summary> 
    public int SystemId { get; set; }

    /// <summary> 
    /// 所属企业ID 
    /// </summary> 
    public int EnterpriseId { get; set; }
}