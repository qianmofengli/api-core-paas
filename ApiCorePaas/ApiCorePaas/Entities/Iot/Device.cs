﻿using ApiCorePaas.Models.Enums;
using SqlSugar;

namespace ApiCorePaas.Entities.Iot;

/// <summary>
/// 设备连接信息
/// </summary>
[SugarTable("b_device")]
[Tenant("iot")]
public class Device : BaseEntity, IIsolate
{
    /// <summary> 
    ///  
    /// </summary> 
    [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "ID")]
    public int DeviceId { get; set; }


    /// <summary> 
    /// 产品ID 
    /// </summary>
    [SugarColumn(ColumnName = "ID_Product")]
    public int ProductId { get; set; }

    /// <summary> 
    /// 设备号 全局唯一 设备连接返回 
    /// </summary> 
    public Int64 DeviceNo { get; set; }

    /// <summary> 
    /// 设备编号 
    /// </summary> 
    public string DeviceNum { get; set; }

    /// <summary> 
    /// 设备名称 
    /// </summary> 
    public string DeviceCustomName { get; set; }

    /// <summary> 
    /// SN号 
    /// </summary> 
    public string SerialNumber { get; set; }

    /// <summary> 
    /// 国际移动设备识别码 移动设备本身关联的唯一标识符 
    /// </summary> 
    public string Imei { get; set; }

    /// <summary> 
    /// 运行时间 秒 
    /// </summary> 
    public string Uptime { get; set; }

    /// <summary> 
    /// mac地址 
    /// </summary> 
    public string Mac { get; set; }

    /// <summary> 
    /// 固件版本 设备连接返回 
    /// </summary> 
    public string FirmwareVersion { get; set; }

    /// <summary> 
    /// 经度 设备连接返回 
    /// </summary> 
    public string Longitude { get; set; }

    /// <summary> 
    /// 纬度 设备连接返回 
    /// </summary> 
    public string Latitude { get; set; }

    /// <summary> 
    /// 海平面高度 
    /// </summary> 
    public string SeaLevelAltitude { get; set; }

    /// <summary> 
    /// 定位质量 
    /// </summary> 
    public string LocationQuality { get; set; }

    /// <summary> 
    /// 卫星颗数 
    /// </summary> 
    public string SatelliteCount { get; set; }

    /// <summary> 
    /// 设备IP 
    /// </summary> 
    public string ClientIp { get; set; }

    /// <summary> 
    /// 设备端口 
    /// </summary> 
    public string ClientPort { get; set; }

    /// <summary> 
    /// [枚举值]设备状态：[1 在线 2 离线 3 未激活] 
    /// </summary> 
    public DeviceStateEnum DeviceState { get; set; }

    /// <summary> 
    /// 唯一标识无线局域网 设备连接返回 
    /// </summary> 
    public string SsId { get; set; }

    /// <summary> 
    /// 是否禁用 
    /// </summary> 
    public bool IsDisable { get; set; }

    /// <summary> 
    /// 是否删除 
    /// </summary> 
    public bool IsDelete { get; set; }

    /// <summary> 
    /// 是否已投放 
    /// </summary> 
    public bool IsInput { get; set; }

    /// <summary> 
    /// 是否已入库 
    /// </summary> 
    public bool IsPutStorage { get; set; }

    /// <summary> 
    /// 创建时间 
    /// </summary> 
    public DateTime CreateDate { get; set; }

    /// <summary> 
    /// 创建用户ID 
    /// </summary> 
    public int CreateUserId { get; set; }

    /// <summary> 
    /// 创建用户名称 
    /// </summary> 
    public string CreateUserName { get; set; }

    /// <summary> 
    /// 更新时间 
    /// </summary> 
    public DateTime UpdateDate { get; set; }

    /// <summary> 
    /// 更新用户Id 
    /// </summary> 
    public int UpdateUserId { get; set; }

    /// <summary> 
    /// 更新用户名称 
    /// </summary> 
    public string UpdateUserName { get; set; }

    /// <summary> 
    /// 备注 
    /// </summary> 
    public string Remark { get; set; }
}