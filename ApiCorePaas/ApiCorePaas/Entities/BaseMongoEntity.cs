﻿namespace ApiCorePaas.Entities;

/// <summary>
/// mongoDB实体基类
/// </summary>
public class BaseMongoEntity
{
    /// <summary>
    /// 企业ID
    /// </summary>
    public int? EnterpriseId { get; set; }
}