﻿using SqlSugar;

namespace ApiCorePaas.Entities;

/// <summary>
/// 基础实体类
/// </summary>
public class BaseEntity
{
    /// <summary> 
    /// 所属系统ID 
    /// </summary> 
    [SugarColumn(ColumnName = "ID_Sys")]
    public int SystemId { get; set; }

    /// <summary> 
    /// 所属用户ID 
    /// </summary> 
    [SugarColumn(ColumnName = "ID_User")]
    public int UserId { get; set; }

    /// <summary> 
    /// 所属企业ID 
    /// </summary> 
    [SugarColumn(ColumnName = "ID_ENT")]
    public int EnterpriseId { get; set; }
}