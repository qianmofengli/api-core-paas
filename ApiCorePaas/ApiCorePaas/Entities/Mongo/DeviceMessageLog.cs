﻿using ApiCorePaas.Models.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using QMFL.MongoMultiDbRepository;

namespace ApiCorePaas.Entities.Mongo;

/// <summary>
/// 设备消息
/// </summary>
[Database("IotLog")]
[CollectionName("yyyyMMdd")]
public class DeviceMessageLog : BaseMongoEntity, IEntity
{
    /// <summary>
    /// 
    /// </summary>
    [BsonId]
    public ObjectId Id { get; set; }

    /// <summary>
    /// 设备号
    /// </summary>
    public long DeviceNo { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 明文消息内容
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// 加密的消息内容
    /// </summary>
    public string EncryptedMessage { get; set; }

    /// <summary>
    /// 接收主题
    /// </summary>
    public string Topic { get; set; }

    /// <summary>
    /// 是否已处理
    /// </summary>
    public bool IsHandle { get; set; } = false;

    /// <summary>
    /// 消息ID 阿里云mqtt会返回
    /// </summary>
    public string MsgId { get; set; }

    /// <summary>
    /// 处理时间
    /// </summary>
    [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
    public DateTime? HandleTime { get; set; }

    /// <summary>
    /// 产品ID
    /// </summary>
    public int ProductId { get; set; }

    /// <summary>
    /// 业务类型 1 设备 2 DMC服务
    /// </summary>
    public MessageSourceEnum MessageSource { get; set; }
}