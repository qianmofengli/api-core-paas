﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using QMFL.MongoMultiDbRepository;

namespace ApiCorePaas.Entities.Mongo;

/// <summary>
/// 操作日志
/// </summary>
[Database("OperationLog")]
[CollectionName("yyyyMMdd")]
public class OperationLog : IEntity
{
    /// <summary>
    /// 
    /// </summary>
    [BsonId]
    public ObjectId Id { get; set; }

    /// <summary>
    /// 操作时间
    /// </summary>
    public DateTime MakeDate { get; set; }

    /// <summary>
    /// 企业名称
    /// </summary>
    public string EntName { get; set; }

    /// <summary>
    /// 系统名称
    /// </summary>
    public string SysName { get; set; }

    /// <summary>
    /// 操作人ID
    /// </summary>
    public string ID_User { get; set; }

    /// <summary>
    /// 操作人名称
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// 模块名称
    /// </summary>
    public string ModelName { get; set; }

    /// <summary>
    /// 操作类型
    /// </summary>
    public string OperationType { get; set; }

    /// <summary>
    /// 操作内容
    /// </summary>
    public string OperationContent { get; set; }

    /// <summary>
    /// 操作结果
    /// </summary>
    public string ScriptContent { get; set; }
}