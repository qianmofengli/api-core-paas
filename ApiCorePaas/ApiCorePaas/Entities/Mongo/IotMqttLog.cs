﻿using ApiCorePaas.Models.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using QMFL.MongoMultiDbRepository;

namespace ApiCorePaas.Entities.Mongo;

/// <summary>
/// 物联设备日志
/// </summary>
[Database("IotLog")]
[CollectionName("IotMqttLog")]
public class IotMqttLog : BaseMongoEntity, IEntity
{
    /// <summary>
    /// Id
    /// </summary>
    [BsonId]
    public ObjectId Id { get; set; }

    /// <summary>
    /// 消息ID
    /// </summary>
    public string MessageId { get; set; }

    /// <summary>
    /// 日志时间
    /// </summary>
    // [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
    public DateTime LogTime { get; set; }

    /// <summary>
    /// 账号类型
    /// </summary>
    public MqttAccountTypeEnum? MqttAccountType { get; set; }

    /// <summary>
    /// 设备号
    /// </summary>
    public long? DeviceNo { get; set; }

    /// <summary>
    /// 客户端Id
    /// </summary>
    public string ClientId { get; set; }

    /// <summary>
    /// 日志级别
    /// </summary>
    public LogLevelEnum LogLevel { get; set; }

    /// <summary>
    /// 事件类型
    /// </summary>
    public DeviceEventEnum EventEnum { get; set; }

    /// <summary>
    /// Topic
    /// </summary>
    public string Topic { get; set; }

    /// <summary>
    /// cleanSession标志是MQTT协议中对一个消费者客户端建立TCP连接后是否关心之前状态的定义，与消息发送端的设置无关。具体语义如下：
    /// cleanSession=true：消费者客户端再次上线时，将不再关心之前所有的订阅关系以及离线消息。
    /// cleanSession=false：消费者客户端再次上线时，还需要处理之前的离线消息，而之前的订阅关系也会持续生效。
    ///         cleanSession=true               cleanSession=false
    /// QoS0	无离线消息，在线消息只尝试推一次。	无离线消息，在线消息只尝试推一次。
    /// QoS1	无离线消息，在线消息保证可达。	有离线消息，所有消息保证可达。
    /// QoS2	无离线消息，在线消息保证只推一次。	暂不支持。
    /// </summary>
    public MqttQualityOfServiceLevelEnum? QoS { get; set; }

    /// <summary>
    /// 日志内容
    /// </summary>
    public string LogContent { get; set; }

    /// <summary>
    /// 客户端ip
    /// </summary>
    public string ClientIp { get; set; }

    /// <summary>
    /// 客户端端口号
    /// </summary>
    public int ClientPort { get; set; }
}