using ApiCorePaas.Models.Extensions;
using ApiCorePaas.Models.Middlewares;
using ApiCorePaas.Options;
using Serilog;

namespace ApiCorePaas
{
    /// <summary>
    /// 
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            IConfigurationRoot configuration = builder.Configuration;
            AppSettings.MySqlConnectionStringOption =
                configuration.GetSection("SqlConnectionStrings").Get<MySqlConnectionStringOption>();
            AppSettings.MongoConnectionStringOption = configuration.GetSection("MongoConnectionStrings")
                .Get<MongoConnectionStringOption>();
            AppSettings.RedisOption = configuration.GetSection("RedisConnectionStrings")
                .Get<RedisOption>();
            AppSettings.JwtSettingOption = configuration.GetSection("JwtSettings")
                .Get<JwtSettingOption>();
            AppSettings.ChatGptOption = configuration.GetSection("ChatGptSettings")
                .Get<ChatGptOption>();
            AppSettings.WeChatOption = configuration.GetSection("WeChatSettings")
                .Get<WeChatOption>();
            AppSettings.ErnieBotOption = configuration.GetSection("ErnieBotSettings")
                .Get<ErnieBotOption>();

            // 使用 Serilog
            builder.Host.UseSerilog((ctx, lc) => lc
                .WriteTo.Console()
                .ReadFrom.Configuration(ctx.Configuration));

            builder.Services.AddTransientHandlers()
                .AddCustomDbContext()
                .AddCustomService()
                .AddQuartzConfig()
                .AddSwagger();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            // if (app.Environment.IsDevelopment())
            // {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/web/swagger.json", "平台");
                c.SwaggerEndpoint("/swagger/applet/swagger.json", "小程序");
                c.SwaggerEndpoint("/swagger/official-account/swagger.json", "公众号");
                //设置为-1 可不显示models
                c.DefaultModelsExpandDepth(-1);
                //显示运行时间
                c.DisplayRequestDuration();
            });
            // }

            app.UseCors("any");
            //追踪中间件
            app.UseMiddleware<TraceMiddleware>();
            // 审计中间件
            app.UseMiddleware<RequestAuditMiddleware>();
            // 全局异常处理中间件
            app.UseMiddleware<GlobalExceptionMiddleware>();

            // app.UseHttpsRedirection();
            app.MapControllers();
            app.Run();
        }
    }
}