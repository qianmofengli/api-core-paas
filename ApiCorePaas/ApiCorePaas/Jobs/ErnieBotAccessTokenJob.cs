﻿using ApiCorePaas.DataModels.ErnieBot;
using ApiCorePaas.Models.CacheServices;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;
using Quartz;

namespace ApiCorePaas.Jobs;

/// <summary>
/// 文心一言获取accessToken
/// </summary>
[DisallowConcurrentExecution]
public class ErnieBotAccessTokenJob : IJob
{
    private readonly ILogger<ErnieBotAccessTokenJob> _logger;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    public ErnieBotAccessTokenJob(ILogger<ErnieBotAccessTokenJob> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public async Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("文心一言AccessToken调度任务开始执行");
        try
        {
            var url =
                $"oauth/2.0/token?grant_type=client_credentials&client_id={AppSettings.ErnieBotOption.ApiKey}&client_secret={AppSettings.ErnieBotOption.SecretKey}";
            var (response, error) =
                await ClientPool.ErnieBotClient.PostAsync<BaiDuAccessTokenResponseModel>(string.Empty, url);
            if (error != null)
            {
                throw error;
            }

            if (!string.IsNullOrEmpty(response.Error))
            {
                throw new Exception(response.ErrorDescription);
            }

            var success = await CachePool.ErnieBotRedisCache.Set(
                $"{RedisKeyEnum.ErnieBotAccessToken.GetDescription()}_{AppSettings.ErnieBotOption.AppId}",
                response.AccessToken, TimeSpan.FromSeconds(response.ExpiresIn));
            if (!success)
            {
                throw new Exception("缓存公众号accessToken失败");
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "文心一言AccessToken调度任务执行失败");
        }
    }
}