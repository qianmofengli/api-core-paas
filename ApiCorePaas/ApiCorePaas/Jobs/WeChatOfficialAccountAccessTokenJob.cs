﻿using ApiCorePaas.DataModels.WeChat;
using ApiCorePaas.Models.CacheServices;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;
using Quartz;

namespace ApiCorePaas.Jobs;

/// <summary>
/// 微信公众号AccessToken
/// </summary>
[DisallowConcurrentExecution] //同一作业实例不会并发执行
public class WeChatOfficialAccountAccessTokenJob : IJob
{
    private readonly ILogger<WeChatOfficialAccountAccessTokenJob> _logger;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    public WeChatOfficialAccountAccessTokenJob(ILogger<WeChatOfficialAccountAccessTokenJob> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public async Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("微信公众号AccessToken调度任务开始执行");
        try
        {
            var appId = AppSettings.WeChatOption.OfficialAccountAppId;
            var url =
                $"cgi-bin/token?grant_type=client_credential&appid={appId}&secret={AppSettings.WeChatOption.OfficialAccountAppSecret}";
            var (response, error) =
                await ClientPool.WeChatClient.GetAsync<AccessTokenResponseModel>(url);
            if (error != null)
            {
                throw error;
            }

            if (response.ErrCode != 0)
            {
                throw new Exception(response.ErrMsg);
            }

            var token = response.AccessToken;
            if (string.IsNullOrEmpty(token))
            {
                throw new Exception("获取公众号accessToken失败,内容为空");
            }

            //放到缓存库
            var success = await CachePool.OfficialAccountRedisCache.Set(
                $"{RedisKeyEnum.OfficialAccountAccessToken.GetDescription()}_{appId}",
                token, TimeSpan.FromSeconds(response.ExpiresIn));
            if (!success)
            {
                throw new Exception("缓存公众号accessToken失败");
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "微信公众号AccessToken调度任务执行失败");
        }
    }
}