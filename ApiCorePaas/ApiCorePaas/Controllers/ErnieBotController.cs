﻿using ApiCorePaas.DataModels.ErnieBot;
using ApiCorePaas.Handlers;
using ApiCorePaas.Models.ErrorAndResult;
using Microsoft.AspNetCore.Mvc;

namespace ApiCorePaas.Controllers;

/// <summary>
/// ErnieBot 聊天机器人
/// </summary>
[ApiController]
[Route("api/ernie-bot")]
[ApiExplorerSettings(GroupName = "web")]
public class ErnieBotController : BaseController
{
    private readonly ErnieBotHandle _ernieBotHandle;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ernieBotHandle"></param>
    public ErnieBotController(ErnieBotHandle ernieBotHandle)
    {
        _ernieBotHandle = ernieBotHandle;
    }

    /// <summary>
    /// 聊天
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("chat")]
    public async Task<ResultTemplate<string>> ErnieBotTurboChat([FromBody] ErnieBotTurboChatModel model)
    {
        var (result, error) = await _ernieBotHandle.ErnieBotTurboChat(model);
        return Result(result, error);
    }

    /// <summary>
    /// 图片
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("image")]
    public async Task<ResultListTemplate<ImageData>> StableDiffusionXlImage([FromBody] ErnieBotTurboChatModel model)
    {
        var (result, error) = await _ernieBotHandle.StableDiffusionXlImage(model);
        return Result(result, error);
    }
}