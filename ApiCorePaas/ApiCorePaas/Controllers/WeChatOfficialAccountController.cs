﻿using ApiCorePaas.DataModels.WeChat;
using ApiCorePaas.Handlers;
using ApiCorePaas.Models.ErrorAndResult;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace ApiCorePaas.Controllers;

/// <summary>
/// 微信公众号
/// </summary>
[ApiController]
[Route("api/wechat")]
[ApiExplorerSettings(GroupName = "web")]
public class WeChatOfficialAccountController : BaseController
{
    private readonly WeChatOfficialAccountHandler _weChatOfficialAccountHandler;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="weChatOfficialAccountHandler"></param>
    public WeChatOfficialAccountController(WeChatOfficialAccountHandler weChatOfficialAccountHandler)
    {
        _weChatOfficialAccountHandler = weChatOfficialAccountHandler;
    }

    /// <summary>
    /// 验证消息的确来自微信服务器
    /// </summary>
    /// <returns></returns>
    [HttpGet("auth-event")]
    public async Task<IActionResult> AuthEvent([FromQuery] AuthEventModel model)
    {
        var result = await _weChatOfficialAccountHandler.VerifyMessage(model);
        HttpContext.Response.ContentType = "text/plain";
        return Ok(result);
    }

    /// <summary>
    /// 接收微信消息
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("auth-event")]
    public async Task<IActionResult> AuthEvent([FromQuery] AuthMessageEventModel model)
    {
        var bodyContent = await new StreamReader(Request.Body).ReadToEndAsync();
        var result = await _weChatOfficialAccountHandler.MessageEvent(model, bodyContent);
        HttpContext.Response.ContentType = "text/plain";
        return Ok(result);
    }

    /// <summary>
    /// 上传临时素材
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("upload-temporary-material")]
    public async Task<ResultTemplate<string>> UploadTemporaryMaterial([FromBody] UploadTemporaryMaterialModel model)
    {
        var (value, error) =
            await _weChatOfficialAccountHandler.UploadTemporaryMaterial(model.Base64);
        return Result(value, error);
    }

    /// <summary>
    /// 获取关注列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("get-sub-user")]
    public async Task<ResultTemplate<bool>> GetSubscribeUser()
    {
        var (result, error) = await _weChatOfficialAccountHandler.GetSubscribeUser();
        return Result(result, error);
    }
}