﻿using ApiCorePaas.Models.ErrorAndResult;
using Microsoft.AspNetCore.Mvc;

namespace ApiCorePaas.Controllers;

/// <summary>
/// 基础控制器
/// </summary>
[ApiController]
[Route("api/[controller]/[Action]")]
// [Consumes("application/json", "application/x-www-form-urlencoded", "multipart/form-data", "application/xml")]
public class BaseController : ControllerBase
{
    /// <summary>
    ///     返回模板
    /// </summary>
    /// <typeparam name="T">模板类型</typeparam>
    /// <param name="data">数据</param>
    /// <param name="error">错误</param>
    /// <returns></returns>
    public static ResultListTemplate<T> Result<T>(List<T> data, ErrorInfo error)
    {
        var result = new ResultListTemplate<T>();
        if (error != null)
        {
            result.Ok = false;
            result.Error = error;
        }
        else
        {
            result.Ok = true;
            result.Data = data;
        }

        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <param name="error"></param>
    /// <param name="pageCount"></param>
    /// <param name="pageIndex"></param>
    /// <param name="pageTotal"></param>
    /// <returns></returns>
    public static ResultPageTemplate<T> Result<T>(List<T> data, ErrorInfo error, int? pageCount, int? pageIndex,
        long? pageTotal)
    {
        var result = new ResultPageTemplate<T>();
        if (error != null)
        {
            result.Ok = false;
            result.Error = error;
        }
        else
        {
            result.Ok = true;
            result.Data = data;
            result.PageSize = pageCount;
            result.PageIndex = pageIndex;
            result.PageTotal = pageTotal;
        }

        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="error"></param>
    /// <param name="pageCount"></param>
    /// <param name="pageIndex"></param>
    /// <param name="pageTotal"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected static ResultPageOneTemplate<T> Result<T>(T data, ErrorInfo error, int? pageCount, int? pageIndex,
        long? pageTotal)
    {
        var result = new ResultPageOneTemplate<T>();
        if (error != null)
        {
            result.Ok = false;
            result.Error = error;
        }
        else
        {
            result.Ok = true;
            result.Data = data;
            result.PageCount = pageCount;
            result.PageIndex = pageIndex;
            result.PageTotal = pageTotal;
        }

        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <param name="error"></param>
    /// <returns></returns>
    protected static ResultTemplate<T> Result<T>(T data, ErrorInfo error)
    {
        var result = new ResultTemplate<T>();
        if (error != null)
        {
            result.Ok = false;
            result.Error = error;
        }
        else
        {
            result.Ok = true;
            result.Data = data;
        }

        return result;
    }
}