﻿using ApiCorePaas.DataModels;
using ApiCorePaas.Handlers;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.ErrorAndResult;
using ApiCorePaas.Options;
using Microsoft.AspNetCore.Mvc;

namespace ApiCorePaas.Controllers;

/// <summary>
/// 
/// </summary>
[ApiController]
[Route("api/test")]
[ApiExplorerSettings(GroupName = "web")]
public class TestController : BaseController
{
    private readonly TestHandle _testHandle;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="testHandle"></param>
    public TestController(TestHandle testHandle)
    {
        _testHandle = testHandle;
    }

    /// <summary>
    /// 测试
    /// </summary>
    /// <returns></returns>
    [HttpGet("get")]
    public async Task<ResultTemplate<bool>> Test([FromQuery] TestGetModel postModel)
    {
        // var (value, error) = await _testHandle.Test();
        return Result(false, null);
    }


    /// <summary>
    /// post请求测试
    /// </summary>
    /// <param name="postModel"></param>
    /// <returns></returns>
    // [LoginAuthorize]
    [HttpPost("post")]
    public async Task<ResultTemplate<bool>> Test(TestPostModel postModel)
    {
        var (value, error) = await _testHandle.Test();
        return Result(value, error);
    }

    /// <summary>
    /// token
    /// </summary>
    /// <returns></returns>
    [HttpPost("token")]
    public async Task<ResultTemplate<string>> TestToken()
    {
        var (value, error) = await _testHandle.TestToken();
        return Result(value, error);
    }

    /// <summary>
    /// 
    /// </summary>
    [HttpGet("stream")]
    public async Task ChatGpt()
    {
        var response = Response;
        response.Headers.Append("Content-Type", "text/event-stream");
        var headers = new List<KeyValuePair<string, string>>
        {
            new("Authorization", AppSettings.ChatGptOption.SecretKey),
            new("Content-Type", "application/json"),
            new("Accept", "application/json")
        };

        // 连接到 SSE 服务
        await ClientPool.ChatGptClient.ConnectToSseAsync("", "chat/completions", headers);

        // 从 Channel 读取数据并发送到前端
        await foreach (var data in ClientPool.ChatGptClient.SseChannel.Reader.ReadAllAsync())
        {
            await response.WriteAsync($"data: {data}\n\n");
            await response.Body.FlushAsync();
        }
    }
}