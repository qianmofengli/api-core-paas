﻿using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace ApiCorePaas.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class Cryptography
    {
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="input">密文</param>
        /// <param name="encodingAesKey"></param>
        /// <param name="appid"></param>
        /// <returns></returns>
        /// 
        public static string AesDecrypt(string input, string encodingAesKey, ref string appid)
        {
            if (appid == null) throw new ArgumentNullException(nameof(appid));

            var key = Convert.FromBase64String(encodingAesKey + "=");
            var iv = new byte[16];
            Array.Copy(key, iv, 16);
            var aesDecrypt = AesDecrypt(input, iv, key);

            var len = BitConverter.ToInt32(aesDecrypt, 16);
            len = IPAddress.NetworkToHostOrder(len);

            var msg = new byte[len];
            var msgAppid = new byte[aesDecrypt.Length - 20 - len];
            Array.Copy(aesDecrypt, 20, msg, 0, len);
            Array.Copy(aesDecrypt, 20 + len, msgAppid, 0, aesDecrypt.Length - 20 - len);
            var encodeMag = Encoding.UTF8.GetString(msg);
            appid = Encoding.UTF8.GetString(msgAppid);

            return encodeMag;
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="input"></param>
        /// <param name="encodingAesKey"></param>
        /// <param name="appid"></param>
        /// <returns></returns>
        public static string AesEncrypt(string input, string encodingAesKey, string appid)
        {
            var key = Convert.FromBase64String(encodingAesKey + "=");
            var iv = new byte[16];
            Array.Copy(key, iv, 16);
            var randCode = CreateRandCode(16);
            var bRand = Encoding.UTF8.GetBytes(randCode);
            var bAppid = Encoding.UTF8.GetBytes(appid);
            var bitmapMsg = Encoding.UTF8.GetBytes(input);
            var bMsgLen = BitConverter.GetBytes(HostToNetworkOrder(bitmapMsg.Length));
            var bMsg = new byte[bRand.Length + bMsgLen.Length + bAppid.Length + bitmapMsg.Length];

            Array.Copy(bRand, bMsg, bRand.Length);
            Array.Copy(bMsgLen, 0, bMsg, bRand.Length, bMsgLen.Length);
            Array.Copy(bitmapMsg, 0, bMsg, bRand.Length + bMsgLen.Length, bitmapMsg.Length);
            Array.Copy(bAppid, 0, bMsg, bRand.Length + bMsgLen.Length + bitmapMsg.Length, bAppid.Length);

            return AesEncrypt(bMsg, iv, key);
        }

        private static string AesEncrypt(byte[] input, byte[] iv, byte[] key)
        {
            using var aesAlg = Aes.Create();
            aesAlg.KeySize = 256;
            aesAlg.BlockSize = 128;
            aesAlg.Padding = PaddingMode.None;
            aesAlg.Mode = CipherMode.CBC;
            aesAlg.Key = key;
            aesAlg.IV = iv;

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            byte[] xBuff;

            #region 自己进行PKCS7补位，用系统自带的不行

            var msg = new byte[input.Length + 32 - input.Length % 32];
            Array.Copy(input, msg, input.Length);
            var pad = Kcs7Encoder(input.Length);
            Array.Copy(pad, 0, msg, input.Length, pad.Length);

            #endregion

            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(msg, 0, msg.Length);
                }

                xBuff = ms.ToArray();
            }

            var output = Convert.ToBase64String(xBuff);
            return output;
        }

        private static byte[] AesDecrypt(string input, byte[] iv, byte[] key)
        {
            using var aesAlg = Aes.Create();
            aesAlg.KeySize = 256;
            aesAlg.BlockSize = 128;
            aesAlg.Mode = CipherMode.CBC;
            aesAlg.Padding = PaddingMode.None;
            aesAlg.Key = key;
            aesAlg.IV = iv;

            var decrypt = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            using var ms = new MemoryStream();
            using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
            {
                var xXml = Convert.FromBase64String(input);
                var msg = new byte[xXml.Length + 32 - xXml.Length % 32];
                Array.Copy(xXml, msg, xXml.Length);
                cs.Write(xXml, 0, xXml.Length);
            }

            var xBuff = Decode2(ms.ToArray());
            return xBuff;
        }

        private static int HostToNetworkOrder(int inValue)
        {
            var outValue = 0;
            for (var i = 0; i < 4; i++)
                outValue = (outValue << 8) + ((inValue >> (i * 8)) & 255);
            return outValue;
        }

        private static string CreateRandCode(int codeLen)
        {
            const string codeSerial =
                "2,3,4,5,6,7,a,c,d,e,f,h,i,j,k,m,n,p,r,s,t,A,C,D,E,F,G,H,J,K,M,N,P,Q,R,S,U,V,W,X,Y,Z";
            if (codeLen == 0)
            {
                codeLen = 16;
            }

            var arr = codeSerial.Split(',');
            var code = "";
            var rand = new Random(unchecked((int)DateTime.Now.Ticks));
            for (var i = 0; i < codeLen; i++)
            {
                var randValue = rand.Next(0, arr.Length - 1);
                code += arr[randValue];
            }

            return code;
        }

        /// <summary>
        /// 将数字转化成ASCII码对应的字符，用于对明文进行补码
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static char Chr(int number)
        {
            var target = (byte)(number & 0xFF);
            return (char)target;
        }

        private static byte[] Kcs7Encoder(int textLength)
        {
            const int blockSize = 32;
            // 计算需要填充的位数
            var amountToPad = blockSize - (textLength % blockSize);
            if (amountToPad == 0)
            {
                amountToPad = blockSize;
            }

            // 获得补位所用的字符
            var padChr = Chr(amountToPad);
            var tmp = "";
            for (var index = 0; index < amountToPad; index++)
            {
                tmp += padChr;
            }

            return Encoding.UTF8.GetBytes(tmp);
        }

        private static byte[] Decode2(byte[] decrypted)
        {
            int pad = decrypted[^1];
            if (pad < 1 || pad > 32)
            {
                pad = 0;
            }

            var res = new byte[decrypted.Length - pad];
            Array.Copy(decrypted, 0, res, 0, decrypted.Length - pad);
            return res;
        }
    }
}