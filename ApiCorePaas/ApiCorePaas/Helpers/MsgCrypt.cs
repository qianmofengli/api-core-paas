﻿using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Options;
using Quartz.Util;
using Serilog;

namespace ApiCorePaas.Helpers
{
    /// <summary>
    /// 微信消息加解密
    /// </summary>
    public static class MsgCrypt
    {
        /// <summary>
        /// 检验消息的真实性，并且获取解密后的明文
        /// </summary>
        /// <param name="msgSignature"></param>
        /// <param name="timeStamp"></param>
        /// <param name="nonce"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public static (MsgCryptCodeEnum, string) DecryptMsg(string msgSignature, string timeStamp, string nonce,
            string postData)
        {
            if (AppSettings.WeChatOption.EncodingAesKey.Length != 43)
                return (MsgCryptCodeEnum.IllegalAesKey, default);
            try
            {
                var doc = new XmlDocument();

                doc.LoadXml(postData);
                var root = doc.FirstChild;
                var encryptMsg = root?["Encrypt"]?.InnerText;

                //verify signature
                var code = VerifySignature(AppSettings.WeChatOption.OfficialAccountToken, timeStamp, nonce, encryptMsg,
                    msgSignature);
                if (code != MsgCryptCodeEnum.Success)
                    return (code, default);
                var msgAppId = "";

                var msg = Cryptography.AesDecrypt(encryptMsg, AppSettings.WeChatOption.EncodingAesKey, ref msgAppId);

                return msgAppId != AppSettings.WeChatOption.OfficialAccountAppId
                    ? (MsgCryptCodeEnum.ValidateAppidError, default)
                    : (MsgCryptCodeEnum.Success, msg);
            }
            catch (FormatException fe)
            {
                Log.Error(fe.Message);
                return (MsgCryptCodeEnum.DecodeBase64Error, default);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return (MsgCryptCodeEnum.ParseXmlError, default);
            }
        }

        /// <summary>
        /// 加密消息
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static (MsgCryptCodeEnum, string) EncryptMsg(string msg)
        {
            var timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            var nonce = new Random().Next().ToString();

            if (AppSettings.WeChatOption.EncodingAesKey.Length != 43)
                return (MsgCryptCodeEnum.IllegalAesKey, default);

            try
            {
                var raw = Cryptography.AesEncrypt(msg, AppSettings.WeChatOption.EncodingAesKey,
                    AppSettings.WeChatOption.OfficialAccountAppId);
                var (code, gSignature) =
                    GenerateSignatures(AppSettings.WeChatOption.OfficialAccountToken, timeStamp, nonce, raw);
                if (code != MsgCryptCodeEnum.Success)
                    return (code, default);
                var encryptMsg = "";

                const string encryptLabelHead = "<Encrypt><![CDATA[";
                const string encryptLabelTail = "]]></Encrypt>";
                const string msgSigLabelHead = "<MsgSignature><![CDATA[";
                const string msgSigLabelTail = "]]></MsgSignature>";
                const string timeStampLabelHead = "<TimeStamp><![CDATA[";
                const string timeStampLabelTail = "]]></TimeStamp>";
                const string nonceLabelHead = "<Nonce><![CDATA[";
                const string nonceLabelTail = "]]></Nonce>";
                encryptMsg = encryptMsg + "<xml>" + encryptLabelHead + raw + encryptLabelTail;
                encryptMsg = encryptMsg + msgSigLabelHead + gSignature + msgSigLabelTail;
                encryptMsg = encryptMsg + timeStampLabelHead + timeStamp + timeStampLabelTail;
                encryptMsg = encryptMsg + nonceLabelHead + nonce + nonceLabelTail;
                encryptMsg += "</xml>";
                return (MsgCryptCodeEnum.Success, encryptMsg);
            }
            catch (Exception)
            {
                return (MsgCryptCodeEnum.EncryptAesError, default);
            }
        }

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="token"></param>
        /// <param name="timeStamp"></param>
        /// <param name="nonce"></param>
        /// <param name="msgEncrypt"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private static MsgCryptCodeEnum VerifySignature(string token, string timeStamp, string nonce, string msgEncrypt,
            string signature)
        {
            var (code, gSignature) = GenerateSignatures(token, timeStamp, nonce, msgEncrypt);
            if (code != MsgCryptCodeEnum.Success)
                return code;
            return gSignature == signature ? MsgCryptCodeEnum.Success : MsgCryptCodeEnum.ValidateSignatureError;
        }

        /// <summary>
        /// 生成签名
        /// </summary>
        /// <param name="token"></param>
        /// <param name="timeStamp"></param>
        /// <param name="nonce"></param>
        /// <param name="msgEncrypt"></param>
        /// <returns></returns>
        private static (MsgCryptCodeEnum, string) GenerateSignatures(string token, string timeStamp, string nonce,
            string msgEncrypt)
        {
            try
            {
                var al = new ArrayList { token, timeStamp, nonce, msgEncrypt };
                al.Sort(new DictionarySort());
                var raw = al.Cast<object>().Aggregate("", (current, t) => current + t);
                
                using var sha256 = SHA1.Create();
                var aSciEncoding = new ASCIIEncoding();
                var dataToHash = aSciEncoding.GetBytes(raw);
                var dataHashed = sha256.ComputeHash(dataToHash);
                var signature = BitConverter.ToString(dataHashed).Replace("-", "").ToLower();
                
                return (MsgCryptCodeEnum.Success, signature);
                // var al = new ArrayList { token, timeStamp, nonce, msgEncrypt };
                //
                // al.Sort(new DictionarySort());
                //
                // var raw = al.Cast<object>().Aggregate("", (current, t) => current + t);
                //
                //
                // SHA1 sha = new SHA1CryptoServiceProvider();
                //
                // var aSciEncoding = new ASCIIEncoding();
                //
                // var dataToHash = aSciEncoding.GetBytes(raw);
                //
                // var dataHashed = sha.ComputeHash(dataToHash);
                //
                // var signature = BitConverter.ToString(dataHashed).Replace("-", "").ToLower();
                //
                //
                // return (MsgCryptCodeEnum.Success, signature);
            }
            catch (Exception e)
            {
                Log.Error("微信公众号生成签名异常：{EMessage}", e.Message);
                return (MsgCryptCodeEnum.ComputeSignatureError, default);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DictionarySort : IComparer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oLeft"></param>
        /// <param name="oRight"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public int Compare(object oLeft, object oRight)
        {
            var sLeft = oLeft as string;
            var sRight = oRight as string;
            if (sLeft.IsNullOrWhiteSpace())
                throw new ArgumentException($"{nameof(oLeft)} 为空");
            if (sRight.IsNullOrWhiteSpace())
                throw new ArgumentException($"{nameof(sRight)} 为空");
            // ReSharper disable once PossibleNullReferenceException
            var iLeftLength = sLeft.Length;
            // ReSharper disable once PossibleNullReferenceException
            var iRightLength = sRight.Length;
            var index = 0;
            while (index < iLeftLength && index < iRightLength)
            {
                if (sLeft[index] < sRight[index])
                    return -1;
                if (sLeft[index] > sRight[index])
                    return 1;
                index++;
            }

            return iLeftLength - iRightLength;
        }
    }
}