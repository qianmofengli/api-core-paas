﻿using ApiCorePaas.DataModels.ErnieBot;
using ApiCorePaas.DataModels.WeChat;
using ApiCorePaas.Models.CacheServices;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;

namespace ApiCorePaas.Helpers;

/// <summary>
/// ErnieBot帮助类
/// </summary>
public static class ErnieBotHelper
{
    /// <summary>
    /// 获取accessToken
    /// </summary>
    public static async Task<string> GetAccessToken()
    {
        var key = $"{RedisKeyEnum.ErnieBotAccessToken.GetDescription()}_{AppSettings.ErnieBotOption.AppId}";

        var (accessToken, getSuccess) = await CachePool.ErnieBotRedisCache.Get(key);
        if (getSuccess && !string.IsNullOrEmpty(accessToken))
        {
            return accessToken;
        }

        var url =
            $"oauth/2.0/token?grant_type=client_credentials&client_id={AppSettings.ErnieBotOption.ApiKey}&client_secret={AppSettings.ErnieBotOption.SecretKey}";
        var (response, error) =
            await ClientPool.ErnieBotClient.PostAsync<BaiDuAccessTokenResponseModel>(string.Empty, url);
        if (error != null)
        {
            throw error;
        }

        if (!string.IsNullOrEmpty(response.Error))
        {
            throw new Exception(response.ErrorDescription);
        }

        var success = await CachePool.ErnieBotRedisCache.Set(
            key,
            response.AccessToken, TimeSpan.FromSeconds(response.ExpiresIn));
        if (!success)
        {
            throw new Exception("缓存公众号accessToken失败");
        }

        return response.AccessToken;
    }
}