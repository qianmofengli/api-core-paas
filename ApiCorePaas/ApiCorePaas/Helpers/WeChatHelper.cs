﻿using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using ApiCorePaas.DataModels.WeChat;
using ApiCorePaas.Models.CacheServices;
using ApiCorePaas.Models.Clients;
using ApiCorePaas.Models.Enums;
using ApiCorePaas.Options;
using ApiCorePaas.Tools;

namespace ApiCorePaas.Helpers;

/// <summary>
/// 微信帮助类
/// </summary>
public static class WeChatHelper
{
    /// <summary>
    /// 获取公众号的AccessToken
    /// </summary>
    /// <returns></returns>
    public static async Task<string> GetOfficialAccountAccessTokenAsync()
    {
        var appId = AppSettings.WeChatOption.OfficialAccountAppId;

        var key =
            $"{RedisKeyEnum.OfficialAccountAccessToken.GetDescription()}_{appId}";
        var (accessToken, success) = await CachePool.OfficialAccountRedisCache.Get(key);
        if (success && !string.IsNullOrEmpty(accessToken))
        {
            return accessToken;
        }

        var url =
            $"cgi-bin/token?grant_type=client_credential&appid={appId}&secret={AppSettings.WeChatOption.OfficialAccountAppSecret}";
        var (response, error) =
            await ClientPool.WeChatClient.GetAsync<AccessTokenResponseModel>(url);
        if (error != null)
        {
            throw error;
        }

        if (response.ErrCode != 0)
        {
            throw new Exception(response.ErrMsg);
        }

        var token = response.AccessToken;
        if (string.IsNullOrEmpty(token))
        {
            throw new Exception("获取公众号accessToken失败,内容为空");
        }

        //放到缓存库
        var setSuccess = await CachePool.OfficialAccountRedisCache.Set(
            $"{RedisKeyEnum.OfficialAccountAccessToken.GetDescription()}_{appId}",
            token, TimeSpan.FromSeconds(response.ExpiresIn));
        if (!setSuccess)
        {
            throw new Exception("缓存公众号accessToken失败");
        }

        return token;
    }
}