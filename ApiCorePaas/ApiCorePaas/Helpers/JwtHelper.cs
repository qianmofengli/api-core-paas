﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using ApiCorePaas.Options;
using Microsoft.IdentityModel.Tokens;

namespace ApiCorePaas.Helpers;

/// <summary>
/// jwt 帮助类
/// </summary>
public static class JwtHelper
{
    // /// <summary>
    // /// 获得一个jwt token
    // /// </summary>
    // /// <param name="userId"></param>
    // /// <param name="userName"></param>
    // /// <returns></returns>
    // public static string GetToken(string userId, string userName)
    // {
    //     // 创建用户身份声明
    //     var claims = new[]
    //     {
    //         new Claim("user_id", userId),
    //         new Claim(ClaimTypes.Name, userName),
    //         new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
    //     };
    //
    //     // 创建令牌的密钥
    //     var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.JwtSettingOption.Key));
    //
    //     // 创建令牌签名证书
    //     var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
    //
    //     // 设置令牌过期时间
    //     var expiration = DateTime.UtcNow.AddMinutes(Convert.ToDouble(AppSettings.JwtSettingOption.DurationInMinutes));
    //
    //     // 创建 JWT
    //     var token = new JwtSecurityToken(
    //         issuer: AppSettings.JwtSettingOption.Issuer,
    //         audience: AppSettings.JwtSettingOption.Audience,
    //         claims: claims,
    //         expires: expiration,
    //         signingCredentials: credentials
    //     );
    //
    //     // 将令牌序列化为字符串
    //     var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
    //
    //     return tokenString;
    // }
    //
    
    /// <summary>
    /// 生成访问令牌
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="isDefault"></param>
    /// <returns></returns>
    public static string GenerateJwtToken(int userId, bool isDefault = false)
    {
        var jwtTokenHandler = new JwtSecurityTokenHandler();

        //var key = Encoding.ASCII.GetBytes(AppSettings.options.SecurityKey);
        var claims = new List<Claim>
        {
            new("user_id", userId.ToString()),
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        };


        //使用rsa加密
        var privateKey = Convert.FromBase64String(
            "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDr3JKRvP3NJ89lYHs8wm7T02hGgPVK3EEko8Z6cGdDHBAq6XIZqSxUIBpynURE26YmHtex3DJhsVDHOyBBbPwZ+f440FcsFwSkSj6rX8Ec4FMYUVi1ULKe/IJMID8rJhQFUpQKkEohIpPu+vwZ69ozH8AP10QXv1zGq/96iy54xV2+225PJUQoJZ299CjvRDhxk5+DH7s/BJSLXsLxO1WTeIyAtW6kRzTtkkV6JAvz6o5DN+mxMVi/Ewo+4oJ+u5vKpL4y4MHALF1jfukY0jEBvukKuNT3xpW8n/jOzi4qk/N9ys/HWcULlmXM4PTBiZDAzzF7WziE0GCwvfeh4UYvAgMBAAECggEAKmF39LgA4txEZ/LuMfy97VIPapvBd7IcAnlrMJUB+Q0R2qQX3ISJcpRSLEYV/kl5kV/5YfwvEV7LJQF6ykbLBd/lzk+hWkrIirzMhf6utIGabMQxLGNTtT2qLAOfViGBd3NfJobGyy1rZ4WxxvaaPpV17WLA2YOJKCm/vkVEFG8KEXSta4onDpbkTFiiBwDg15QpxtyVWZAag0xy2EAkA7tlFg4UsWyc5SaWX1P5hmaWiaUiq6fJtGYLDuTRlmlWJHivFIxFc8seQrJYFjK680I5+W2QHnHQViJm2Tgy5i4iFPObxNCfuGOgik4bLvfWPuZwIMWPEs1caUN88GKQAQKBgQD8z6YjJlrBibV3I0//bl1rTX/YK//UqfpicvjzAA+D08s8nCD4RUv4rTeqKeaQ5KWCS9nH/OIRJ/oCT80ac6Qw7ioyoWInhvKNVzvZFpiSiVD5M5I+4N4ISQm4G/ArFq3KSzdjJuqqKzjsR45QZetTpSBVbJ0SU6gwFHCsjje/TQKBgQDu1jEk5MzjeOre4aocS0XvZFD3cs/7qnyKt4z7qn9g+AmkYhOId/8ZFSXVnp+5GE+MTx0rFBUTf8yFZE2+aGtRhIEToOZikei5fJi9vGy5eH2mMh4H6lg7MRe1qdgu8GX7N6tonhPgPuasACKF0BL+s2hFo9YnDcGZKYWBme8VawKBgBISkhDfNAuz2Xob1yMuc5UxJBuDAbXlNNYG3A8bCQDmihfaHW1CHu03DMyoVCg1gDNxUq8+P7b0QaYaqFHkxG8PB4s5CDhFKF+8RbX+LIgFMvmtmx3Nm3HCLuSvcprR8ziL6fi3lnz1vAsADodm0czX+BjDO/NMcbCGNYvQWuHdAoGAAi7U/ihVMxMM2oKngmCxv/FjyA81lXBGeSIaWMaDKYi61dTLpuip3TnTQbOUdCWjvH2Qn68sTiRR4zo6d64OpQ0X/OzwjiAVR3S7FKjfOYa/Z0B4A8yBh/D37YIi5rLty7EDkQ440kjg5NuPdTekpM1Q+6n2sB6xEUxO2xNkGwcCgYEAgCkzfR6YCjA+zrizrFXkYVDM0CONMU0O/AoH/D+zj+Bn76MTmMVWcleC4dfPesRvhFEgQW7Z0/nwNCcCvI1LfZEdQk31/twZa0ONPEoehREXuZGon/rPbLKsIMYiGK7sd16vH8M/hSJphA8GI84Kvapwnat2ozkTJIuYPSEw3Ms=");
        var rsa = RSA.Create();
        rsa.ImportPkcs8PrivateKey(privateKey, out _);
        var key = new RsaSecurityKey(rsa);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),

            //10天免登录
            Expires = isDefault ? DateTime.UtcNow.AddDays(10) : DateTime.UtcNow.AddMinutes(AppSettings.JwtSettingOption.DurationInMinutes),
            SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.RsaSha256Signature)
        };

        var token = jwtTokenHandler.CreateToken(tokenDescriptor);
        var jwtToken = jwtTokenHandler.WriteToken(token);

        return jwtToken;
    }

}